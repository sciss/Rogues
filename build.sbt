lazy val baseName       = "Rogues"
//lazy val baseNameL      = baseName.toLowerCase
lazy val projectVersion = "1.3.0-SNAPSHOT"
lazy val gitHost        = "codeberg.org"
lazy val gitUser        = "sciss"
lazy val gitRepo        = baseName

lazy val buildInfoSettings = Seq(
  // ---- build info ----
  buildInfoKeys := Seq(name, organization, version, scalaVersion, description,
    BuildInfoKey.map(homepage) { case (k, opt)           => k -> opt.get },
    BuildInfoKey.map(licenses) { case (_, Seq((lic, _))) => "license" -> lic }
  ),
  buildInfoOptions += BuildInfoOption.BuildTime
)

lazy val commonSettings = Seq(
  version      := projectVersion,
  homepage     := Some(url(s"https://$gitHost/$gitUser/$gitRepo")),
  scalaVersion := "3.1.3", // "2.13.8",
  scalacOptions ++= Seq("-deprecation"),
  licenses     := Seq("AGPL v3+" -> url("http://www.gnu.org/licenses/agpl-3.0.txt")),
  run / fork   := true,
  libraryDependencies ++= Seq(
    "de.sciss"    %% "fileutil"   % deps.common.fileUtil,   // utility functions
    "de.sciss"    %% "model"      % deps.common.model,      // events
    "de.sciss"    %% "numbers"    % deps.common.numbers,    // numeric utilities
    "de.sciss"    %% "scalaosc"   % deps.common.osc,        // open sound control
    "de.sciss"    %% "swingplus"  % deps.common.swingPlus,  // user interface
    "org.rogach"  %% "scallop"    % deps.common.scallop     // command line option parsing
  )
) ++ assemblySettings

lazy val root = project.in(file("."))
  .aggregate(common, online, offline)
  .settings(commonSettings)
//  .settings(assemblySettings)
  .settings(
    name := baseName,
    description  := "An art piece"
  )

lazy val common = project.in(file("common"))
  .enablePlugins(BuildInfoPlugin)
  .settings(commonSettings)
  .settings(buildInfoSettings)
  .settings(
    name := s"$baseName-common",
    description := "Common code",
    buildInfoPackage := "de.sciss.rogues"
  )

lazy val online = project.in(file("online"))
  .dependsOn(common)
  .settings(commonSettings)
  .settings(
    name := s"$baseName-online",
    description := "Common code",
    libraryDependencies ++= Seq(
      "com.jhlabs"    %  "filters"                  % deps.online.jhlabs,       // image composites
      "com.pi4j"      %  "pi4j-core"                % deps.online.pi4j,         // GPIO control
      "com.pi4j"      %  "pi4j-plugin-raspberrypi"  % deps.online.pi4j,         // GPIO control
      "com.pi4j"      %  "pi4j-plugin-pigpio"       % deps.online.pi4j,         // GPIO control
      "de.sciss"      %% "audiofile"                % deps.online.audioFile,    // record data as sound file
      "de.sciss"      %% "fscape-lucre"             % deps.online.fscape,       // offline rendering
      "de.sciss"      %% "patterns-lucre"           % deps.online.patterns,
      "de.sciss"      %% "soundprocesses-core"      % deps.online.soundProcesses,
      "net.harawata"  %  "appdirs"                  % deps.online.appDirs,      // finding standard directories
      "com.fazecast"  %  "jSerialComm"              % deps.online.jSerialComm   // Serial port reading
    ),
    assembly / assemblyJarName := s"$baseName-online.jar",
  )

lazy val offline = project.in(file("offline"))
  .dependsOn(common)
  .settings(commonSettings)
  .settings(
    name := s"$baseName-common",
    description := "Offline code not run during the installation",
    libraryDependencies ++= Seq(
      "net.imagej"    %  "ij"                       % deps.offline.imageJ,      // analyzing image data
      "org.hid4java"  %  "hid4java"                 % deps.offline.hid4java     // USB HID access
    ),
    resolvers += Resolver.sonatypeRepo("snapshots")  // needed for hid4java
  )

lazy val deps = new {
  val common = new {
    val fileUtil        = "1.1.5"
    val model           = "0.3.5"
    val numbers         = "0.2.1"
    val osc             = "1.3.1"
    val scallop         = "4.1.0"
    val swingPlus       = "0.5.0"
  }
  val online = new {
    val appDirs         = "1.2.2"
    val audioFile       = "2.4.2"
    val fscape          = "3.15.6"
    val jhlabs          = "2.0.235"
    val jSerialComm     = "2.9.3" // "2.8.0"
    val patterns        = "1.11.0"
    val pi4j            = "2.1.1" // "2.1.0"
    val soundProcesses  = "4.14.17"
  }
  val offline = new {
    val hid4java        = "develop-20201104.172733-8" // stable: "0.7.0"
    val imageJ          = "1.53j" // "1.47h"
  }
}

lazy val assemblySettings = Seq(
  // ---- assembly ----
  assembly / test            := {},
  assembly / target          := baseDirectory.value,
  ThisBuild / assemblyMergeStrategy := {
    case "logback.xml" => MergeStrategy.last
    case PathList("org", "xmlpull", _ @ _*)              => MergeStrategy.first
    case PathList("org", "w3c", "dom", "events", _ @ _*) => MergeStrategy.first // Apache Batik
    case PathList(ps @ _*) if ps.last endsWith "module-info.class" =>
//      println(s"DISCARD: $p")
      MergeStrategy.discard // Jackson, Pi4J
    case PathList(ps @ _*) if ps.last endsWith ".proto" =>
//      println(s"DISCARD: $p")
      MergeStrategy.discard // Akka vs Google protobuf what the hell
    case x =>
      val old = (ThisBuild / assemblyMergeStrategy).value
      old(x)
  }
//  assembly / fullClasspath := (Test / fullClasspath).value // https://github.com/sbt/sbt-assembly/issues/27
)
