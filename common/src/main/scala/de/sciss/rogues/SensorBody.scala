/*
 *  SensorBody.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.model.Model
import de.sciss.model.impl.ModelImpl
import de.sciss.osc
import org.rogach.scallop.ScallopConf

import java.awt.Color
import scala.swing.{Component, Dimension, Graphics2D, MainFrame, Swing}

object SensorBody {
  case class ConfigImpl(
                         numLDRSensors          : Int     = 6,
                         numCapSensors          : Int     = 6,
                         buttonSensors          : Boolean = true,
                         sensorThreshWake       : Float   = 0.25f,
                         sensorThreshShift      : Float   = 0.12f,
                         sensorAttackShift      : Float   = 0.95f,
                         sensorDecayShift       : Float   = 0.995f,
                         sensorAttackFlare      : Float   = 0.1f,
                         sensorDecayFlare       : Float   = 0.1f,
                         sensorDiffMinBoost     : List[Float] = 1.0f / 32 :: Nil,
                         sensorDiffMaxBoost     : List[Float] = 1.0f /  4 :: Nil,
                         sensorDiffShrinkThresh : Float   = 0.5f,
                       )
    extends Config

  trait Config {
    def numLDRSensors     : Int
    def numCapSensors     : Int
    def buttonSensors     : Boolean
    def sensorThreshWake  : Float
    def sensorThreshShift : Float
    def sensorAttackShift : Float
    def sensorDecayShift  : Float
    def sensorAttackFlare : Float
    def sensorDecayFlare  : Float
    def sensorDiffMinBoost: List[Float]
    def sensorDiffMaxBoost: List[Float]
    def sensorDiffShrinkThresh: Float

    val numSensors: Int = numLDRSensors + numCapSensors + (if buttonSensors then 1 else 0)
  }

  def apply()(implicit config: Config): SensorBody = {
    new Impl
  }

  private class Impl(implicit config: Config) extends SensorBody with ModelImpl[Unit] {
    import config._

    private val baseGain        = 1.0 / 64
    private val _rawValues      = new Array[Int](numSensors)
    private var initialized     = false
    private val xs              = new Array[Double](numSensors)      // smooth sensor values, base gain applied
    private val dsShift         = new Array[Double](numSensors)      // smooth differentiation
    private val dsFlare         = new Array[Double](numSensors)      // smooth differentiation
    private val dGainTimeShift  = new Array[Long  ](numSensors)
    private val dGainTimeFlare  = new Array[Long  ](numSensors)
    private val diffMinBoost    = Array.tabulate(numSensors)(i => config.sensorDiffMinBoost(i % config.sensorDiffMinBoost.length))
    private val diffMaxBoost    = Array.tabulate(numSensors)(i => config.sensorDiffMaxBoost(i % config.sensorDiffMaxBoost.length))
    private val diffInitBoost   = Array.tabulate(numSensors)(i => Math.sqrt(diffMinBoost(i) * diffMaxBoost(i)))
    private val bsShift         = Array.tabulate[Double](numSensors)(i => diffInitBoost(i))   // differentiation boost
    private val bsFlare         = Array.tabulate[Double](numSensors)(i => diffInitBoost(i))   // differentiation boost
    private val w1In            = 0.96
    private val w2In            = 1.0 - w1In
    private val w1DiffUpShift   = config.sensorAttackShift
    private val w2DiffUpShift   = 1.0f - w1DiffUpShift
    private val w1DiffUpFlare   = config.sensorAttackFlare
    private val w2DiffUpFlare   = 1.0f - w1DiffUpFlare
    private val w1DiffDnShift   = config.sensorDecayShift
    private val w2DiffDnShift   = 1.0f - w1DiffDnShift
    private val w1DiffDnFlare   = config.sensorDecayShift
    private val w2DiffDnFlare   = 1.0f - w1DiffDnFlare
    private val diffGainShrink  = 0.99f
    private val diffGainGrow    = 1.001f
    private val diffShrinkThresh= config.sensorDiffShrinkThresh

    override def rawValues      : Array[Int]    = _rawValues
    override def smoothValues   : Array[Double] = xs
    override def diffValuesShift: Array[Double] = dsShift
    override def diffValuesFlare: Array[Double] = dsFlare

    override def buttons: Int = if config.buttonSensors then _rawValues(_rawValues.length - 1) else 0

    override def update(data: Array[Int]): Unit = {
      val a       = _rawValues
      val numVals = Math.min(data.length, a.length)
      System.arraycopy(data, 0, a, 0, numVals)
      if initialized then {
        val now = System.currentTimeMillis()
        var i = 0
        val numLDR = Math.min(numVals, config.numLDRSensors)
        while i < numVals do {
          val xn  = a(i) * baseGain
          val xb  = xs(i)
          val x   = xb * w1In + xn * w2In
          xs(i)   = x
          if i < numLDR then {
            val dbShift       = dsShift(i)
            val dbFlare       = dsShift(i)
            val diffGainShift = bsShift(i)
            val diffGainFlare = bsFlare(i)
            val dShift   = Math.abs(xb - xn) * diffGainShift
            val dFlare   = Math.abs(xb - xn) * diffGainFlare
            val dnShift  = if dbShift < dShift then dbShift * w1DiffUpShift + dShift * w2DiffUpShift else dbShift * w1DiffDnShift + dShift * w2DiffDnShift
            val dnFlare  = if dbFlare < dFlare then dbFlare * w1DiffUpFlare + dFlare * w2DiffUpFlare else dbFlare * w1DiffDnFlare + dFlare * w2DiffDnFlare
            dsShift(i) = dnShift
            dsFlare(i) = dnFlare
            if dnShift > diffShrinkThresh then {
              if diffGainShift > diffMinBoost(i) then {
                val gainNew = diffGainShift * diffGainShrink
                bsShift(i) = gainNew
                dGainTimeShift(i) = now + 60000L // one minute time-out
              }
            } else {
              if diffGainShift < diffMaxBoost(i) && now > dGainTimeShift(i) then {
                val gainNew = diffGainShift * diffGainGrow
                bsShift(i) = gainNew
              }
            }
            if dnFlare > diffShrinkThresh then {
              if diffGainFlare > diffMinBoost(i) then {
                val gainNew = diffGainFlare * diffGainShrink
                bsFlare(i) = gainNew
                dGainTimeFlare(i) = now + 60000L // one minute time-out
              }
            } else {
              if diffGainFlare < diffMaxBoost(i) && now > dGainTimeFlare(i) then {
                val gainNew = diffGainFlare * diffGainGrow
                bsFlare(i) = gainNew
              }
            }
          }
          i += 1
        }

      } else {
        initialized = true
        val b = xs
        var i = 0
        while i < numLDRSensors do {
          b(i) = a(i) * baseGain
          i += 1
        }
      }
      dispatch(())
    }
  }

  def ui(body: SensorBody)(implicit config: Config): Unit = {
    import config.{sensorThreshShift, sensorThreshWake}

//    val numSensors  = config.numSensors
    val numLDR      = config.numLDRSensors
    val numBoth     = numLDR + config.numCapSensors
    val sv          = body.smoothValues
    val dv          = body.diffValuesShift

    lazy val lb: Component = new Component {
      preferredSize = new Dimension(520, 8 + ((numBoth + numLDR) * 10 + (if config.buttonSensors then 20 else 0)))

      override def paintComponent(g: Graphics2D): Unit = {
        super.paintComponent(g)

        if config.buttonSensors then {
          val butVal = body.buttons
          g.drawString(butVal.toString, 256, 12)
        }

        var i = 0
        var y = if config.buttonSensors then 24 else 4
        val x0 = 4
        val w0 = 512
        val x1 = x0 + w0 - 1
        g.drawLine(x0, y, x1, y)
        y += 4
        while i < numBoth do {
          val x = sv(i)
          g.fillRect(4, y, x.toInt, 8)
          i += 1
          y += 10
        }
        //        y -= 6
        g.drawLine(x0, y, x1, y)
        val yt0 = y
        i = 0
        while i < numLDR do {
          val dn = dv(i)
          g.setColor(if dn > sensorThreshWake then Color.red else if dn > sensorThreshShift then Color.blue else Color.black)
          val x = Math.min(1.0, dn) * w0
          g.fillRect(4, y, x.toInt, 8)
          i += 1
          y += 10
        }
        val yt1 = y
        g.drawLine(x0, y, x1, y)
        val xtShift = x0 + (sensorThreshShift * w0).toInt
        val xtWake  = x0 + (sensorThreshWake  * w0).toInt
        g.setColor(Color.blue)
        g.drawLine(xtShift, yt0, xtShift, yt1)
        g.setColor(Color.red)
        g.drawLine(xtWake, yt0, xtWake, yt1)
      }
    }

    Swing.onEDT {
      val f = new MainFrame
      f.title = "Sensors"
      f.contents = lb
      f.pack().centerOnScreen()
      f.open()
    }

    body.addListener {
      case _ =>
        lb.repaint()
        lb.toolkit.sync()
    }
  }
}
trait SensorBody extends Model[Unit] {
  def update(raw: Array[Int]): Unit

  def rawValues       : Array[Int]

  def smoothValues    : Array[Double]
  def diffValuesShift : Array[Double]
  def diffValuesFlare : Array[Double]

  /** bit-mask */
  def buttons: Int
}