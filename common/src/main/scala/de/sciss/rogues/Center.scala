/*
 *  Center.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.file.*

import java.io.{DataInputStream, DataOutputStream, FileInputStream, FileOutputStream}

object Center {
  private val COOKIE = 0x43656e74

  def mkFile(id: Int): File = file("images") / s"centers$id.bin"

  def read(id: Int): Seq[Center] =
    read(mkFile(id))

  def read(f: File): Seq[Center] = {
    val fin = new FileInputStream(f)
    try {
      val din = new DataInputStream(fin)
      val cookie = din.readInt()
      require (cookie == COOKIE, s"0x${cookie.toHexString} is not 0x${COOKIE.toHexString}")
      val num = din.readShort().toInt
      Seq.fill(num) {
        val cx = din.readInt()
        val cy = din.readInt()
        val r  = din.readInt()
        val strength = din.readFloat().toDouble
        Center(cx = cx, cy = cy, r = r, strength = strength)
      }
    } finally fin.close()
  }

  def write(id: Int, cs: Seq[Center]): Unit =
    write(mkFile(id), cs)

  def write(f: File, cs: Seq[Center]): Unit = {
    require (!f.exists())
    val fos = new FileOutputStream(f)
    try {
      val dos = new DataOutputStream(fos)
      dos.writeInt(COOKIE)
      dos.writeShort(cs.size)
      cs.foreach { c =>
        dos.writeInt(c.cx)
        dos.writeInt(c.cy)
        dos.writeInt(c.r)
        dos.writeFloat(c.strength.toFloat)
      }
      dos.flush()
    } finally fos.close()
  }

//  def main(args: Array[String]): Unit = {
//    val f = new File("").getAbsoluteFile
//    val dirImg = f / "images"
//    require (dirImg.isDirectory)
//    map.foreach { case (id, cs) =>
//      val fOut = dirImg / s"centers$id.bin"
//      require (!fOut.exists())
//      write(fOut, cs)
//    }
//  }
}
case class Center(cx: Int, cy: Int, r: Int, strength: Double)
