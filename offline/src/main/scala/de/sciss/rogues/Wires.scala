/*
 *  Wires.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.numbers.Implicits.*

object Wires {
  def main(args: Array[String]): Unit = {
    val res = gordite_1()
    println(s"Number of cells: ${res.size} (${res.size / 16.0} rows)")
    println("Result:")
    println(res.grouped(16).map(r => r.map(c => "% 4d".format(c)).mkString(" ", ", ", ",")).mkString("[\n", "\n", "\n]"))
  }

  // upper part of 'gordite'
  def gordite_1(): Vector[Int] =
    cone(
      bLow      = 0,
      bHigh     = 150,
      startVal  = 150,
      bStep     = 5,
      numCells  = 192 + 32,
      seed      = 0xA11B1114L
    )

  // upper part of second 'phoretic'
  def attr_4(): Vector[Int] =
    cone(
      bLow      = 0,
      bHigh     = 116,
      startVal  = 116,
      bStep     = 5,
      numCells  = 192 + 20,
      seed      = 0xA11B1114L
    )

  // lower part of second 'phoretic'
  def attr_3(): Vector[Int] =
    cone(
      bLow      = 0,
      bHigh     = 116,
      startVal  = 116,
      bStep     = 5,
      numCells  = 192 - 20,
      seed      = 0xA11B1117L
    )

  // upper part of first 'phoretic'
  def attr_2(): Vector[Int] =
    cone(
      bLow      = 0,
      bHigh     = 116,
      startVal  = 112,
      bStep     = 4,
      numCells  = 192,
      seed      = 0xA11B1111L
    )

  // lower part of first 'phoretic'
  def attr_1(): Vector[Int] =
    cone(
      bLow      = 0,
      bHigh     = 116,
      startVal  = 112,
      bStep     = 4,
      numCells  = 192,
      seed      = 0xA11B1110L
    )

  // upper part of 'window'
  def cone0(): Vector[Int] =
    val c = cone(
      bLow      = 0,
      bHigh     = 116,
      startVal  = 56,
      bStep     = 4,
      numCells  = 19 * 16 - 94,
      seed      = (0xBEBCL + 4)
    )
    val done = Vector(
      112, 112, 108, 104, 104, 104, 100, 104, 100, 96, 100, 104, 104, 104, 104, 104, 100, 96, 96, 92, 96, 92, 88, 88, 84, 80, 76, 76, 72, 72, 76, 72, 72, 68, 68, 64, 64, 60, 56, 52, 48, 48, 48, 44, 48, 48, 52, 56, 60, 56, 56, 56, 60, 64, 64, 64, 64, 64, 68, 68, 68, 64, 60, 64, 60, 64, 64, 64, 64, 64, 60, 64, 60, 56, 56, 52, 52, 56, 56, 56, 52, 56, 60, 64, 60, 64, 68, 64, 64, 60, 60, 56, 60, 56,
    )
    done ++ c


  // lower part of 'window'
  def cone1(): Vector[Int] =
    cone(
      bLow      = 0,
      bHigh     = 116,
      startVal  = 112,
      bStep     = 4,
      numCells  = 17 * 16,
      seed      = (0xBEBCL + 8)
    )

  def cone2(): Vector[Int] =
    val bLow      = 4
    val bHigh     = 28
    val bStep     = 1
    val mul       = 4
    val numCells  = 268
    val startVal  = 104 / mul
    val rnd       = new util.Random(0xBABEL)
    while true do
      var i     = 0
      var pos   = startVal
      val resB  = Vector.newBuilder[Int]
      resB.sizeHint(numCells)
      while pos > bLow && i <= numCells do
        val step  = rnd.nextInt(bStep * 2 + 1) - bStep
        pos = (pos + step).clip(bLow, bHigh)
        resB.addOne(pos)
        i += 1

      val done = i <= numCells
      if done then return resB.result().map(_ * mul)

    Vector.empty  // never here

  // lower part of 'trittico'
  def cone5(): Vector[Int] =
    cone(
      bLow = 0,
      bHigh = 116,
      startVal = 116,
      bStep = 4,
      numCells = 19 * 16,
      seed = (0xBEBCL + 3)
    )

  // upper part of 'trittico'
  def cone4(): Vector[Int] =
    cone(
      bLow      = 0,
      bHigh     = 116,
      startVal  = 116,
      bStep     = 4,
      numCells  = 20 * 16,
      seed      = 0xBEBCL
    )

  // lower part of 'stripey'
  def cone3(): Vector[Int] =
    cone(
      bLow      = 0,
      bHigh     = 112,
      startVal  = 112,
      bStep     = 4,
      numCells  = 192,
      seed      = 0xABEBL
    )

  def cone(bLow: Int, bHigh: Int, bStep: Int, startVal: Int, numCells: Int, seed: Long): Vector[Int] =
    val rnd       = new util.Random(seed)
    while true do
      var i     = 0
      var pos   = startVal
      val resB  = Vector.newBuilder[Int]
      resB.sizeHint(numCells)
      resB.addOne(pos)
      while pos > bLow && i <= numCells do
        val step  = rnd.nextInt(bStep * 2 + 1) - bStep
        pos = (pos + step).clip(bLow, bHigh)
        resB.addOne(pos)
        i += 1

      val done = i <= numCells
      if done then return resB.result() // .map(_ * mul)

    Vector.empty  // never here
}
