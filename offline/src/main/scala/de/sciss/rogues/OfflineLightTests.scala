/*
 *  OfflineLightTests.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.osc

import java.awt.Color
import scala.swing.{Component, Dimension, Graphics2D, MainFrame, Swing}

object OfflineLightTests {
  def main(args: Array[String]): Unit =
    Swing.onEDT(run())

  def run(): Unit = {
    val rc = osc.UDP.Config()
    // rc.localIsLoopback = true
    rc.localAddress = java.net.InetAddress.getByName("192.168.77.78")
    rc.localPort = 57130
    // rc.localSocketAddress //
    val r = osc.UDP.Receiver(rc)
    r.connect()

    val numSensors  = 13
    val sensorVals  = new Array[Int](numSensors)

    implicit val sbConfig: SensorBody.Config = SensorBody.ConfigImpl()
    val sBody = SensorBody()

    r.action = { (p, _ /*addr*/) =>
      p match {
        case osc.Message("/ldr", args @ _*) =>
          val n = Math.min(args.size, numSensors)
          var i = 0
          var j = 0
          while i < n do {
            args(i) match {
              case v: Int => sensorVals(j) = v; j += 1
              case _ =>
            }
            i += 1
          }
          sBody.update(sensorVals)

        case m: osc.Message => println(s"[Warn] Ignoring unknown message ${m.name}")
        case _ => println("[Warn] Ignoring unknown packet")
      }
    }

    SensorBody.ui(sBody)
  }
}
