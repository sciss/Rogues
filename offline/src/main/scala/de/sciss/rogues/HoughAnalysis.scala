/*
 *  HoughAnalysis.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import circledetection.Hough_Transform
import de.sciss.file.*
import ij.ImagePlus
import ij.io.FileInfo
import org.rogach.scallop.{ScallopConf, ScallopOption => Opt}

import java.awt.image.BufferedImage
import java.awt.{BasicStroke, Color}
import javax.imageio.ImageIO

object HoughAnalysis {
  case class Config(
                     minRadius  : Int           = 80,
                     maxRadius  : Int           = 90,
                     numResults : Int           = 20,
                     fileIn     : File          = file("in.jpg"),
                     fileOut    : Option[File]  = None,
                     centerIndex: Int           = -1,
                   )

  def main(args: Array[String]): Unit = {
    object p extends ScallopConf(args) {
      import org.rogach.scallop.*

      printedName = "HoughAnalysis"
      private val default = Config()

      val minRadius: Opt[Int] = opt(default = Some(default.minRadius),
        descr = s"Hough minimum radius (default: ${default.minRadius}).",
        validate = x => x >= 3
      )
      val maxRadius: Opt[Int] = opt(default = Some(default.maxRadius),
        descr = s"Hough maximum radius (default: ${default.maxRadius}).",
        validate = x => x >= 3,
      )
      val numResults: Opt[Int] = opt(default = Some(default.numResults),
        descr = s"Maximum number of centers (default: ${default.numResults}).",
        validate = x => x >= 1,
      )
      val fileIn: Opt[File] = opt(required = true,
        descr = s"Hough analysis input image",
      )
      val fileOut: Opt[File] = opt(
        descr = s"Hough analysis test output image",
      )
      val autoFileOut: Opt[Boolean] = toggle(
        descrYes = "Automatically create test output image name"
      )
      val index: Opt[Int] = opt(default = Some(default.centerIndex),
        descr = s"Output id for center.bin file creation, or -1 to skip creation",
      )

      verify()
      val config: Config = Config(
        minRadius   = minRadius(),
        maxRadius   = maxRadius(),
        numResults  = numResults(),
        fileIn      = fileIn(),
        fileOut     = fileOut.toOption.orElse {
          if !autoFileOut.toOption.contains(true) then None else {
            val fIn   = fileIn()
            val fOut  = fIn.replaceName(s"${fIn.base}-hough.jpg")
            Some(fOut)
          }
        },
        centerIndex = index(),
      )
    }

    implicit val c: Config = p.config
    run()
  }

  def run()(implicit config: Config): Unit = {
    import config.*
    val imgIn0 = ImageIO.read(fileIn)

    def toImageJ(in: BufferedImage): ImagePlus =
      val res = new ImagePlus(fileIn.name, in)
      val fi  = new FileInfo
      fi.fileFormat = FileInfo.IMAGEIO
      fi.fileName   = fileIn.name
      fileIn.parentOption.foreach { p => fi.directory = p.path + File.sep }
      res.setFileInfo(fi)
      res

    val imgInS = imgIn0
    val imgInSP: ImagePlus = toImageJ(imgInS)

    val wS      = imgInSP.getWidth
    val hS      = imgInSP.getHeight
    println(s"Hough input size $wS, $hS")

    val procInS = imgInSP.getProcessor
    val ht      = new Hough_Transform()
    ht.setParameters(minRadius, maxRadius, numResults)
    val htRes: Array[Array[Int]] = ht.runHeadless(procInS)
    val centers = htRes.iterator.map {
      case Array(x, y, r, a) => Center(x, y, r, a) // Point2D(x / houghScale, y / houghScale)
    } .toList.distinct

    println(s"# results: ${centers.size}")
    println(centers.mkString("\n"))
    println("Done.")

    if centerIndex > 0 then {
      Center.write(centerIndex, centers)
    }

    val minAcc = if centers.isEmpty then 0 else centers.map(_._4).min
    val maxAcc = if centers.isEmpty then 1 else centers.map(_._4).max

    fileOut.foreach { fOut =>
      val g = imgInS.createGraphics()
      g.setColor(Color.red)
//      g.setStroke(new BasicStroke(3f))
      centers.foreach { case Center(x, y, r, a) =>
        import de.sciss.numbers.Implicits.*
        val w = a.toFloat.linLin(minAcc.toFloat, maxAcc.toFloat, 1f, 6f)
        g.setStroke(new BasicStroke(w))
        g.drawOval(x - r, y - r, r * 2, r * 2)
      }
      val fmt = if Seq("jpg", "jpeg").contains(fOut.extL) then "jpg" else "png"
      ImageIO.write(imgInS, fmt, fOut)
      g.dispose()
    }
  }
}