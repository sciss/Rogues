// Regular OpenSCAD Variables
$fn = 40;// Quality

stampDiam   = 33.0;
stampLen    = 30.0;
boardWidth  = 85.0;
boardHeight = 56.0;
boardDepth  =  9.5;
baseDepth   = 14.0;
wallThick   =  2.0;
nfcExtent   = 36.0;
holePad     =  3.5;
hole3Dist   = 23.5;
holeDiam    =  3.0;
nfcPad      =  6.7;
printTol    = 0.25;
cablePad    = 6.5;
cableExtent = 2.54 * 5;
cableWide   = 7.0;
baseDuctDepth = 6.0;

// select part
setPart_drop_down_box = "lid";  // [body,lid,base]

part        = setPart_drop_down_box;

stampOuterR = (stampDiam + wallThick*2)/2;
stampX      = boardWidth  - (nfcPad + nfcExtent/2);
stampY      = boardHeight - (nfcPad + nfcExtent/2);
basicDepth  = (part == "body") ? boardDepth : ((part == "base") ? baseDepth : wallThick);
ductDepth   = (part == "base") ? baseDuctDepth : basicDepth;

lin_lin     = function(x, in_lo, in_hi, out_lo, out_hi) (x - in_lo) / (in_hi - in_lo) * (out_hi - out_lo) + out_lo;

function mkPtAngZ(a, w, h, z = 0.0, cx = 0.0, cy = 0.0) =
    let (
        e  = min(w, h),
        m  = max(w, h),
        rx = e * 0.5,
        ry = e * 0.5,
        x  = cos(a) * rx,
        y0 = sin(a) * ry,
        y  = y0 + (a < 180 ? (m - e)/2 : (e - m)/2)
    ) [x + cx, y + cy, z];

// points per circle
ppc = 24; // 36; // 12; // 36;

module ellipseSegmTri(z1 = 0.0, w1 = 1.0, h1 = 1.0,
                     z2 = 1.0, w2 = 1.0, h2 = 1.0, a1start = 0, a1stop = 360, a2start = 0, a2stop = 360, num = ppc,
                     cx1 = 0.0, cy1 = 0.0, cx2 = 0.0, cy2 = 0.0) {
    anglesBot = [
        for (i=[0:num-1]) lin_lin(i, 0, num-1, a1start, a1stop)
    ];
    anglesTop = [
        for (i=[0:num-1]) lin_lin(i, 0, num-1, a2start, a2stop)
    ];
    ptBot = [
        for (i=[0:num-1]) mkPtAngZ(anglesBot[i], w1, h1, z1, cx = cx1, cy = cy1)
    ];
    ptTop = [
        for (i=[0:num-1]) mkPtAngZ(anglesTop[i], w2, h2, z2, cx = cx2, cy = cy2)
    ];
    isPie = num < ppc;
    ptCBot = [cx1, cy1, z1];
    ptCTop = [cx2, cy2, z2];
    fCBot  = num * 2;
    fCTop  = fCBot + 1;
    
    pt = concat(ptBot, ptTop, [ ptCBot, ptCTop ]);
    topNum = isPie ? num - 1 : num;
    facesSide = [
        for (i=[0:topNum-1]) [i, i + num, (i + 1) % num + num, (i + 1) % num ]
    ];
    facesBot = [
        for (i=[0:topNum-1]) [(i + 1) % num, fCBot, i]
    ];
    facesTop = [
        for (i=[0:topNum-1]) [i + num, fCTop, (i + 1) % num + num ]
    ];
    facesPi = isPie ? [[0, fCBot, fCTop, num], [num-1, num+num-1, fCTop, fCBot]] : [];
    faces = concat(facesSide, facesBot, facesTop, facesPi);
    
    polyhedron(pt, faces);
}

module screwDuct() {
    difference() {
        cylinder(h = ductDepth, r = (holeDiam + wallThick*2)/2);
        translate([0, 0, -1]) {
            cylinder(h = ductDepth + 2, r = holeDiam/2);
        }
    }
}

module base(addWall = 0.0) {
    minkowski() {
        cube([boardWidth + addWall*2,boardHeight + addWall*2,basicDepth]);
        cylinder(r=wallThick,h=0.001);
    }
}

module all(){
    difference() {
        if (part == "body" || part == "base") {
            difference() {
                base();
                dz = part == "body" ? wallThick : -wallThick*2; 
                translate([wallThick, wallThick, dz]) {
                    minkowski() {
                        cube([boardWidth - wallThick*2,boardHeight - wallThick*2,basicDepth + 2]);
                        cylinder(r=wallThick,h=0.001);
                    }
                }
            }
        } else {
            base();
        }
        union() {
            translate([hole3Dist, boardHeight - holePad, -1]) {
                cylinder(h = basicDepth + 2, r = holeDiam/2);
            }
            translate([boardWidth - holePad, holePad, -1]) {
                cylinder(h = basicDepth + 2, r = holeDiam/2);
            }
            translate([boardWidth - holePad, boardHeight - holePad, -1]) {
                cylinder(h = basicDepth + 2, r = holeDiam/2);
            }
            if (part == "body" || part == "base") {
                // make space for electronic parts
                dz = part == "body" ? -1: baseDepth - wallThick;
                translate([0, 0, dz]) {
                    union() {
                        cube([19, boardHeight, boardDepth + 2]);
                        translate([0, (boardHeight - 37.0)/2, 0]) {
                            cube([42.5, 37.0, boardDepth + 2]);
                        }
                        translate([26, 0, 0]) {
                            cube([79-26, 7, boardDepth + 2]);
                        }
                    }
                }
            } else if (part == "lid") {
                translate([0, 0, -1]) {
                    translate([stampX, stampY, -1]) {
                        cylinder(h = stampLen + 2, r = stampOuterR + printTol);
                    }
                    smooth = 1;
                    translate([smooth, boardHeight - (cableExtent + cablePad - smooth), 0]) {
                        minkowski() {
                            cube([cableWide - smooth*2, cableExtent - smooth*2, wallThick + 2]);
                            cylinder(r=smooth,h=0.001);
                        }
                    }
                }
            }
        }
    }
    if (part == "body" || part == "base") {
        dz = basicDepth - ductDepth; // basicDepth - boardDepth;
        translate([hole3Dist, boardHeight - holePad, dz]) {
            screwDuct();
        }
        translate([boardWidth - holePad, holePad, dz]) {
            screwDuct();
        }
        translate([boardWidth - holePad, boardHeight - holePad, dz]) {
            screwDuct();
        }
    }
    translate([stampX, stampY, 0]) {
        if (part == "body") {
            difference() {
                cylinder(h = stampLen, r = stampOuterR);
                translate([0, 0, -1]) {
                    cylinder(h = stampLen + 2, r = stampDiam/2);
                }
            }
        }
    }
}

//all();
//translate([wallThick, wallThick, 0]) { base(); }
boardWidthB  = boardWidth  + wallThick * 2;
boardHeightB = boardHeight + wallThick * 2;
pcbThick = 2.0;
platformZ = boardDepth*2 + wallThick + pcbThick + printTol;
heapWidth1  = boardWidthB  * 1.68;
heapWidth1a1 = boardWidthB  * 1.69;
heapHeight1 = boardHeightB * 1.68;
heapHeight1a1 = boardHeightB * 1.72;
heapWidth2  = boardWidthB  * 1.34;
heapHeight2 = boardHeightB * 1.34;
heapWidth3  = (stampOuterR + 2) * 2;
heapHeight3 = (stampOuterR + 2) * 2;
platformZ2  = stampLen - (boardDepth + wallThick) + 2.0;
platformBotThick = 1.0;
platformBotInnerH = 4.6;
platformZ0   = platformBotInnerH + platformBotThick;
platformZ0a1 = platformZ0 + 1.0;

platformZ2a1  = platformZ2 - 10;
heapWidth2a1  = heapWidth3 + 20;
headpWidth2a1 = heapHeight3 + 30;
platformZ3a1  = platformZ2 + 6.66; // 20;

module heapBaseSurface(w1 = heapWidth1, h1 = heapHeight1, w2 = heapWidth2, h2 = heapHeight2, z = platformZ, cubeZ = 0.0) {
    translate([-(boardWidthB/2+printTol), -(boardHeightB/2+printTol)]) {
        difference() {
            ellipseSegmTri(z1 = 0.0, w1 = w1, h1 = h1, z2 = z, w2 = w2, h2 = h2, a1start = 0, a1stop = 360, a2start = 0, a2stop = 360, num = ppc,
                    cx1 = boardWidthB/2, cy1 = boardHeightB/2,
                    cx2 = boardWidthB/2, cy2 = boardHeightB/2
            );
            translate([boardWidthB/2+printTol, boardHeightB/2+printTol]) {
                rotate([0, 0, 90]) {
                    translate([-(boardWidthB/2+printTol), -(boardHeightB/2+printTol), -0.01 + cubeZ]) {
                        cube([boardWidthB + printTol*2, boardHeightB + printTol*2, z + 0.02]);
                    }
                }
            }
        }
    }
}

m25ThreadInsDiam    = 4.6;
m25ThreadInsMinWall = 1.6;
m25ThreadInsColDiam = m25ThreadInsDiam + m25ThreadInsMinWall*2;
m25ThreadInsHoleDepth = 5.0;
m25ThreadInsHoleDiam = 4.05;
reanShaftLenNet     = 24.0;
stampXExc = stampX - boardWidth/2;  //  boardWidth  - (nfcPad + nfcExtent/2);
stampYExc = stampY - boardHeight/2; // boardHeight - (nfcPad + nfcExtent/2);
m25HeadDiam = 5.0;
m25HeadHeight = 1.5;
m25Diam = 2.5 + printTol;

//difference() {
//    cylinder(h = reanShaftLenNet + 0.04, r = m25ThreadInsColDiam/2, center = false);
//    translate([0, 0, reanShaftLenNet - m25ThreadInsHoleDepth + 0.04]) {
//        cylinder(h = m25ThreadInsHoleDepth + 0.01, r = m25ThreadInsHoleDiam/2, center = false);
//    }
//}

module heapBase(ai = 0) {
    w1 = ai == 0 ? heapWidth1  : heapWidth1a1;
    h1 = ai == 0 ? heapHeight1 : heapHeight1a1;
    difference() {
        heapBaseSurface(w1 = w1, h1 = h1);
        union() {
            for (j=[0:1]) {
                for (i=[0:3]) {
                    dx = (i == 0 || i == 2) ? 0 : (boardHeightB/2 + 5) * (i == 1 ? -1 : 1);
                    dy = (i == 1 || i == 3) ? 0 : (boardWidthB /2 + 5) * (i == 0 ? -1 : 1);
                    dz = j == 0 ? -0.01 : platformZ - m25ThreadInsHoleDepth;
                    translate([dx, dy, dz]) {
                        cylinder(h = m25ThreadInsHoleDepth + 0.01, r = m25ThreadInsHoleDiam/2, center = false);
                    }
                }
            }
        }
    }
}

module heapTop(ai = 0) {
    difference() {
        translate([(boardWidthB/2), (boardHeightB/2)]) {
            rotate([0, 0, 90]) {
                translate([-(boardWidthB/2), -(boardHeightB/2)]) {
                    difference() {
                        union() {
                            ellipseSegmTri(z1 = 0.0, w1 = heapWidth2, h1 = heapHeight2, z2 = platformZ2, w2 = heapWidth3, h2 = heapHeight3, a1start = 0, a1stop = 360, a2start = 0, a2stop = 360, num = ppc,
                            cx1 = boardWidthB/2, cy1 = boardHeightB/2,
                            cx2 = boardWidthB/2 + stampYExc, cy2 = boardHeightB/2 - stampXExc
                            );
                            if (ai == 1) {
                                ellipseSegmTri(z1 = platformZ2a1, w1 = heapWidth2a1, h1 = headpWidth2a1, z2 = platformZ3a1, w2 = heapWidth3, h2 = heapHeight3, a1start = 0, a1stop = 360, a2start = 0, a2stop = 360, num = ppc,
                                cx1 = boardWidthB/2 + stampYExc/2, cy1 = boardHeightB/2 - stampXExc/2,
                                cx2 = boardWidthB/2 + stampYExc, cy2 = boardHeightB/2 - stampXExc
                                );
                            }
                        }
                        union() {
                            translate([(boardWidthB/2), (boardHeightB/2)]) {
                                for (i=[0:3]) {
                                    dx = (i == 0 || i == 2) ? 0 : (boardHeightB/2 + 5) * (i == 1 ? -1 : 1);
                                    dy = (i == 1 || i == 3) ? 0 : (boardWidthB /2 + 5) * (i == 0 ? -1 : 1);
                                    dz = -0.01; // j == 0 ? -0.01 : platformZ - m25ThreadInsHoleDepth;
                                    translate([dx, dy, dz]) {
                                        union() {
                                            cylinder(h = platformZ3a1 + 0.02, r = m25Diam/2, center = false);
                                            translate([0, 0, 2]) {
                                                cylinder(h = platformZ3a1 -2 + 0.02, r = m25HeadDiam/2, center = false);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        translate([wallThick, wallThick, -0.01]) {
            union() {
                translate([stampX, stampY, 0]) {
                    cylinder(h = platformZ3a1 + 0.02, r = stampOuterR + printTol);
                }
                smooth = 1;
                translate([smooth, boardHeight - (cableExtent + cablePad - smooth), 0]) {
                    minkowski() {
                        cube([cableWide - smooth*2, cableExtent - smooth*2, platformZ2 + 0.02]);
                        cylinder(r=smooth,h=0.001);
                    }
                }
                translate([hole3Dist, boardHeight - holePad, -1]) {
                    cylinder(h = m25HeadHeight + 0.5, r = m25HeadDiam/2 + printTol);
                }
                translate([boardWidth - holePad, holePad, -1]) {
                    cylinder(h = m25HeadHeight + 0.5, r = m25HeadDiam/2 + printTol);
                }
                translate([boardWidth - holePad, boardHeight - holePad, -1]) {
                    cylinder(h = m25HeadHeight + 0.5, r = m25HeadDiam/2 + printTol);
                }
            }
        }
    }
}

module heapBottom(ai = 0) {
    difference() {
        w2 = ai == 0 ? heapWidth1  : heapWidth1a1;
        h2 = ai == 0 ? heapHeight1 : heapHeight1a1;
        w1 = ai == 0 ? w2 * 1.04 : w2 * 1.03;
        h1 = ai == 0 ? h2 * 1.04 : h2 * 1.06;
        z  = ai == 0 ? platformZ0 : platformZ0a1;
        heapBaseSurface(w1 = w1, h1 = h1, w2 = w2, h2 = h2, 
            z = z, cubeZ = 1.01);
        union() {
            for (i=[0:3]) {
                dx = (i == 0 || i == 2) ? 0 : (boardHeightB/2 + 5) * (i == 1 ? -1 : 1);
                dy = (i == 1 || i == 3) ? 0 : (boardWidthB /2 + 5) * (i == 0 ? -1 : 1);
                dz = -0.01; // j == 0 ? -0.01 : platformZ - m25ThreadInsHoleDepth;
                translate([dx, dy, dz + 1.5]) {
                    union() {
                        cylinder(h = m25ThreadInsHoleDepth + 0.12, r = m25Diam/2, center = false);
                        cylinder(h = 1, r2 = m25Diam/2, r1 = m25HeadDiam/2, center = false);
                        translate([0, 0, -2]) {
                            cylinder(h = 2 + 0.01, r = m25HeadDiam/2, center = false);
                        }
                    }
                }
            }
        }
    }
}

module heap(ai = 0) {
    z = ai == 0 ? platformZ0 : platformZ0a1;
    translate([0, 0, z]) {
        translate([(boardWidthB/2), (boardHeightB/2)]) {
            rotate([0, 0, 90]) {
                heapBase(ai = ai);
                translate([0, 0, -z]) {
                    heapBottom(ai = ai);
                }
            }
        }
        translate([0, 0, platformZ]) {
            heapTop(ai = ai);
        }
    }
}

module insertStamp() {
    l = 9.5;
    difference() {
        cylinder(h = l, r = stampOuterR);
        translate([0, 0, -1]) {
            cylinder(h = l + 2, r = stampDiam/2);
        }
    }
}

//heapTop(ai = 1);
//heapBase(ai = 1);
//heapBottom(ai = 0);
//heap(ai = 1);
insertStamp();

//heap(ai = 0);
//translate([0, 120, 0]) {
//    heap(ai = 1);
//}

//translate([wallThick, wallThick, -(boardDepth /* + wallThick */) + 0.01]) {
//    all();
//}

//echo(platformZ);
//echo(platformZ2);
