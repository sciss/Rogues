// Regular OpenSCAD Variables
$fn = 180;// Quality

ringDiaOutside = 212.0;
ringThick      =   4.0;
ringHeight     =  10.0;

module all(){
    difference() {
        cylinder(h = ringHeight, r = ringDiaOutside/2);
        translate([0, 0, -1]) {
            cylinder(h = ringHeight + 2, r = ringDiaOutside/2 - ringThick);
        }
    }
}

all();
