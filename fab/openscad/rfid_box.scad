// Regular OpenSCAD Variables
$fn = 40;// Quality

stampDiam   = 33.0;
stampLen    = 30.0;
boardWidth  = 85.0;
boardHeight = 56.0;
boardDepth  =  9.5;
baseDepth   = 14.0;
wallThick   =  2.0;
nfcExtent   = 36.0;
holePad     =  3.5;
hole3Dist   = 23.5;
holeDiam    =  3.0;
nfcPad      =  6.7;
printTol    = 0.25;
cablePad    = 6.5;
cableExtent = 2.54 * 5;
cableWide   = 7.0;
baseDuctDepth = 6.0;

// select part
setPart_drop_down_box = "body";  // [body,lid,base]

part        = setPart_drop_down_box;

stampOuterR = (stampDiam + wallThick*2)/2;
stampX      = boardWidth  - (nfcPad + nfcExtent/2);
stampY      = boardHeight - (nfcPad + nfcExtent/2);
basicDepth  = (part == "body") ? boardDepth : ((part == "base") ? baseDepth : wallThick);
ductDepth   = (part == "base") ? baseDuctDepth : basicDepth;

module screwDuct() {
    difference() {
        cylinder(h = ductDepth, r = (holeDiam + wallThick*2)/2);
        translate([0, 0, -1]) {
            cylinder(h = ductDepth + 2, r = holeDiam/2);
        }
    }
}

module base() {
    minkowski() {
        cube([boardWidth,boardHeight,basicDepth]);
        cylinder(r=wallThick,h=0.001);
    }
}

module all(){
    difference() {
        if (part == "body" || part == "base") {
            difference() {
                base();
                dz = part == "body" ? wallThick : -wallThick*2; 
                translate([wallThick, wallThick, dz]) {
                    minkowski() {
                        cube([boardWidth - wallThick*2,boardHeight - wallThick*2,basicDepth + 2]);
                        cylinder(r=wallThick,h=0.001);
                    }
                }
            }
        } else {
            base();
        }
        union() {
            translate([hole3Dist, boardHeight - holePad, -1]) {
                cylinder(h = basicDepth + 2, r = holeDiam/2);
            }
            translate([boardWidth - holePad, holePad, -1]) {
                cylinder(h = basicDepth + 2, r = holeDiam/2);
            }
            translate([boardWidth - holePad, boardHeight - holePad, -1]) {
                cylinder(h = basicDepth + 2, r = holeDiam/2);
            }
            if (part == "body" || part == "base") {
                // make space for electronic parts
                dz = part == "body" ? -1: baseDepth - wallThick;
                translate([0, 0, dz]) {
                    union() {
                        cube([19, boardHeight, boardDepth + 2]);
                        translate([0, (boardHeight - 37.0)/2, 0]) {
                            cube([42.5, 37.0, boardDepth + 2]);
                        }
                        translate([26, 0, 0]) {
                            cube([79-26, 7, boardDepth + 2]);
                        }
                    }
                }
            } else if (part == "lid") {
                translate([0, 0, -1]) {
                    translate([stampX, stampY, -1]) {
                        cylinder(h = stampLen + 2, r = stampOuterR + printTol);
                    }
                    smooth = 1;
                    translate([smooth, boardHeight - (cableExtent + cablePad - smooth), 0]) {
                        minkowski() {
                            cube([cableWide - smooth*2, cableExtent - smooth*2, wallThick + 2]);
                            cylinder(r=smooth,h=0.001);
                        }
                    }
                }
            }
        }
    }
    if (part == "body" || part == "base") {
        dz = basicDepth - ductDepth; // basicDepth - boardDepth;
        translate([hole3Dist, boardHeight - holePad, dz]) {
            screwDuct();
        }
        translate([boardWidth - holePad, holePad, dz]) {
            screwDuct();
        }
        translate([boardWidth - holePad, boardHeight - holePad, dz]) {
            screwDuct();
        }
    }
    translate([stampX, stampY, 0]) {
        if (part == "body") {
            difference() {
                cylinder(h = stampLen, r = stampOuterR);
                translate([0, 0, -1]) {
                    cylinder(h = stampLen + 2, r = stampDiam/2);
                }
            }
        }
    }
}

all();
