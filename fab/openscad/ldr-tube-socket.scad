// https://github.com/BelfrySCAD/BOSL2/
include <BOSL2/std.scad>

// Regular OpenSCAD Variables
$fn = 72; // 180;// Quality

ldrLongSide     = 5.5; // 5.0; // incl print tolerance
ldrConnSpaceOut = 3.84; //
ldrConnDiam     = 0.48; // (0.5mm)
ldrConnSpace    = ldrConnSpaceOut - ldrConnDiam;
ldrHeadHeight   = 2.5; // 2.0

ldrConnHoleDiam = ldrConnDiam + 0.5;
ldrSocketWall   = 1;
ldrSocketDuctLen = 2;
ldrOuterDiam     = ldrLongSide + 2*ldrSocketWall;

ledDiam         = 7.0; // 6.8; // 5.5; // 5.0; // incl print tolerance
ledConnSpace    = 2.54;
ledConnDiam     = 0.55;
ledConnHoleDiam = ledConnDiam + 0.5;
ledSocketWall   = 1;
ledHeadHeight   = 8.8;
ledSocketDuctLen = 2;
ledOuterDiam     = ledDiam + 2*ledSocketWall;

compOuterDiam    = max(ldrOuterDiam, ledOuterDiam);

ringLen         = compOuterDiam; // 6.0
ringInnerDiam   = 8.5; // 10; // 8; // 19;
ringWall        = 2;
ringOuterDiam   = ringInnerDiam + 2 * ringWall;
ringNominalDiam = (ringInnerDiam + ringOuterDiam)/2;
ringBreak       = 4; // 6;

// echo(ringLen);

ldrSocketOpenLen = ledHeadHeight; // ringLen - ldrSocketDuctLen;
ledSocketOpenLen = ledHeadHeight; // ringLen - ledSocketDuctLen;

screwPlateThick = 3.0;
screwHoleDiam   = 2.8;
screwPlateWidth = screwHoleDiam + 4;

module sensorRing(cut = true, dr = 0, screws = false) {
    difference() {
        union() {
            difference() {
                cylinder(h = ringLen, r = ringOuterDiam/2 + dr, center = false);
                translate([0, 0, -0.01]) {
                    cylinder(h = ringLen + 0.02, r = ringInnerDiam/2 + dr, center = false);
                }
            }
            if (screws) {
                for (hi=[0:1]) {
                    rotate([0, 0, hi * 180]) {
                        translate([0, ringNominalDiam/2 + screwPlateWidth/2, ringLen/2]) {
                            difference() {
                                cube([screwPlateThick, screwPlateWidth, ringLen], center = true);
                                rotate([0, 90, 0]) {
                                    translate([0.0, 0.0, -0.01]) {
                                        cylinder(h = screwPlateThick + 0.04, r = screwHoleDiam/2, center = true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (cut) translate([-(ringNominalDiam/2 + dr), 0, -0.01]) {
            cube([ringWall * 2 + dr + 0.01, ringBreak, (ringLen + 0.02)*2], center = true);
        }
    }
}

module ledSocketInner() {
    union() {
        translate([0, 0, ledSocketDuctLen]) {
            cylinder(h = ledSocketOpenLen + 0.01, r = ledDiam/2, center = false);
        }
        for (hi=[-1:2:1]) {
            translate([0, ledConnSpace/2 * hi, -0.01]) {
                cylinder(h = ledSocketDuctLen + 0.02, r = ledConnHoleDiam/2, center = false);
            }
        }
    }
}

module ledSocket() {
    difference() {
        cylinder(h = ledSocketDuctLen + ledHeadHeight, r = compOuterDiam/2, center = false);
        ledSocketInner();
    }
}

module ldrSocketInner() {
    union() {
        translate([0, 0, ldrSocketDuctLen]) {
            cylinder(h = ldrSocketOpenLen + 0.01, r = ldrLongSide/2, center = false);
        }
        for (hi=[-1:2:1]) {
            translate([0, ldrConnSpace/2 * hi, -0.01]) {
                cylinder(h = ldrSocketDuctLen + 0.02, r = ldrConnHoleDiam/2, center = false);
            }
        }
    }
}

module ldrSocket() {
    difference() {
        cylinder(h = ldrSocketDuctLen + ldrHeadHeight, r = compOuterDiam/2, center = false);
        ldrSocketInner();
    }
}

module sensorRingComplete() {
    union() {
        difference() {
            sensorRing(cut = false, screws = true);
            union() {
                translate([0.0 /* + ldrSocketWall/2 */, 0, (ringLen)/2]) {
                    rotate([90, 0, 90]) {
                        ldrSocketInner();
                    }
                }
                translate([0.0 /* + ldrSocketWall/2 */, 0, (ringLen)/2]) {
                    rotate([90, 0, -90]) {
                        ledSocketInner();
                    }
                }
            }
        };
        translate([ldrSocketDuctLen + ldrHeadHeight + ringInnerDiam/2 /* + ldrSocketWall/2 */, 0, (ringLen)/2]) {
            rotate([-90, 0, 90]) {
                ldrSocket();
            }
        }
        translate([-(ledSocketDuctLen + ledHeadHeight + ringInnerDiam/2) /* + ldrSocketWall/2 */, 0, (ringLen)/2]) {
            rotate([-90, 0, -90]) {
                ledSocket();
            }
        }
    }
}

module sensorRingLEDHalf() {
    intersection() {
        sensorRingComplete();
        translate([-50, -25, 0]) {
            cube(50);
        }
    }
}

module sensorRingLDRHalf() {
    intersection() {
        sensorRingComplete();
        translate([0, -25, 0]) {
            cube(50);
        }
    }
}

module sensorRingHalves() {
    union() {
        translate([-2, 0, 0]) {
            sensorRingLEDHalf();
        }
        translate([2, 0, 0]) {
            sensorRingLDRHalf();
        }
    }
}

module darkenRingHalf1() {
    intersection() {
        sensorRing(cut = false, screws = true);
        translate([0, -25, 0]) {
            cube(50);
        }
    }
}

module darkenRingHalf2() {
    intersection() {
        sensorRing(cut = false, screws = true);
        translate([-50, -25, 0]) {
            cube(50);
        }
    }
}

module darkenRingHalves() {
    union() {
        translate([-2, 0, 0]) {
            darkenRingHalf2();
        }
        translate([2, 0, 0]) {
            darkenRingHalf1();
        }
    }
}

module all() {
    sensorRingHalves();
    for (si=[-1:2:1]) {
        translate([0, (ringOuterDiam + 4 + screwPlateWidth + 4) * si, 0]) {
            darkenRingHalves(); // (dr = 0.0 /* -0.5 */, cut = false, screws = true);
        }
    }
}

//sensorRingLEDHalf();
all();

//sensorRingComplete();
//sensorRing();
//ldrSocket();
//ledSocket();
