// Regular OpenSCAD Variables
$fn = 40;// Quality

module drawDoubleTile(symbol1, symbol2, xOffset = 0, yOffset = 0){
  translate([(xOffset * (keySize + tileSpacing)), -yOffset * (keySize + tileSpacing), 0]){
    backplate();
    intersection(){
      Char_display_standard2(symbol1, symbol2);
      translate([0, 0, -baseHeight / 100])scale([1, 1, 10 * scaleBraille])backplate();
    }

  }
}

screwSpacingRadius = 37.0;
screwHoleDiam      = 3.0;
screwWallThickness = 2.0;
screwOuterDiam     = screwHoleDiam + 2*screwWallThickness;
ringInnerDiam      = 60.0;
ringOuterDiam      = 71.25;
ledDistance        = 17.0;
plateThickness     = 2.5;
ductLength         = ledDistance + plateThickness;
circuitHoleH       = 25.0;
circuitHoleV       = 19.5;
circuitRota        = 22.5;
circuitHoleDiam    = 3.0;
brassRadius        = 22.0;
sideHoleDiam       = 5.0;
coolingHoleDiam    = 20.0;

module backplate() {
  difference() {
    cylinder(h = ductLength, r = ringOuterDiam/2);
    union() {
      translate([0, 0, -1]) {
        rotate([0, 0, circuitRota]) {
          translate([-circuitHoleH/2, -circuitHoleV/2, 0]) {
            cylinder(h = ductLength + 2, r = circuitHoleDiam/2);
          }
          translate([circuitHoleH/2, circuitHoleV/2, 0]) {
            cylinder(h = ductLength + 2, r = circuitHoleDiam/2);
          }
        }
        for(i = [0:3]){
          rotate([0, 0, i * 90]) {
            translate([brassRadius, 0, 0]) {
              cylinder(h = ductLength + 2, r = circuitHoleDiam/2);
            }
          }
        }
        cylinder(h = ductLength + 2, r = coolingHoleDiam/2);
      }
    }
  }
}

module screwOuter() {
  cylinder(h = ductLength, r = screwOuterDiam/2);
}

module screwInner() {
  translate([0, 0, -1]) {
    cylinder(h = ductLength + 2, r = screwHoleDiam/2);
  }
}

module screwDuct() {
  difference() {
    cylinder(h = ductLength, r = screwOuterDiam/2);
    translate([0, 0, -1]) {
      cylinder(h = ductLength + 2, r = screwHoleDiam/2);
    }
  }
}

module all(){
  difference() {
    union() {
      for(i = [0:3]){
        rotate([0, 0, i * 90]) {
          translate([screwSpacingRadius, 0, 0]) {
            screwOuter();
          }
        }
      }
      difference() {
        backplate();
        translate([0, 0, plateThickness]) {
          cylinder(h = ledDistance + 1, r = ringInnerDiam/2);
        }
      }
    }
    union() {
      for(i = [0:3]){
        rotate([0, 0, i * 90]) {
          translate([screwSpacingRadius, 0, 0]) {
            screwInner();
          }
        }
      }
      rotate([0, 0, circuitRota]) {
        translate([0, 0, sideHoleDiam]) {
          union() {
            for(i = [0:1]){
              rotate([0, i * 180 -90, 0]) {
                cylinder(h = ringOuterDiam/2 + 1, r = sideHoleDiam/2);
              }
            }
          }
        }
      }
    }
  }
}

all();
