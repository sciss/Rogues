# these are now corresponding to the board revision 2

import time
import board
import digitalio
import neopixel

ledInt = digitalio.DigitalInOut(board.LED)
ledInt.direction = digitalio.Direction.OUTPUT

ledExt = digitalio.DigitalInOut(board.GP0)
ledExt.direction = digitalio.Direction.OUTPUT
ledExt.value = False

relay = digitalio.DigitalInOut(board.GP18)
relay.direction = digitalio.Direction.OUTPUT
relay.value = False

but = digitalio.DigitalInOut(board.GP15)
but.switch_to_input(pull=digitalio.Pull.UP) # they close to GND

# On CircuitPlayground Express, and boards with built in status NeoPixel -> board.NEOPIXEL
# Otherwise choose an open pin connected to the Data In of the NeoPixel strip, i.e. board.D1
pixel_pin = board.GP19 # GP18

# The number of NeoPixels
num_pixels = 1

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
ORDER = neopixel.GRB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=ORDER
)

butState = True


def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos * 3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos * 3)
        g = 0
        b = int(pos * 3)
    else:
        pos -= 170
        r = 0
        g = int(pos * 3)
        b = int(255 - pos * 3)
    return (r, g, b) if ORDER in (neopixel.RGB, neopixel.GRB) else (r, g, b, 0)

wheelPos = 0

while True:
    ledInt.value = True
    time.sleep(0.05)
    ledInt.value = False
    time.sleep(0.25)

    for i in range(num_pixels):
        pixel_index = (i * 256 // num_pixels) + wheelPos
        pixels[i] = wheel(pixel_index & 255)
    pixels.show()
    wheelPos = (wheelPos + 2) % 255

    if (not (butState is but.value)):
        butState = not butState
        if not butState:
            ledExt.value = not relay.value
            relay.value  = not relay.value
            ledInt.value = True
            time.sleep(0.5)
            ledInt.value = False
            time.sleep(0.25)
