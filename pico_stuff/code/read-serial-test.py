import time
import board
import busio
import digitalio
import neopixel
import usb_cdc

ledInt = digitalio.DigitalInOut(board.LED)
ledInt.direction = digitalio.Direction.OUTPUT

butWait = digitalio.DigitalInOut(board.GP15)
butWait.switch_to_input(pull=digitalio.Pull.UP) # they close to GND

# press button to avoid program
if (not butWait.value):
    while True:
        ledInt.value = True
        time.sleep(0.1)
        ledInt.value = False
        time.sleep(0.4)

print("Hello from read-serial-test. Version: 15-Aug-2022, 16:00h")

# On CircuitPlayground Express, and boards with built in status NeoPixel -> board.NEOPIXEL
# Otherwise choose an open pin connected to the Data In of the NeoPixel strip, i.e. board.D1
pixel_pin = board.GP16 # GP18

# The number of NeoPixels
num_pixels = 1

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
pixelOrder = neopixel.RGB # GRB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=pixelOrder
)

ledInt.value = True
time.sleep(0.5)
ledInt.value = False
time.sleep(0.5)

# numColors  = 4
# colorRed   = [255, 0, 0, 128]
# colorGreen = [0, 255, 0, 128]
# colorBlue  = [0, 0, 255, 128]
# colorIdx   = 0

pixels.fill((0, 0, 0))
pixels.show()

serial = usb_cdc.data
# serial.write_timeout = None
serial.timeout = None  # block indefinitely on read

bufSz       = 11  # 'cRRRGGGBBB\n'
buf         = bytearray(bufSz)
bufTmp      = bytearray(bufSz)
bufOff      = 0
bufValid    = True

samplePeriod = 0.05 # 20 Hz

ledCnt = 0
ledExt1 = digitalio.DigitalInOut(board.GP0)
ledExt1.direction = digitalio.Direction.OUTPUT

while True:
    # bytes = serial.readline()

    # if (bytes is not None):
    #     string = bytes.decode('utf-8')
    #     if (string.startswith('color')):
    #         pixels.fill((colorGreen[colorIdx], colorRed[colorIdx], colorBlue[colorIdx]))
    #         colorIdx = (colorIdx + 1) % numColors
    #         pixels.show()
        
    # numBytes = serial.in_waiting
    if (serial.in_waiting > 0):
        while (serial.in_waiting > 0):
            # chunkMax = min(bufSz, numBytes)
            chunk = serial.readinto(bufTmp) # "ValueError: length argument not allowed for this type", blablabla, Python go to hell (chunkMax)
            # print("chunk ", chunk)
            # numBytes -= chunk
            for ri in range(chunk):
                byte = bufTmp[ri]
                if bufValid:
                    buf[bufOff] = byte
                    bufOff += 1
                    if (byte == 10):    # 10 == '\n'
                        if ((bufOff == bufSz) and (buf[0] == 99)):  # 99 == 'c'
                            colorRed   = (buf[1] - 48) * 100 + (buf[2] - 48) * 10 + (buf[3] - 48)
                            colorGreen = (buf[4] - 48) * 100 + (buf[5] - 48) * 10 + (buf[6] - 48)
                            colorBlue  = (buf[7] - 48) * 100 + (buf[8] - 48) * 10 + (buf[9] - 48)
                            # tup        = (colorGreen, colorRed, colorBlue)  # GRB
                            tup        = (colorRed, colorGreen, colorBlue)  # RGB
                            # print(tup)
                            pixels.fill(tup)
                            pixels.show()

                        bufOff = 0  # begin new line
 
                    elif (bufOff == bufSz):
                        bufValid = False  # junk/overflow, need to wait for next newline to resume

                elif (byte == 10):  # buffer becomes valid
                    bufOff   = 0
                    bufValid = True # begin new line


    else:
        time.sleep(samplePeriod)
        ledCnt = ledCnt + 1
        if (ledCnt == 20):
            ledExt1.value = True
        elif (ledCnt == 21):
            ledExt1.value = False
            ledCnt = 0
