# these are now corresponding to the board revision 2

import time
import board
import digitalio
 
ledInt = digitalio.DigitalInOut(board.LED)
ledInt.direction = digitalio.Direction.OUTPUT

ledExt = digitalio.DigitalInOut(board.GP0)
ledExt.direction = digitalio.Direction.OUTPUT
ledExt.value = False

relay = digitalio.DigitalInOut(board.GP18)
relay.direction = digitalio.Direction.OUTPUT
relay.value = False

but = digitalio.DigitalInOut(board.GP15)
but.switch_to_input(pull=digitalio.Pull.UP) # they close to GND

# ledInt.value = True
# time.sleep(0.2)
# ledInt.value = False

butState = True

while True:
    ledInt.value = True
    time.sleep(0.05)
    ledInt.value = False
    time.sleep(0.25)
    if (not (butState is but.value)):
        butState = not butState
        if not butState:
            ledExt.value = not relay.value
            relay.value  = not relay.value
            ledInt.value = True
            time.sleep(0.5)
            ledInt.value = False
            time.sleep(0.25)
