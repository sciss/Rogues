# swap-rogues.py
# (Rogues)
#
# Copyright (c) 2021-2022 Hanns Holger Rutz. All rights reserved.
#
# This software is published under the GNU Affero General Public License v3+
#
#
# For further information, please contact Hanns Holger Rutz at
# contact@sciss.de

import time
import board
import busio
import digitalio
import adafruit_ads1x15.ads1115 as ADS
import touchio
import neopixel
from adafruit_ads1x15.analog_in import AnalogIn
import usb_cdc
from adafruit_ticks import ticks_ms, ticks_add, ticks_diff

version         = "06-Nov-2022, 23:45h"  # window

samplePeriodMs  = 50 # milliseconds; equivalent to 20 Hz ; note: Pico is actually slower :(

# this file has pin layout like the PCB v1

ledInt = digitalio.DigitalInOut(board.LED)
ledInt.direction = digitalio.Direction.OUTPUT

butWait = digitalio.DigitalInOut(board.GP15)
butWait.switch_to_input(pull=digitalio.Pull.UP) # they close to GND

# press button to avoid program
if (not butWait.value):
    while True:
        ledInt.value = True
        time.sleep(0.1)
        ledInt.value = False
        time.sleep(0.4)

butAux = digitalio.DigitalInOut(board.GP14)
butAux.switch_to_input(pull=digitalio.Pull.UP) # they close to GND

butOff = digitalio.DigitalInOut(board.GP13)
butOff.switch_to_input(pull=digitalio.Pull.UP) # they close to GND

ledExt1 = digitalio.DigitalInOut(board.GP0)
ledExt1.direction = digitalio.Direction.OUTPUT
ledExt2 = digitalio.DigitalInOut(board.GP1)
ledExt2.direction = digitalio.Direction.OUTPUT

# Create the I2C bus
pinSCA = board.GP26 # GP20 # GP16
pinSCL = board.GP27 # GP21 # GP17
i2c = busio.I2C(scl = pinSCL, sda = pinSCA)

# Create the ADC object using the I2C bus
ads1 = ADS.ADS1115(i2c, address=0x48)
ads2 = ADS.ADS1115(i2c, address=0x49)

# Create single-ended input on channel 0
ldr_ch1 = AnalogIn(ads1, ADS.P0)
ldr_ch2 = AnalogIn(ads1, ADS.P1)
ldr_ch3 = AnalogIn(ads1, ADS.P2)

ldr_ch4 = AnalogIn(ads2, ADS.P0)
ldr_ch5 = AnalogIn(ads2, ADS.P1)
ldr_ch6 = AnalogIn(ads2, ADS.P2)

numSensorsLDR = 6
sensorsLDR = [ldr_ch1, ldr_ch2, ldr_ch3, ldr_ch4, ldr_ch5, ldr_ch6]

ledInt.value = True
time.sleep(0.5)
ledInt.value = False
time.sleep(0.5)

cap_ch1 = touchio.TouchIn(board.GP2)
cap_ch2 = touchio.TouchIn(board.GP3)
cap_ch3 = touchio.TouchIn(board.GP4)

cap_ch4 = touchio.TouchIn(board.GP7)    # GP6
cap_ch5 = touchio.TouchIn(board.GP10)   # GP7
cap_ch6 = touchio.TouchIn(board.GP12)   # GP8

numSensorsCap = 6
sensorsCap = [cap_ch1, cap_ch2, cap_ch3, cap_ch4, cap_ch5, cap_ch6]

ledInt.value = True
time.sleep(0.5)
ledInt.value = False
time.sleep(0.5)

print("Swap Rogues -", version)

# On CircuitPlayground Express, and boards with built in status NeoPixel -> board.NEOPIXEL
# Otherwise choose an open pin connected to the Data In of the NeoPixel strip, i.e. board.D1
pixel_pin = board.GP19

# The number of NeoPixels
num_pixels = 1

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
pixelOrder = neopixel.RGB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=pixelOrder
)

# relays
pinFan = digitalio.DigitalInOut(board.GP18)
pinFan.direction = digitalio.Direction.OUTPUT
pinFan.value = False

ledInt.value = True
time.sleep(0.5)
ledInt.value = False
time.sleep(0.5)

pixels.fill((0, 0, 0))
pixels.show()

ledCnt = 0

serial = usb_cdc.data
serial.timeout          = 0.2 # None
serial.write_timeout    = 0.2 # None

numSensors = numSensorsLDR + numSensorsCap

bufOutSz = (numSensors * 6) + 2   # 5 digits, a space; followed by a digit for the buttons and newline
bufOut = bytearray(bufOutSz)
for si in range(numSensors):
    bufOut[si * 6 + 5] = 32 # space

bufOut[bufOutSz - 1] = 10 # newline

bufInSz     = 11  # 'cRRRGGGBBB\n'
bufIn       = bytearray(bufInSz)
bufInTmp    = bytearray(bufInSz)
bufInOff    = 0
bufInValid  = True

ticksPrev = ticks_ms()

while True:
    # ---- handle input ----

    if (serial.in_waiting > 0):
        while (serial.in_waiting > 0):
            chunk = serial.readinto(bufInTmp) # "ValueError: length argument not allowed for this type", blablabla, Python go to hell (chunkMax)
            # print("chunk ", chunk)
            # numBytes -= chunk
            for ri in range(chunk):
                byte = bufInTmp[ri]
                if bufInValid:
                    bufIn[bufInOff] = byte
                    bufInOff += 1
                    if (byte == 10):    # 10 == '\n'
                        if (bufInOff == bufInSz):
                            cmd = bufIn[0]
                            if (cmd == 99):  # 99 == 'c'
                                colorRed   = (bufIn[1] - 48) * 100 + (bufIn[2] - 48) * 10 + (bufIn[3] - 48)
                                colorGreen = (bufIn[4] - 48) * 100 + (bufIn[5] - 48) * 10 + (bufIn[6] - 48)
                                colorBlue  = (bufIn[7] - 48) * 100 + (bufIn[8] - 48) * 10 + (bufIn[9] - 48)
                                tup        = (colorRed, colorGreen, colorBlue)  # RGB
                                # print(tup)
                                # pixels.fill(tup)
                                pixels[0] = tup
                                pixels.show()
                            elif (cmd == 114):  # 114 == 'r'
                                fanOn = (bufIn[1] - 48) > 0
                                pinFan.value = fanOn
                                ledExt2.value = fanOn

                        bufInOff = 0  # begin new line

                    elif (bufInOff == bufInSz):
                        bufInValid = False  # junk/overflow, need to wait for next newline to resume

                elif (byte == 10):  # buffer becomes valid
                    bufInOff   = 0
                    bufInValid = True # begin new line

    # ---- handle output ----

    for si in range(numSensorsLDR):
        try:    # we have trouble with some ADS 'loose contact'
            value   = sensorsLDR[si].value
        except:
            value   = 0

        b5      = value // 10000
        value   = value % 10000
        bi      = si * 6
        bufOut[bi + 0] = b5 + 48
        b4      = value // 1000
        value   = value % 1000
        bufOut[bi + 1] = b4 + 48
        b3      = value // 100
        value   = value % 100
        bufOut[bi + 2] = b3 + 48
        b2      = value // 10
        value   = value % 10
        bufOut[bi + 3] = b2 + 48
        b1      = value
        bufOut[bi + 4] = b1 + 48

    for si in range(numSensorsCap):
        value   = sensorsCap[si].raw_value
        b5      = value // 10000
        value   = value % 10000
        bi      = (si + numSensorsLDR) * 6
        bufOut[bi + 0] = b5 + 48
        b4      = value // 1000
        value   = value % 1000
        bufOut[bi + 1] = b4 + 48
        b3      = value // 100
        value   = value % 100
        bufOut[bi + 2] = b3 + 48
        b2      = value // 10
        value   = value % 10
        bufOut[bi + 3] = b2 + 48
        b1      = value
        bufOut[bi + 4] = b1 + 48

    butCode = int(not butOff.value) + (int(not butAux.value) * 2) + (int(not butWait.value) * 4)
    bi = bufOutSz - 2
    bufOut[bi] = butCode + 48

    serial.write(bufOut)

    ticksNext = ticks_ms()
    # print('ticksNext', ticksNext)
    ticksWait = samplePeriodMs - ticks_diff(ticksNext, ticksPrev)
    # print('ticksWait', ticksWait)
    # just grab the new ticks value, because the Pico might be too slow to perform at desired sampling rate
    if (ticksWait > 0):
        time.sleep(ticksWait * 0.001)
        ticksPrev = ticks_add(ticksNext, ticksWait)
    else:
        tricksPrev = ticksNext

    ledCnt = ledCnt + 1
    if (ledCnt == 20):
        ledExt1.value = True
    elif (ledCnt == 21):
        ledExt1.value = False
        ledCnt = 0
