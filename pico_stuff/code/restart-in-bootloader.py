# https://github.com/todbot/circuitpython-tricks#programmatically-reset-to-uf2-bootloader
import microcontroller
microcontroller.on_next_reset(microcontroller.RunMode.BOOTLOADER)
microcontroller.reset()
