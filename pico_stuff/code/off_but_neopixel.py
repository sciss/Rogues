import time
import board
import busio
import digitalio
import adafruit_ads1x15.ads1115 as ADS
import touchio
import neopixel
# import usb_cdc
from adafruit_ads1x15.analog_in import AnalogIn
import usb_cdc

# this file has pin layout like the PCB v1

ledInt = digitalio.DigitalInOut(board.LED)
ledInt.direction = digitalio.Direction.OUTPUT

butWait = digitalio.DigitalInOut(board.GP15)
butWait.switch_to_input(pull=digitalio.Pull.UP) # they close to GND

# press button to avoid program
if (not butWait.value):
    while True:
        ledInt.value = True
        time.sleep(0.1)
        ledInt.value = False
        time.sleep(0.4)

samplePeriod = 0.05 # 20 Hz

butAux = digitalio.DigitalInOut(board.GP14)
butAux.switch_to_input(pull=digitalio.Pull.UP) # they close to GND

butOff = digitalio.DigitalInOut(board.GP13)
butOff.switch_to_input(pull=digitalio.Pull.UP) # they close to GND

ledExt1 = digitalio.DigitalInOut(board.GP0)
ledExt1.direction = digitalio.Direction.OUTPUT
ledExt2 = digitalio.DigitalInOut(board.GP1)
ledExt2.direction = digitalio.Direction.OUTPUT

ledInt.value = True
time.sleep(0.5)
ledInt.value = False
time.sleep(0.5)

print("off_but_neopixel")

# On CircuitPlayground Express, and boards with built in status NeoPixel -> board.NEOPIXEL
# Otherwise choose an open pin connected to the Data In of the NeoPixel strip, i.e. board.D1
pixel_pin = board.GP16 # GP18

# The number of NeoPixels
num_pixels = 1

# The order of the pixel colors - RGB or GRB. Some NeoPixels have red and green reversed!
# For RGBW NeoPixels, simply change the ORDER to RGBW or GRBW.
pixelOrder = neopixel.GRB

pixels = neopixel.NeoPixel(
    pixel_pin, num_pixels, brightness=0.2, auto_write=False, pixel_order=pixelOrder
)

ledInt.value = True
time.sleep(0.5)
ledInt.value = False
time.sleep(0.5)

numColors  = 4
colorRed   = [255, 0, 0, 128]
colorGreen = [0, 255, 0, 128]
colorBlue  = [0, 0, 255, 128]
colorIdx   = 0

pixels.fill((0, 0, 0))
pixels.show()

ledCnt = 0

while True:
    time.sleep(samplePeriod)
    ledCnt = ledCnt + 1
    if (ledCnt == 20):
        ledExt1.value = True
    elif (ledCnt == 21):
        ledExt1.value = False
        ledCnt = 0

    if (not butOff.value):
        # ledExt2.value = True
        # time.sleep(0.5)
        # ledExt2.value = False
        pixels.fill((colorRed[colorIdx], colorGreen[colorIdx], colorBlue[colorIdx]))
        colorIdx = (colorIdx + 1) % numColors
        # Uncomment this line if you have RGBW/GRBW NeoPixels
        # pixels.fill((255, 0, 0, 0))
        pixels.show()
