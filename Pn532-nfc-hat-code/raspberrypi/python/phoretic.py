"""Phoretic Rogues - OSC to Pn532 server
"""
import argparse
import math
import asyncio
import pn532.pn532 as nfc

from pythonosc.dispatcher import Dispatcher
from pythonosc import osc_server
from pythonosc import udp_client
from pn532 import *

parser = None
client = None
# current_client = None
registered = False
card = None
present = False


# def print_volume_handler(unused_addr, args, volume):
#     print("[{0}] ~ {1}".format(args[0], volume))
#
# def print_compute_handler(unused_addr, args, volume):
#     try:
#         print("[{0}] ~ {1}".format(args[0], args[1](volume)))
#     except ValueError: pass

def log(what):
    print("[rfid] {0}".format(what))


def register_client(unused_addr):
    global registered, card
    if not registered:
        args = parser.parse_args()
        card = PN532_UART(debug=False, reset=20, dev=args.tty)
        ic, ver, rev, support = card.get_firmware_version()
        log('Found PN532 with firmware version: {0}.{1}'.format(ver, rev))
        # Configure PN532 to communicate with NTAG215 cards
        card.SAM_configuration()
        registered = True


def save_data(unused_addr, data):
    global registered, card
    if not registered:
        log('save_data ignored, since not yet registered')
    else:
        try:
            # log("here")
            # ntag 215: 135 pages a 4 bytes = 540 bytes
            # user pages are 0x02 to 0x86 (134), i.e. 126 pages or 504 bytes
            data_len = min(504, len(data))     # max 126 pages a 4 bytes
            blocks = data_len // 4
            log("saving {0} bytes in {1} blocks".format(data_len, blocks))
            for i in range(blocks):
                off1 = i << 2
                off2 = off1 + 4
                block = data[off1:off2]
                block_num = i + 4
                card.ntag2xx_write_block(block_num, block)
                # card.ntag2xx_read_block(block_num)  # do we need this to avoid time-out?

            log("save done.")
            client.send_message("/done", "save")

        except nfc.PN532Error as e:
            print(e.errmsg)
            client.send_message("/fail", "save")

def load_data(unused_addr, max_len):
    global registered, card
    if not registered:
        log('load_data ignored, since not yet registered')
    else:
        try:
            data_len = min(504, max_len)     # max 126 pages a 4 bytes
            blocks = data_len // 4
            log("loading {0} bytes in {1} blocks".format(data_len, blocks))
            data = bytearray(data_len)
            for i in range(blocks):
                off1 = i << 2
                off2 = off1 + 4
                block_num = i + 4
                block = card.ntag2xx_read_block(block_num)
                data[off1:off2] = block

            data_b = bytes(data)
            log("load done.")
            client.send_message("/done", "load")
            client.send_message("/data", [data_b])

        except nfc.PN532Error as e:
            print(e.errmsg)
            client.send_message("/fail", "load")

async def wait_register():
    log('Waiting for /register')
    while not registered:
        # log('<dang>')
        await asyncio.sleep(1)


async def loop():
    """main loop"""
    global present
    await wait_register()
    log('Waiting for card')
    while True:
        # Check if a card is available to read
        try:
            uid = card.read_passive_target(timeout=0.5)
            # print('.', end="")
            # Try again if no card is available.
            new_present = uid is not None
            if present != new_present:
                present = new_present
                if present:
                    log('Found card with UID: {0}'.format([hex(i) for i in uid]))
                else:
                    log('Card removed')

                client.send_message("/present", [int(present)])

        except Exception as e:
            print(e)

        await asyncio.sleep(0.5)


async def init_main():
    global client, parser

    parser = argparse.ArgumentParser(prog='Phoretic')
    parser.add_argument("--server_ip",  # metavar="--server-ip",
                        default="127.0.0.1", help="The ip to receive commands from; default: 127.0.0.1")
    parser.add_argument("--server_port",  # metavar="--server-port",
                        type=int, default=5005, help="The port to receive commands from; default: 5005")
    parser.add_argument("--client_ip",  # metavar="--client-ip",
                        default="127.0.0.1", help="The ip to send replies to; default: 127.0.0.1")
    parser.add_argument("--client_port",  # metavar="--client-port",
                        type=int, default=5006, help="The port to send replies to; default: 5006")
    parser.add_argument("--tty",
                        default='/dev/ttyUSB0', help="The tty device to use for Pn532 UART; default: /dev/ttyUSB0")

    args = parser.parse_args()

    dispatcher = Dispatcher()
    dispatcher.map("/register", register_client)
    dispatcher.map("/save", save_data)
    dispatcher.map("/load", load_data)
    dispatcher.map("/test_print", print)
    # dispatcher.map("/volume", print_volume_handler, "Volume")
    # dispatcher.map("/logvolume", print_compute_handler, "Log volume", math.log)

    log("client [{0}, {1}]".format(args.client_ip, args.client_port))
    client = udp_client.SimpleUDPClient(args.client_ip, args.client_port)
    # print("client result [{0}]".format(client))

    log("server [{0}, {1}]".format(args.server_ip, args.server_port))
    server = osc_server.AsyncIOOSCUDPServer(
        (args.server_ip, args.server_port), dispatcher, asyncio.get_event_loop())
    transport, protocol = await server.create_serve_endpoint()  # Create datagram endpoint and start serving
    # print("Serving on {}".format(transport))
    await loop()  # Enter main loop of program
    transport.close()  # Clean up serve endpoint


if __name__ == "__main__":
    asyncio.run(init_main())
