#!/bin/bash
cd "$(dirname "$0")"
cd ..

echo "Phoretic Rogues (tower)"

sleep 2
source ./scripts/init-sound.sh
amixer -c1 set Speaker Playback 31 # -6 dB  37  # 0 dB
amixer -c1 set Mic Capture 18
sleep 8
source ./scripts/mount-ram-disk.sh
sleep 1

source ./scripts/run-phoresis.sh
sleep 1

# LDR with 'cache': all
# amp: right channel is metal ring (needs +6 dB)
source ./scripts/run.sh --node-id 4 --rfid --image-index 1635 --cap-noise 8.0 --cap-thresh 30 --sensor-diff-max-boost 0.3 0.3 0.3 0.3 0.3 0.3 --sensor-diff-min-boost 0.03 0.03 0.03 0.03 0.03 0.03 --sensor-thresh-shift 0.18 --sleep-time 22.0 --detect-space-period 953.0 --rot-scan-speed 1.0e-7 --shift-main-amp -10.0 -4.0
