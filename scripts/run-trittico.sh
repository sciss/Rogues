#!/bin/bash
cd "$(dirname "$0")"
cd ..

echo "Swap Rogues (trittico)"

sleep 2
source ./scripts/init-sound.sh
amixer -c1 set Speaker Playback 32
amixer -c1 set Mic Capture 18
sleep 8
source ./scripts/mount-ram-disk.sh
sleep 1
# LDR with 'cache': 1,4,5,6
source ./scripts/run.sh --node-id 3 --cap-noise 8.0 --cap-thresh 30 --sensor-diff-max-boost 0.75 0.4 0.22 1.1 0.95 1.1 --sensor-diff-min-boost 0.53 0.05 0.035 0.77 0.66 0.77 --sensor-thresh-shift 0.18 --detect-space-period 1019.0
