#!/bin/bash
cd "$(dirname "$0")"
cd ..

echo "Phoretic Rogues (stable)"

sleep 2
source ./scripts/init-sound.sh
amixer -c1 set Speaker Playback 31 # -6 dB  37  # 0 dB
amixer -c1 set Mic Capture 18
sleep 8
source ./scripts/mount-ram-disk.sh
sleep 1

source ./scripts/run-phoresis.sh
sleep 1

# LDR with 'cache': all
source ./scripts/run.sh --node-id 1 --rfid --cap-noise 8.0 --cap-thresh 30 --sensor-diff-max-boost 0.3 0.3 0.3 0.3 0.3 0.3 --sensor-diff-min-boost 0.03 0.03 0.03 0.03 0.03 0.03 --sensor-thresh-shift 0.18 --sleep-time 22.0 --detect-space-period 953.0 --shift-main-amp -10.0 -4.0
