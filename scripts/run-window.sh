#!/bin/bash
cd "$(dirname "$0")"
cd ..

echo "Swap Rogues (window)"

sleep 2
source ./scripts/init-sound.sh
amixer -c1 set Speaker Playback 32
amixer -c1 set Mic Capture 18
sleep 8
source ./scripts/mount-ram-disk.sh
sleep 1
# LDR with 'cache': 1,3,5,6
source ./scripts/run.sh --node-id 1 --cap-noise 5.0 --cap-thresh 50 --sensor-diff-max-boost 1.5 0.1 0.6 0.27 0.65 0.65 --sensor-diff-min-boost 1.05 0.17 0.42 0.22 0.45 0.45 --sensor-thresh-shift 0.18 --sleep-time 24.0 --detect-space-period 1069.0
