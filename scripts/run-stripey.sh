#!/bin/bash
cd "$(dirname "$0")"
cd ..

echo "Swap Rogues (stripey)"

sleep 2
source ./scripts/init-sound.sh
amixer -c1 set Speaker Playback 30  # slightly less, as we're in the foreground
amixer -c1 set Mic Capture 18
sleep 8
source ./scripts/mount-ram-disk.sh
sleep 1
# LDR with 'cache': 1,3,4,6
source ./scripts/run.sh --node-id 2 --cap-noise 8.0 --cap-thresh 30 --sensor-diff-max-boost 2.0 0.08 1.5 2.1 0.42 1.8 --sensor-diff-min-boost 1.4 0.04 1.05 1.47 0.08 1.26 --sensor-thresh-shift 0.18 --sleep-time 22.0 --detect-space-period 953.0
