///// local tests

val cfgT = osc.UDP.Config()
cfgT.localIsLoopback = true
val t = osc.UDP.Transmitter(cfgT)
t.connect()
val tgt = "127.0.0.1" -> 5005
t.send(osc.Message("/test_print", 1, 2.3f), tgt)

t.close()

/////
val cfgR = osc.UDP.Config()
cfgR.localIsLoopback = true
cfgR.localPort = 5006
val r = osc.UDP.Receiver(cfgR)
r.connect()
r.dump(osc.Dump.Text)
// r.isConnected

r.close()


///// remote tests

val cfgT = osc.UDP.Config()
cfgT.localIsLoopback = false
cfgT.localAddress = "192.168.77.78"
val t = osc.UDP.Transmitter(cfgT)
t.connect()
val tgt = "192.168.77.36" -> 5005   // 31 - stable, 36 - tower
// val tgt = "192.168.77.40" -> 5005
t.send(osc.Message("/test_print", 1, 2.3f), tgt)
val blob  = Array.tabulate(36)(i => i.toByte)
t.send(osc.Message("/test_print", java.nio.ByteBuffer.wrap(blob)), tgt)

t.close()

/////
val cfgR = osc.UDP.Config()
cfgR.localIsLoopback = false
cfgR.localAddress = "192.168.77.78"
cfgR.localPort = 5006
val r = osc.UDP.Receiver(cfgR)
r.connect()
r.dump(osc.Dump.Text)
// r.isConnected

r.close()

/////

t.send(osc.Message("/test_print", 1, 2.346f), tgt)
t.send(osc.Message("/register"), tgt)
t.send(osc.Message("/save", java.nio.ByteBuffer.wrap(blob)), tgt)

val blob2 = Array.tabulate(504)(i => i.toByte)
t.send(osc.Message("/save", java.nio.ByteBuffer.wrap(blob2)), tgt)

r.dump(osc.Dump.Hex)
t.send(osc.Message("/load", 16), tgt)
t.send(osc.Message("/load", 504), tgt)

