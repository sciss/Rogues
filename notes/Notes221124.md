- [-] possibility to rotate the light colour (arg switch), so we can adjust rather than twisting the sensors
- [X] node-id not detected without network cable? they all come up with the same image, why?
      they anyway have to have individual start scripts (`.desktop` files), because we want to give distinct
      parameters
