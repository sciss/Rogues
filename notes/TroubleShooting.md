# ADS disappearing ('window')

```
Swap Rogues - 15-Aug-2022, 20:20h
Traceback (most recent call last):
File "code.py", line 179, in <module>
File "adafruit_ads1x15/analog_in.py", line 55, in value
File "adafruit_ads1x15/ads1x15.py", line 134, in read
File "adafruit_ads1x15/ads1x15.py", line 176, in _read
File "adafruit_ads1x15/ads1x15.py", line 189, in _conversion_complete
File "adafruit_ads1x15/ads1x15.py", line 215, in _read_register
OSError: [Errno 19] No such device
```

seems a loose contact

# Hyperpixel not showing up ('stripey')

seems a loose contact at the header booster on the GPIO, or between the header booster and the GPIO cable

# LED colour glitches ('window')

seems that we actually need to lift the data signal to 5V (?)

see also: https://iris.artins.org/electronics/glitchy-neopixels/

(stripey is fine, though, and blue is not too bad)