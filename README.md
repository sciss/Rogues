[![Build Status](https://github.com/Sciss/Rogues/workflows/Scala%20CI/badge.svg?branch=main)](https://github.com/Sciss/Rogues/actions?query=workflow%3A%22Scala+CI%22)

# Rogues

This repository contains code for an ongoing art project.
See [Research Catalogue](https://www.researchcatalogue.net/view/1437680/1437681).

(C)opyright 2021–2025 by Hanns Holger Rutz. All rights reserved. This project is released under the
[GNU Affero General Public License](https://github.comt/Sciss/Rogues/blob/main/LICENSE) v3+ and
comes with absolutely no warranties.
To contact the author, send an e-mail to `contact at sciss.de`.

## building

Builds with sbt against Scala 3. See options: `sbt 'common/run --help'`. E.g.

    sbt 'common/runMain de.sciss.rogues.SteinerChain --margin 0 --radius 240 --full-screen --hide-envelope'

    sbt 'common/runMain de.sciss.rogues.Iris --num-blades 7 --full-screen'

    sbt common/assembly
    java -cp common/Rogues-common.jar de.sciss.rogues.ReceiveSensors --gui
    java -cp common/Rogues-common.jar de.sciss.rogues.TransmitColor --gui

    java -cp common/Rogues-common.jar de.sciss.rogues.SensorBody

    java -Dsun.java2d.opengl=true -cp common/Rogues-common.jar de.sciss.rogues.SwapRogue --margin 0 --full-screen --center-index 5

To improve Pi 4 performance: `java -Dsun.java2d.opengl=true ...`

## cap test

__OBSOLETE__

pigpio must be installed first: http://abyz.me.uk/rpi/pigpio/download.html

    git clone https://github.com/joan2937/pigpio.git
    cd pigpio
    make
    sudo make install

The in `Rogues`:

    sbt common/assembly
    sudo java -cp common/Rogues-common.jar de.sciss.rogues.CapSense

(yes, I need to get rid of the sudo; thanks pi4j)

## fix wiring-pi

__OBSOLETE__

__Important:__ Wiring-Pi is broken on the Pi 4. The pull up/down resistors cannot be configured.
See https://pi4j.com/1.3/install.html#WiringPi_Native_Library -- one needs to replace the installed versions
with an unofficial one!

    sudo apt remove wiringpi -y
    sudo apt install git-core gcc make
    cd ~/Documents/devel/
    git clone https://github.com/WiringPi/WiringPi --branch master --single-branch wiringpi
    cd wiringpi
    sudo ./build

## installing SuperCollider on the Pi

We build SC 3.10.4:

```
cd ~/Documents/devel
git clone https://github.com/supercollider/supercollider.git

sudo apt install libjack-jackd2-dev libsndfile1-dev libasound2-dev libavahi-client-dev \
libreadline-dev libfftw3-dev libxt-dev libudev-dev libncurses5-dev cmake git qttools5-dev qttools5-dev-tools \
qtdeclarative5-dev libqt5svg5-dev qjackctl

cd supercollider
git checkout -b 3.10.4 Version-3.10.4

git submodule update --init --recursive

mkdir build
cd build

cmake -DCMAKE_BUILD_TYPE=Release -DSUPERNOVA=OFF -DSC_ED=OFF -DSC_EL=OFF -DSC_VIM=ON -DNATIVE=ON -DSC_USE_QTWEBENGINE:BOOL=OFF ..

cmake --build . --config Release --target all -- -j3

sudo cmake --build . --config Release --target install
```

This installs in `/usr/local/bin`. If debian package has been installed, it will override through `/usr/bin`,
to remove use `sudo apt remove supercollider-server` (or `-common` I guess?).

We use `JPverb` thus also need `sc3-plugins`:

```
cd ~/Documents/devel
git clone https://github.com/supercollider/sc3-plugins.git

cd sc3-plugins
git checkout -b 3.10.4 643709850b2f22f68792372aaece5fc6512defc6

git submodule update --init --recursive

mkdir build
cd build

cmake -DSC_PATH=/home/pi/Documents/devel/supercollider/ -SC_PATH=/home/pi/.local/share/SuperCollider ..

cmake --build . --config Release

sudo cmake --build . --config Release --target install

```

## access TTL-to-USB via tinyUSB0 on Linux

To avoid root requirement, add user to dialout group:

    sudo usermod -a -G dialout $USER

Prepare reading, setting speed etc.

    stty -F /dev/ttyUSB0 speed 115200 cs8 -cstopb -parenb -echo

Dump output:

    cat /dev/ttyUSB0

Check if other process accesses the USB:

    lsof /dev/ttyUSB0

Running Python REPL in the terminal:

    screen /dev/ttyACM0 115200

(You may need to `sudo apt install screen`)

To stop `screen`, press `ctrl-a` followed by `k`. To scroll back the history, press `ctrl-a` followed by `Esc`.
Now up/down and page-up/down work. Exit the scroll-back mode by pressing `Esc` again.

## Install Pn532-OSC interface

    pip3 install python-osc

Pn532 library code, see next section.

## Pn532 tests

https://www.waveshare.com/wiki/PN532_NFC_HAT

Code:

https://www.waveshare.com/w/upload/6/67/Pn532-nfc-hat-code.7z

To unzip: 

    sudo apt install p7zip-full
    mkdir out
    cd out
    7z x ../Pn532-nfc-hat-code.7z

Set both jumpers L0 and L1 to L(ow), to enable UART. Connect the first four pins
to 3v3 / GND / Rx / Tx. Flip Tx/Rx with respect to the TTL-to-USB adapter.

When connected via UART to USB:

```
pn532 = PN532_UART(debug=False, reset=20, dev='/dev/ttyUSB0')
```

## Install HyperPixel

    git clone https://github.com/pimoroni/hyperpixel2r

    cd hyperpixel2r
    sudo ./install.sh

output:

    Notice: building hyperpixel2r.dtbo
    Created symlink /etc/systemd/system/multi-user.target.wants/hyperpixel2r-init.service → /etc/systemd/system/hyperpixel2r-init.service.
    Installed: /usr/bin/hyperpixel2r-init
    Installed: /etc/systemd/system/hyperpixel2r-init.service
    Installed: /boot/overlays/hyperpixel2r.dtbo
    Config: Added dtoverlay=hyperpixel2r to /boot/config.txt
    Config: Added enable_dpi_lcd=1 to /boot/config.txt
    Config: Added dpi_group=2 to /boot/config.txt
    Config: Added dpi_mode=87 to /boot/config.txt
    Config: Added dpi_output_format=0x7f216 to /boot/config.txt
    Config: Added dpi_timings=480 0 10 16 55 480 0 15 60 15 0 0 0 60 0 19200000 6 to /boot/config.txt
    
    After rebooting, use 'hyperpixel2r-rotate left/right/normal/inverted' to rotate your display!
    
      left - Landscape, power/HDMI on bottom
      right - Landscape, power/HDMI on top
      normal - Portrait, USB ports on top
      inverted - Portrait, USB ports on bottom

Needs Raspbian Buster not Bullseye!

## Pis

### Swap Rogues

- 192.168.77.35 "window"
- 192.168.77.43 "stripey"
- 192.168.77.45 "trittico"

### Phoretic Rogues

- 192.168.77.31 "stable"
- 192.168.77.36 "tower"

## Customise CircuitPython

- https://learn.adafruit.com/building-circuitpython/linux

Do not add Ubuntu PPA to apt on Debian!! All packages should be in Debian stable.

- https://learn.adafruit.com/building-circuitpython/build-circuitpython

We use 7.3.2 as base.

Before building, adjust the source code, e.g. `shared-module/touchio/Touchin.c`.

To build for the Pico:

    cd ports/raspberrypi
    make BOARD=raspberry_pi_pico

To reboot the Pico in bootloader mode, copy `restart-in-bootloader.py` to `code.py`.
Copying the uf2 over doesn't seem to work directly. Instead, erase all using the `flash_nuke.uf2`
provided in the `pico_stuff` directory, or here:

- https://learn.adafruit.com/getting-started-with-raspberry-pi-pico-circuitpython?view=all#circuitpython

Afterwards copy `firmware.uf2`. Since the Pico has been wiped, the contents of `pico_stuff/lib` as well as
`pico_stuff/boot.py` have to be copied again.

## RAM disk

```
sudo mkdir /mnt/ramdisk
sudo mount -t tmpfs -o size=64m tmpfs /mnt/ramdisk
```

check usage

```
df | grep ramdisk
```

to unmount

```
sudo umount /mnt/ramdisk
```

If you see discrepancies between `dh` and `du` when checking the RAM disk usage, there are probably leaking
file references (use `lsof` on the JDK process).
