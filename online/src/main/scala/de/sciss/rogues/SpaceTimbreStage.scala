/*
 *  SpaceTimbreStage.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.lucre.Txn.peer
import de.sciss.numbers.Implicits._
import de.sciss.proc.TimeRef
import de.sciss.rogues.{SwapRogue => Main}
import Main.{T, log}

import scala.concurrent.stm.Ref

class SpaceTimbreStage extends Stage.Running with ChannelKey { chanKey =>
  override def stage: Stage = Stage.SpaceTimbre

  private val timbreRef = Ref(Option.empty[SpaceTimbre.Result])
  private val tokenRef  = Ref(-1)

  override def start()(implicit tx: T, body: SoundBody): Unit = {
    import body.{config, random, universe}
    // println(s"FROM ${config.spaceAmp} TO ${config.spaceAmp - config.spaceAmpMaxDamp}")
    val spaceAmp0 = /*if (config.isEisenerz) config.spaceAmp - 3 else*/ config.spaceAmp
    val ampDb0    = random.nextFloat().linLin(0f, 1f, spaceAmp0, spaceAmp0 - config.spaceAmpMaxDamp)
    val ampDb     = /*if (config.isLoudCard) ampDb0 - 10 else*/ ampDb0
    log.debug(f"space-timbre amp is $ampDb%1.1f dB")
    val amp       = ampDb.dbAmp
//    val skipFreq  = body.allCommFreq /*.flatMap(tup => tup._1 ::  tup._2 :: Nil)*/.sorted
    val channel   = body.allocChannel(chanKey)
    val timbre    = SpaceTimbre.applyWith(body.spacePos, amp = amp, skipFreq = Nil /*skipFreq*/, channel = channel)
    timbre.runner.reactNow { implicit tx => state =>
      if (state.idle) {
        if (timbreRef.swap(None).contains(timbre)) {
          timbre.runner.dispose()
          body.releaseChannel(chanKey)
          body.released(stage)
        }
      }
    }
    timbreRef() = Some(timbre)

    val sch     = universe.scheduler
    val dlySec  = random.nextDouble().linLin(0.0, 1.0, config.spaceMinDur, config.spaceMaxDur)
    log.info(f"SpaceTimbre duration in seconds: $dlySec%1.1f")
    val dlyFr   = (dlySec * TimeRef.SampleRate).toLong
    val token   = sch.schedule(sch.time + dlyFr) { implicit tx =>
      release()
    }
    sch.cancel(tokenRef.swap(token))
  }

  override def release()(implicit tx: T, body: SoundBody): Unit = {
    timbreRef().foreach { timbre =>
      log.debug("Releasing space-timbre")
      timbre.release()
    }
    val sch = body.universe.scheduler
    sch.cancel(tokenRef.swap(-1))
  }
}
