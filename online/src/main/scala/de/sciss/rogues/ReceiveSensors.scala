/*
 *  ReceiveSensors.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import com.fazecast.jSerialComm.SerialPort
import de.sciss.audiofile.{AudioFile, AudioFileSpec, AudioFileType, SampleFormat}
import de.sciss.file.File
import de.sciss.lucre.impl.ObservableImpl
import de.sciss.lucre.synth.InMemory
import de.sciss.lucre.{Cursor, Disposable, Observable, Publisher}
import de.sciss.model.Model
import de.sciss.model.impl.ModelImpl
import de.sciss.osc
import de.sciss.proc.{SoundProcesses, Universe}
import de.sciss.rogues.SwapRogue as Main
import Main.T
import de.sciss.model.Model.Listener
import org.rogach.scallop.ScallopConf

import java.io.{BufferedReader, FileInputStream, InputStream, InputStreamReader}
import java.net.{InetAddress, InetSocketAddress}
import scala.swing.{Component, Dimension, Graphics2D, Label, MainFrame, Swing}

/*  Corresponds to `swap-rogues.py` on the Pico, sending data via USB serial.
 *
 *  Reads ASCII text formatted lines of sensor 16-bit sensor values separated by space characters,
 *  and an optional button bitfield
 */
object ReceiveSensors {
  case class ConfigImpl(
                         debugSensors     : Boolean         = false,
                         printSensors     : Boolean         = false,
                         sensorGui        : Boolean         = false,
                         serialDevice     : String          = SerialSupport.defaultDevice,
                         numLDRSensors    : Int             = 6,
                         numCapSensors    : Int             = 6,
                         buttonSensors    : Boolean         = true,
                         recordSensors    : Option[File]    = None,
                         sensorOsc        : Boolean         = false,
                         sensorOscIP      : String          = "127.0.0.1",
                         sensorOscPort    : Int             = 57130,
                         sensorThreshWake : Float           = 0.25f,
                         sensorThreshShift: Float           = 0.12f,
                         sensorAttackShift: Float           = 0.95f,
                         sensorDecayShift : Float           = 0.995f,
                         sensorAttackFlare: Float           = 0.1f,
                         sensorDecayFlare : Float           = 0.1f,
                         sensorDiffMinBoost: List[Float]    = 1.0f / 32 :: Nil,
                         sensorDiffMaxBoost: List[Float]    = 1.0f /  4 :: Nil,
                         sensorDiffShrinkThresh: Float = 0.5f,
                       ) extends Config

  trait Config extends SensorBody.Config {
    def debugSensors  : Boolean
    def printSensors  : Boolean
    def sensorGui     : Boolean
    def serialDevice  : String
    def recordSensors : Option[File]
    def sensorOsc     : Boolean
    def sensorOscIP   : String
    def sensorOscPort : Int
  }

  def parseArgs(args: Array[String], name: String): ConfigImpl = {
    object p extends ScallopConf(args) {
      import org.rogach.scallop.{ScallopOption => Opt, *}

      printedName = name // "ReceiveSensors"
      private val default: ConfigImpl = ConfigImpl()

      val debug: Opt[Boolean] = toggle(default = Some(default.debugSensors),
        descrYes = "Debug operation.",
      )
      val verbose: Opt[Boolean] = toggle(default = Some(default.printSensors),
        descrYes = "Verbose sensor data printing.",
      )
      val gui: Opt[Boolean] = toggle(default = Some(default.sensorGui),
        descrYes = "Show sensor data GUI.",
      )
      val button: Opt[Boolean] = toggle(default = Some(default.buttonSensors),
        descrYes = "Assume button bitfield is included (default)",
        descrNo  = "Assume button bitfield is not included",
      )
      val device: Opt[String] = opt(default = Some(default.serialDevice),
        descr = s"Serial device name (default: ${default.serialDevice}).",
      )
      val numLDRSensors: Opt[Int] = opt(name = "num-ldr-sensors", default = Some(default.numLDRSensors),
        descr = s"Number of LDR sensor values per frame (default: ${default.numLDRSensors}).",
      )
      val numCapSensors: Opt[Int] = opt(name = "num-cap-sensors", default = Some(default.numCapSensors),
        descr = s"Number of capacitive sensor values per frame (default: ${default.numCapSensors}).",
      )
      val record: Opt[File] = opt(default = default.recordSensors,
        descr = s"Record data to IRCAM sound file.",
      )
      val osc: Opt[Boolean] = toggle(default = Some(default.sensorOsc),
        descrYes = "Send sensor data to OSC.",
      )
      val oscIP: Opt[String] = opt(name = "osc-ip", default = Some(default.sensorOscIP),
        descr = s"Target OSC IP address (default: ${default.sensorOscIP}).",
      )
      val oscPort: Opt[Int] = opt(name = "osc-port", default = Some(default.sensorOscPort),
        descr = s"Target OSC port (default: ${default.sensorOscPort}).",
      )
      val threshWake: Opt[Float] = opt(default = Some(default.sensorThreshWake),
        descr = s"Threshold for waking up (default: ${default.sensorThreshWake}).",
      )
      val threshShift: Opt[Float] = opt(default = Some(default.sensorThreshShift),
        descr = s"Threshold for shifting frequency bands (default: ${default.sensorThreshShift}).",
      )
      val attackShift: Opt[Float] = opt(default = Some(default.sensorAttackShift),
        descr = s"Attack coefficient for shifting frequency bands (default: ${default.sensorAttackShift}).",
      )
      val decayShift: Opt[Float] = opt(default = Some(default.sensorDecayShift),
        descr = s"Decay coefficient for shifting frequency bands (default: ${default.sensorDecayShift}).",
      )
      val attackFlare: Opt[Float] = opt(default = Some(default.sensorAttackFlare),
        descr = s"Attack coefficient for light flare (default: ${default.sensorAttackFlare}).",
      )
      val decayFlare: Opt[Float] = opt(default = Some(default.sensorDecayFlare),
        descr = s"Decay coefficient for light flare (default: ${default.sensorDecayFlare}).",
      )
      val diffMinBoost: Opt[List[Float]] = opt(default = Some(default.sensorDiffMinBoost),
        descr = s"Differentiation minimum boost (default ${default.sensorDiffMinBoost})"
      )
      val diffMaxBoost: Opt[List[Float]] = opt(default = Some(default.sensorDiffMaxBoost),
        descr = s"Differentiation maximum boost (default ${default.sensorDiffMaxBoost})"
      )
      val diffShrinkThresh: Opt[Float] = opt(default = Some(default.sensorDiffShrinkThresh),
        descr = s"Differentiation shrink threshold (default: ${default.sensorDiffShrinkThresh}).",
      )

      verify()
      val config: ConfigImpl = ConfigImpl(
        debugSensors        = debug         (),
        printSensors        = verbose       (),
        sensorGui           = gui           (),
        serialDevice        = device        (),
        numLDRSensors       = numLDRSensors (),
        numCapSensors       = numCapSensors (),
        buttonSensors       = button        (),
        recordSensors       = record.toOption,
        sensorOsc           = osc           (),
        sensorOscIP         = oscIP         (),
        sensorOscPort       = oscPort       (),
        sensorThreshWake    = threshWake    (),
        sensorThreshShift   = threshShift   (),
        sensorAttackShift   = attackShift   (),
        sensorDecayShift    = decayShift    (),
        sensorAttackFlare   = attackFlare   (),
        sensorDecayFlare    = decayFlare    (),
        sensorDiffMinBoost  = diffMinBoost  (),
        sensorDiffMaxBoost  = diffMaxBoost  (),
        sensorDiffShrinkThresh = diffShrinkThresh(),
      )
    }
    p.config
  }

  def main(args: Array[String]): Unit = {
    implicit val c: Config = parseArgs(args, "ReceiveSensors")
    val port = SerialSupport.openPort(c.serialDevice)
    implicit val csr: Cursor[T] = Main.mkSystem()
    run(port)
//    res.addListener {
//      _ => ()
//    }

    if !c.sensorGui then {
      val t = new Thread {
        override def run(): Unit = synchronized(wait())
      }
      t.setDaemon(false)
      t.run()
    }
  }

//  // note: we don't care about the mutability,
//  // that means receivers may read mixed packets
//  // from two adjacent readings, though!
//  type Blob = Array[Int]

//  trait Result extends Model[Blob] with Observable[T, Blob]
  trait Result {
    def body: SensorBody
  }

  def dummy(implicit config: Config): Result = new Result {
    val body: SensorBody = SensorBody()
  }

  def run(port: SerialPort)(implicit cursor: Cursor[T], c: Config): Result = {
    val sensorVals = new Array[Int](c.numSensors + (if c.buttonSensors then 1 else 0))

    val recFileOpt = c.recordSensors.map { f =>
      require (!f.exists(), s"Recordings file $f already exists. Not overwriting.")

      val spec  = AudioFileSpec(
        fileType      = AudioFileType.IRCAM,  // doesn't need header update
        sampleFormat  = SampleFormat.Int16,   // that's the resolution of the ADC
        numChannels   = c.numSensors,
        sampleRate    = 20.0,                 // approximately; not used
      )
      val af = AudioFile.openWrite(f, spec)
      af
    }
    val recBufLen = 1024
    val recBuf    = Array.ofDim[Double](c.numSensors, recBufLen)

    val oscT = if !c.sensorOsc then None else {
      val oscCfg  = osc.UDP.Config()
      val loop    = c.sensorOscIP == "127.0.0.1"
      oscCfg.localIsLoopback  = loop
      oscCfg.localPort        = 57140
      if !loop then oscCfg.localAddress = InetAddress.getByName(SwapRogue.thisIP())
      println(s"sensor OSC socket ${oscCfg.localSocketAddress}")
      val t = osc.UDP.Transmitter(oscCfg)
      t.connect()
      Some(t)
    }

    val oscTgt =
      if !c.sensorOsc then InetSocketAddress.createUnresolved (c.sensorOscIP, c.sensorOscPort)
      else             new InetSocketAddress                  (c.sensorOscIP, c.sensorOscPort)

    if c.sensorOsc then println(s"sensor OSC target $oscTgt")

    val _body = SensorBody()
    
    object Res extends Result {
      val body = _body
    }

    if c.sensorGui then SensorBody.ui(_body)
    
//    object Res extends Result with ModelImpl[Array[Int]] with ObservableImpl[T, Blob] {
//      def update(b: Blob): Unit = {
//        dispatch(b)
//        SoundProcesses.step[T]("sensor-blob") { implicit tx =>
//          fire(b)
//        }
//      }
//    }

    var recBufOff = 0

    val st = new Thread {
      override def run(): Unit = {
        runWith(port = port, sensorVals = sensorVals) {
          _body.update(sensorVals)

          recFileOpt.foreach { af =>
            var si = 0
            while si < c.numSensors do {
              recBuf(si)(recBufOff) = sensorVals(si).toDouble / 0x7FFF
              si += 1
            }

            recBufOff += 1
            if recBufOff == recBufLen then {
              // println("WRITE")
              af.write(recBuf)
              recBufOff = 0
            }
          }

          oscT.foreach { t =>
            t.send(osc.Message("/ldr", sensorVals: _*), oscTgt)
          }

          if c.printSensors then {
            val s = sensorVals.mkString("sensors: ", ", ", "")
            println(s)
          }
        }
      }
    }
    st.setDaemon(true)
    st.start()

    Res
  }

  def runWith(port: SerialPort, sensorVals: Array[Int])(fun: => Unit)(implicit c: Config): Unit =
    val in = {
      port.getInputStreamWithSuppressedTimeoutExceptions /*getInputStream*/
    }
    try
      runStream(in, sensorVals = sensorVals)(fun)
    finally
      port.closePort()

  def runStream(in: InputStream, sensorVals: Array[Int])(fun: => Unit)(implicit c: Config): Unit = {
    val numLDR      = c.numLDRSensors
    val numCap      = c.numCapSensors
    val numBoth     = numLDR + numCap
    val button      = c.buttonSensors
    val numSensors  = if button then numBoth + 1 else numBoth
    assert (numSensors == c.numSensors)
    val bufSz       = numBoth * 6 + (if button then 2 else 0)
    val buf         = new Array[Byte](bufSz)
    while true do {
      var lineDone  = false
      var overflow  = false
      var bufOff    = 0

      while !lineDone do {
        val c = in.read()
        if c < 0 then {
          // asynchronous; not available
          Thread.sleep(1)
//          Thread.`yield`()
          // println("Stream closed")
          // return
        } else if c == 10 then {
          lineDone = true
        } else if !overflow then {
          buf(bufOff) = c.toByte
          bufOff += 1
          if bufOff == bufSz then {
            overflow  = true
            lineDone  = true
          }
        }
      }

      val sLine = new String(buf, 0, bufOff, "UTF-8")
      if c.debugSensors then println(s"lineDone at $bufOff. overflow? $overflow. line is '$sLine'")

      if !overflow then {
        val sArr = sLine.split(' ')
//        println(s"GOT ${sArr.length} VALUES")
        if sArr.length >= numSensors then {
          var t = 0
          while t < numBoth do
            val s = sArr(t)
            var i = 0
            var sensorVal = 0
            var ok = true
            while ok && i < s.length do {
              val c = s.charAt(i).toInt - 48
              if c >= 0 && c <= 9 then {
                sensorVal = sensorVal * 10 + c
                i += 1
              } else {
                ok = false
              }
            }

            if ok then sensorVals(t) = sensorVal
            t += 1

          if button then {
            val s = sArr(numBoth)
            if s.length == 1 then {
              val butVal = s.charAt(0).toInt - 48
              sensorVals(numBoth) = butVal
            }
          }

          fun
        }
      }
    }
  }
}