/*
 *  FillDatabase.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues.writing

import de.sciss.proc.Control
import de.sciss.rogues.SwapRogue.{SR, T}

object FillDatabase {
  def apply(verbose: Boolean)(implicit tx: T): Control[T] = {
    val g = Control.Graph {
      import de.sciss.lucre.expr.graph._
      import de.sciss.proc.ExImport._

      val self            = ThisRunner()
      val dbCueIn         = "database".attr[AudioCue](AudioCue.Empty())
      val recCueIn        = "tmp-file".attr[AudioCue](AudioCue.Empty())
      val rQueryRadioRec  = Runner("query-rec")
      val rAppendDb       = Runner("append-db")
      val busyRec         = self.attr[Boolean]("busy-rec")

      val SRd             = SR.toDouble // "sample-rate".attr(44100.0)
      val dbTargetLen     = (SRd * 180).toInt
      val maxCaptureLen   = (SRd *  12).toInt // 20
      val minCaptureLen   = (SRd *   4).toInt

      val recDone         = rQueryRadioRec.stoppedOrDone
      val recFail         = rQueryRadioRec.failed
      val appendDone      = rAppendDb.stoppedOrDone
      val appendFail      = rAppendDb.failed

      val dbFileIn        = dbCueIn.artifact
      val dbCount         = "db-count".attr(0)
      val loadBang        = LoadBang()
      val dbCount1        = (dbCount.latch(loadBang) + 1) % 2
      val dbFileOut       = dbFileIn.replaceName("db" ++ dbCount1.toStr ++ ".aif")
      val recFileIn       = recCueIn.artifact
      val recFileOut      = recFileIn // .replaceName(recName)

      val dbLen = dbCueIn.numFrames.toInt
      val captLen: Ex[Int]    = maxCaptureLen min (dbTargetLen - dbLen)
      val captSec: Ex[Double] = captLen / SRd // Mellite issue #35

      val actAppend = Act(
        if verbose then PrintLn("writing append rec") else Act.Nop(),
        rAppendDb.runWith(
          "capt-frames" -> captLen,
          "in-db"       -> dbFileIn,
          "out-db"      -> dbFileOut,
          "in-app"      -> recFileOut,
        ),
      )

      val actRec: Act = {
        If (captLen < minCaptureLen) Then {
          Act(
            if verbose then PrintLn("writing (db long enough)") else Act.Nop(), 
            Delay(0.1)(self.done) // actAppend
          )
        } Else {
          Act(
            if verbose then PrintLn("writing run rec") else Act.Nop(),
            busyRec.set(true),
            rQueryRadioRec.runWith(
              "dur" -> captSec,
              "out" -> recFileOut,
            ),
          )
        }
      }

      val specDbOut = AudioFileSpec.read(dbFileOut)
        .getOrElse(AudioFileSpec.Empty())
      val dbCueOut = AudioCue(dbFileOut, specDbOut)

      val actDone = Act(
        if verbose then PrintLn("writing append done.") else Act.Nop(),
        dbCount.set(dbCount1),
        dbCueIn.set(dbCueOut),
        PrintLn(dbCueIn.toStr),
        self.done
      )

      loadBang    --> actRec
      recDone     --> Act(
        busyRec.set(false),
        actAppend
      )
      recFail     --> Act(
        busyRec.set(false),
        self.fail("query-rec failed")
      )
      appendDone  --> actDone
//      appendDone  --> PrintLn("APPEND DONE ; " ++ rAppendDb.state.toStr)
      appendFail  --> self.fail("database-append failed")

//      val TEST = "append-db".attr[Obj]
//      loadBang --> PrintLn("append-db obj: " ++ TEST.toStr)

      // rQueryRadioRec.state.changed ---> PrintLn(rQueryRadioRec.state.toStr)
    }

    val c = Control[T]()
    c.graph() = g
    c
  }
}
