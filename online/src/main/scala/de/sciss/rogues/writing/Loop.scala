/*
 *  Loop.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues.writing

import de.sciss.proc.Control
import de.sciss.rogues.SwapRogue.T

object Loop {
  def apply(verbose: Boolean)(implicit tx: T): Control[T] = {
    val g = Control.Graph {
      import de.sciss.lucre.expr.graph._
      import de.sciss.proc.ExImport._

      val self          = ThisRunner()
      val rRender       = Runner("render")
      val rPlay         = Runner("play-phrase")
      val chan          = self.attr[Int]("channel").get
      val loadBang      = LoadBang()
      val busyPlay      = self.attr[Boolean]("busy-play")
//      val busyRec       = self.attr[Boolean]("busy-rec")      // doesn't work for obj's attr map?
//      val busyRender    = self.attr[Boolean]("busy-render")
      val busyRec       = "busy-rec".attr(false)
      val busyRender    = "busy-render".attr(false)
      val vrBusyRec     = Var(false); vrBusyRec     --> busyRec    // cf. Lucre issue #70
      val vrBusyRender  = Var(false); vrBusyRender  --> busyRender // cf. Lucre issue #70
//      val busyRender  = "busy-render".attr(false)
//      val next        = self.attr[Boolean]("next")
      val awake       = "awake".attr(false)
      val idle        = Var(true)

//      busyRender.changed --> PrintLn("BUSY RENDER: " ++ busyRender.toStr)

      val actPlay = Act(
        if verbose then PrintLn("-- writing: play") else Act.Nop(),
//        next.set(false),
        busyPlay.set(true),
        rPlay.runWith(
          "channel"   -> chan,
        ),
      )

      val actRender = Act(
        if verbose then PrintLn("-- writing: render") else Act.Nop(),
//        busyRender.set(true),
        rRender.runWith(
          "phrase-mod"  -> 2,
          "busy-rec"    -> vrBusyRec,
          "busy-render" -> vrBusyRender,
        ),
      )

      val actRenderDone = Act(
        if verbose then PrintLn("-- writing: render done") else Act.Nop(),
        If (awake) Then actPlay Else idle.set(true),
      )

      val condRun     = idle /*&& next.get*/ && awake
      val trRun       = condRun.toTrig
      val rnd         = Random()
      val dlyTime     = rnd.range(2.0, 12.0)
      val actIterate  = Act(
        idle.set(false),
        dlyTime.update,
        Delay(dlyTime)(actRender),
      )
      val actMaybeIterate  = If (condRun) Then actIterate

      val actPlayDone = Act(
        if verbose then PrintLn("-- writing: play done") else Act.Nop(),
        busyPlay.set(false),
        idle    .set(true ),
        actMaybeIterate,
      )

      rPlay   .done --> actPlayDone
      rRender .done --> actRenderDone

      loadBang  --> actMaybeIterate
      trRun     --> actIterate

      // debugging
//      (loadBang | next .changed) --> PrintLn("Writing - NEXT  IS " ++ next .toStr)
//      (loadBang | idle .changed) --> PrintLn("Writing - IDLE  IS " ++ idle .toStr)
//      (loadBang | awake.changed) --> PrintLn("Writing - AWAKE IS " ++ awake.toStr)

    }
    val c = Control[T]()
    c.graph() = g
    c
  }
}
