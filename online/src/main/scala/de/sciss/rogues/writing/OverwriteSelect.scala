/*
 *  OverwritePerform.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues.writing

import de.sciss.fscape.{GE, Graph}
import de.sciss.patterns.lucre.Stream as LStream
import de.sciss.patterns.Stream as PStream
import de.sciss.patterns.graph.Pat
import de.sciss.proc.{Control, FScape}
import de.sciss.rogues.SwapRogue.{SR, T}

import scala.Stream as _

object OverwriteSelect {
  def apply()(implicit tx: T): Control[T] = {
    import de.sciss.patterns.graph.*
    import de.sciss.patterns.PatImport.*
    val sRng = mkStream {
      White(0.0, 1.0)
    }
    val sSpaceDur = mkStream {
      Brown(1.2, 2.4, 0.1)
    }
    val sStretchStable = mkStream {
      Brown(0.0, 1.0, 0.1).linExp(0.0, 1.0, 1.0 / 1.1, 1.1)
    }
    val sStretchGrow = mkStream {
      Brown(1.2, 2.0, 0.2)
    }
    val sStretchShrink = mkStream {
      Brown(0.6, 0.95, 0.2)
    }
    val c = mkControl()
    val fSelect = mkFScape()
    val a = c.attr
    a.put("rng", sRng)
    a.put("space-dur", sSpaceDur)
    a.put("stretch", sStretchStable)
    a.put("stretch-stable", sStretchStable)
    a.put("stretch-grow"  , sStretchGrow)
    a.put("stretch-shrink", sStretchShrink)
    a.put("select", fSelect)
    c
  }

  private def mkStream(body: => Pat[_])(implicit tx: T): LStream[T] = {
    val res = LStream[T]()
    val pat: Pat[_] = body
    val peer = {
      import res.context
      pat.expand[T]
    }
    res.peer() = peer
    res
  }

  private def mkFScape()(implicit tx: T): FScape[T] = {
    val g = Graph {
      import de.sciss.fscape.graph.{AudioFileIn => _, AudioFileOut => _, *}
      import de.sciss.fscape.lucre.graph.*
      import de.sciss.fscape.lucre.graph.Ops.*

      val spaceDur = "space-dur".attr[Double]

      def mkIn() = AudioFileIn("in")

      val fileIn = mkIn()
      //      val specIn    = AudioFile.readSpec(fileIn)
      //      import specIn.{numChannels, numFrames, sampleRate}
      import fileIn.{numFrames, sampleRate}
      // require(numChannels == 2) // left channel is sound signal, right channel is 'withering'

      val in = fileIn /*mkIn()*/ out 0
      val inWither = mkIn() out 1 // separate UGen so we don't run into trouble wrt to buffering

      // XXX TODO --- enabling this prevents some hanging. but why?
      // if (Main.showLog) {
      // in.poll("writing ovr-fsc")
      // }

      val fftSize   = 1024 // 2048
      val stepDiv   = 4
      val numMel    = 42
      val numCoef   = 21
      val sideDur   = 0.25
      val minFreq   = 100.0
      val maxFreq   = 14000.0
      val witherTgt = 0.0012 / 30 // a threshold of 0.0012 in 30 iterations
      val WitheringConstant = 0.0078125 // = 1.0/128

      val stepSize    = fftSize / stepDiv
      val sideFrames  = (sampleRate * sideDur ).toInt
      val spaceFrames = (sampleRate * spaceDur).toInt
      val spaceLen    = spaceFrames / stepSize
      val sideLen     = (sideFrames / stepSize).max(1) // 24
      val covSize     = numCoef * sideLen
      val numSteps    = (numFrames / stepSize).toInt
      val numCov      = (numSteps - (2 * sideLen)).max(0)
      val numCov1     = (numSteps - sideLen - spaceLen).max(0)

      //numSteps.poll(0, "numSteps")
      //numCov1.poll(0, "numCov1")

      val lap = Sliding(in, fftSize, stepSize) * GenWindow(fftSize, GenWindow.Hann)
      val fft = Real1FFT(lap, fftSize, mode = 2)
      val mag = fft.complex.mag
      val mel = MelFilter(mag, fftSize / 2, bands = numMel,
        minFreq = minFreq, maxFreq = maxFreq, sampleRate = sampleRate)
      val mfcc = DCT_II(mel.log, numMel, numCoef, zero = 1 /* 0 */)

      // reconstruction of what strugatzki's 'segmentation' is doing (first step)
      val mfccSlid  = Sliding(mfcc, size = covSize, step = numCoef)
      val mfccSlidT = mfccSlid.drop(covSize)
      val el        = BufferMemory(mfccSlid, size = covSize)
      val cov       = Pearson(el, mfccSlidT, covSize)

      val covNeg    = -cov + (1.0: GE.D) // N.B. not `1 - cov` because binary-op-ugen stops when first input stops
      val wither0   = ResizeWindow(inWither, size = stepSize, start = 0, stop = -(stepSize - 1))
      val wither    = wither0 * (witherTgt / WitheringConstant)

      val key = (covNeg + wither).take(numCov1)
      //    Length(BufferDisk(covNeg)).poll(0, "covNeg.length")
      //    Length(BufferDisk(wither)).poll(0, "wither.length")
      //Length(BufferDisk(key   )).poll(0, "key   .length")

      val covMin0 = DetectLocalMax(key, size = spaceLen)
      val covMin  = covMin0.take(numCov) // XXX TODO --- bug in DetectLocalMax?

      val keysEl  = key.elastic()
      val values  = (Frames(keysEl) - 1).toInt
      val keysG   = FilterSeq(keysEl, covMin)
      val valuesG = FilterSeq(values, covMin)

      //    RunningMax(keysG).last.poll(0, "MAX SCHNUCK")

      val top       = PriorityQueue(keysG, valuesG, size = 1) // lowest covariances mapped to frames
      val startF    = (top * stepSize) :+ 0
      val valuesGE  = BufferMemory(valuesG, numCov1)
      val stopF     = (valuesG.dropWhile(valuesGE <= top) ++ numCov /* numSteps */) /*.take(1) */ * stepSize

      // def ValueOut(name: String, value: GE): Unit = ()
      //
      // ValueOut("start", startF)
      // ValueOut("stop" , stopF )

      MkLong("start", startF)
      MkLong("stop" , stopF )
    }
    val f = FScape[T]()
    f.graph() = g
    f
  }

  private def mkControl()(implicit tx: T): Control[T] = {
    val g = Control.Graph {
      import de.sciss.lucre.expr.graph.*
      import de.sciss.proc.ExImport.*

      val self = ThisRunner()

      val resSpan   = "span"      .attr[Span]
      val resNewLen = "new-length".attr[Long]

      val ph0   = "phrase".attr[AudioCue](AudioCue.Empty())
      val len0  = ph0.numFrames
      val SRd   = SR.toDouble // "sample-rate".attr(44100.0) // 48000.0

      val minPhaseDur     = "min-phrase-dur".attr(6.0)
      val minPhInsDur     =   1.5 // 3.0
      val maxPhaseDur     = "max-phrase-dur".attr(20.0)
      val minStabDur      =  10.0
      val stableDurProb   =   3.0 / 100
      val ovrBoundaryProb =   6.0 / 100

      val minPhaseLen   = (SRd * minPhaseDur).toLong
      val minPhInsLen   = (SRd * minPhInsDur).toLong
      val maxPhaseLen   = (SRd * maxPhaseDur).toLong

      val noStream        = Stream()
      val mStretch        = "stretch"       .attr(noStream)
      val mStretchGrow    = "stretch-grow"  .attr(noStream)
      val mStretchShrink  = "stretch-shrink".attr(noStream)
      val mStretchStable  = "stretch-stable".attr(noStream)
      val mSpaceDur       = "space-dur"     .attr(noStream)
      val rng             = "rng"           .attr(noStream)

      val pDur            = len0 / SRd // framesToSeconds(len0)
      val rngStableDur    = rng.next(0.5)
      val actSetStretch = If (pDur <= minPhaseDur) Then Act(
        PrintLn("-> grow"),
        mStretch.set(mStretchGrow)
      ) ElseIf (pDur >= maxPhaseDur) Then Act(
        PrintLn("-> shrink"),
        mStretch.set(mStretchShrink)
      ) ElseIf (pDur > minStabDur) Then Act(
        rngStableDur, // "draw"
        If (rngStableDur < stableDurProb) Then Act(
          PrintLn("-> stable"),
          mStretch.set(mStretchStable)
        )
      )

      val fStretchN   = mStretch.next(1.0)
      val rngUseBound = rng.next(0.5)
      val rngBoundEnd = rng.next(0.5)
      val rngJitAmt   = rng.next(0.5)

      val useBound    = rngUseBound <= ovrBoundaryProb
      val boundEnd    = useBound && (rngBoundEnd > 0.667)
      val jitAmt      = rngJitAmt

      val spaceDurN   = mSpaceDur.next(2.0)
      val spanStart0  = Var[Long]()
      val spanStop0   = Var[Long]()

      val rSelect = Runner("select")

      val selectDone      = rSelect.stoppedOrDone
      val selectFail      = rSelect.failed

      val phCueIn         = "phrase".attr[AudioCue](AudioCue.Empty())

      val phFileIn        = phCueIn.artifact
      // val phCount         = "ph-count".attr(0)
      val loadBang        = LoadBang()
      // val phCount1        = phCount.latch(loadBang) + (1: Ex[Int])
      // val phFileOut       = phFileIn.replaceName("ph" ++ phCount1.toStr ++ ".aif")

      val actSelect: Act = {
        val recRun: Act = rSelect.runWith(
          "space-dur" -> spaceDurN,
          "in"        -> phFileIn,
          "start"     -> spanStart0,
          "stop"      -> spanStop0,
        )

        Act(
          actSetStretch,
          spaceDurN,    // draw
          fStretchN,    // used in `actDone`
//          PrintLn("run select; space-dur = " ++ spaceDurN.toStr),
          recRun,
        )
      }

      val span0 = Span(spanStart0, spanStop0)
      val span1 = If (!useBound) Then span0 Else {
        If (boundEnd) Then Span(len0 - span0.length, len0) Else Span(0L, span0.length)
      }

      def jitter(in: Ex[Span], r: Ex[Double], secs: Ex[Double], minStart: Ex[Long], maxStop: Ex[Long]): Ex[Span] = {
        val secs2   = secs * 2
        val dFrames = (r * secs2 - secs * SRd).toLong
        val d       = (minStart - in.start) max ((maxStop - in.stop) min dFrames)
        Span(in.start + d, in.stop + d)
      }

      val span        = jitter(span1, r = jitAmt, secs = 0.2, minStart = 0L, maxStop = len0)
      val newLength0  = minPhInsLen max (span.length * fStretchN + (0.5: Ex[Double])).toLong
      val newDiff0    = newLength0 - span.length
      val len1        = minPhaseLen max (maxPhaseLen min (len0 + newDiff0))
      val newDiff1    = len1 - len0
      val newLength   = newDiff1 + span.length

      val actDone: Act =
        Act(
          rngUseBound,
          rngBoundEnd,
          rngJitAmt,
//          PrintLn("selected - start0: " ++ spanStart0.toStr ++
//            " ; stop0: " ++ spanStop0.toStr ++
//            " ; span: " ++ span.toStr ++
//            " ; newLength: " ++ newLength.toStr
//          ),
          resSpan   .set(span),
          resNewLen .set(newLength),
          self.done,
        )

      loadBang    --> actSelect
      selectDone  --> actDone
      selectFail  --> self.fail("overwrite-select failed")
    }
    val c = Control[T]()
    c.graph() = g
    c
  }
}
