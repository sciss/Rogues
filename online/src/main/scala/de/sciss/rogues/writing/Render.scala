/*
 *  Render.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues.writing

import de.sciss.proc.Control
import de.sciss.rogues.SwapRogue.T

object Render {
  def apply(verbose: Boolean, debug: Boolean)(implicit tx: T): Control[T]= {
    val g = Control.Graph {
      import de.sciss.proc.ExImport.*
      import de.sciss.lucre.expr.graph.*

      val phraseMod = "phrase-mod".attr(Int.MaxValue)

      val self          = ThisRunner()
      val rDbFill       = Runner("database-fill")
      val rOvrSelect    = Runner("overwrite-select")
      val rMatchSelect  = Runner("match-select")
      val rOvrPerform   = Runner("overwrite-perform")

      val resOvrSpan    = Var[Span]()
      val resOvrNewLen  = Var[Long]()
      val resInsPos     = Var[Long]()

      val dbCueIn       = "database".attr[AudioCue](AudioCue.Empty())
      val phCueIn       = "phrase"  .attr[AudioCue](AudioCue.Empty())
      val dbFileIn      = dbCueIn.artifact
      val phFileIn      = phCueIn.artifact
      val dbCount       = "db-count".attr(0)
      val phCount       = "ph-count".attr(0)

      val busyRec       = self.attr[Boolean]("busy-rec")
      val busyRender    = self.attr[Boolean]("busy-render")
      val vrBusyRec     = Var(false); vrBusyRec     --> busyRec    // cf. Lucre issue #70

      val doneMatch     = rMatchSelect.done

      val dbCount1      = (dbCount.latch(doneMatch) + 1) % 2
      val phCount1      = (phCount.latch(doneMatch) + 1) % phraseMod // 2
      val dbFileOut     = dbFileIn.replaceName("db" ++ dbCount1.toStr ++ ".aif")
      val phFileOut     = phFileIn.replaceName("ph" ++ phCount1.toStr ++ ".aif")

      val specDbOut     = AudioFileSpec.read(dbFileOut).getOrElse(AudioFileSpec.Empty())
      val dbCueOut      = AudioCue(dbFileOut, specDbOut)
      val specPhOut     = AudioFileSpec.read(phFileOut).getOrElse(AudioFileSpec.Empty())
      val phCueOut      = AudioCue(phFileOut, specPhOut)

      Seq(
        rDbFill       -> "database-fill",
        rOvrSelect    -> "overwrite-select",
        rMatchSelect  -> "match-select",
        rOvrPerform   -> "perform-overwrite",
      ).foreach { case (runner, key) =>
        runner.failed --> PrintLn(runner.messages.mkString(s"FAILURE: $key :\n", "\n", ""))
      }

      val timeOut = Delay(60.0) // somehow the rendering can hang
      val init = LoadBang()
      init --> Act(
        timeOut,
        rDbFill.runWith(
          "busy-rec" -> vrBusyRec
        )
      )

      rDbFill.done --> Act(
        busyRender.set(true),
        rOvrSelect.runWith(
          "span"        -> resOvrSpan,
          "new-length"  -> resOvrNewLen,
        )
      )
      rOvrSelect.done --> Act(
        if debug then PrintLn(
          "writing instr: " ++ resOvrSpan.toStr ++
            ", newLength = " ++ resOvrNewLen.toStr
        ) else Act.Nop(),
        rMatchSelect.runWith(
          "database"    -> dbCueIn,
          "phrase"      -> phCueIn,
          "start"       -> resOvrSpan.start,
          "stop"        -> resOvrSpan.stop,
          "new-length"  -> resOvrNewLen,
          "result"      -> resInsPos,
        )
      )

      doneMatch --> Act(
        if debug then PrintLn("writing match start: " ++ resInsPos.toStr) else Act.Nop(),
        rOvrPerform.runWith(
          "in-db"       -> dbFileIn,
          "out-db"      -> dbFileOut,
          "in-ph"       -> phFileIn,
          "out-ph"      -> phFileOut,
          "start"       -> resOvrSpan.start,
          "stop"        -> resOvrSpan.stop,
          "new-length"  -> resOvrNewLen,
          "match-pos"   -> resInsPos,
        )
      )

      val actDone: Act = Act(
        busyRender.set(false),
        self.done,
      )

      timeOut --> Act(
        if verbose then PrintLn("WARN: writing render time-out") else Act.Nop(),
        actDone,
      )

      rOvrPerform.done --> Act(
        if verbose then PrintLn("writing iteration done") else Act.Nop(),
        dbCount.set(dbCount1),
        dbCueIn.set(dbCueOut),
        if debug then PrintLn(dbCueIn.toStr) else Act.Nop(),
        phCount.set(phCount1),
        phCueIn.set(phCueOut),
        if debug then PrintLn(phCueIn.toStr) else Act.Nop(),
        actDone,
      )
    }
    val c = Control[T]()
    c.graph() = g
    c
  }
}
