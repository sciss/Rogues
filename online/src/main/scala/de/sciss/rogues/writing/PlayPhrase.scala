/*
 *  PlayPhrase.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues.writing

import de.sciss.lucre.{BooleanObj, DoubleObj, IntObj}
import de.sciss.patterns.graph.Pat
import de.sciss.proc.{Control, Proc}
import de.sciss.rogues.SwapRogue.T
import de.sciss.synth.SynthGraph
import de.sciss.patterns.lucre.Stream as LStream

object PlayPhrase {
  /** @param amp      linear */
  private def mkProc(/*limLevel: Double*/)(implicit tx: T): Proc[T] = {
    val g = SynthGraph {
      import de.sciss.synth.ugen.{VDiskIn => _, *}
      import de.sciss.synth.proc.graph.*
      import de.sciss.synth.Curve
      import de.sciss.synth.Import.*
      import de.sciss.synth.proc.graph.Ops.*

      val bus     = "bus"     .ir(1)
      bus.poll(0, "writing-chan")
      val dur     = "dur"     .ir
      val fdIn    = "fade-in" .ir
      val fdOut   = "fade-out".ir
      val disk    = VDiskIn.ar("in", loop = 0) // numChannels = 2, buf = buf, speed = BufRateScale.ir(buf), loop = 0)
      val chan    = disk out 0 // if (numChannels == 1) disk else Select.ar(bus, disk)
      val hpf     = HPF.ar(chan, 80.0)
      val env     = Env.linen(attack = fdIn, sustain = dur - (fdIn + fdOut), release = fdOut, curve = Curve.sine)
      val amp     = "amp".kr(1.0)
      val eg      = EnvGen.ar(env, levelScale = amp /* , doneAction = freeSelf */)
      val done    = Done.kr(eg) // XXX TODO: could delay by limiter look-ahead of 10ms
      val sig0    = hpf * eg
//      val PP = Peak.ar(sig0, 0)
//      PP.ampDb.poll(2, "WR PK")
      val limLevel = -15.dbAmp  // XXX TODO why it gets too loud?
      // println(s"limLevel $limLevel")
      val sig     = Limiter.ar(sig0, limLevel)
      Out.ar(bus, sig)
      DoneSelf(done)
    }
    val p = Proc[T]()
    p.graph() = g
    p
  }

  /** @param limLevel  linear */
  def apply(busyRef: BooleanObj[T], chanRef: IntObj[T], amp: Double /*, limLevel: Double*/)(implicit tx: T): Control[T] = {
    import de.sciss.patterns.graph.*
    import de.sciss.patterns.PatImport.*
    val sFadeOut = mkStream {
      White(0.1, 0.5)
    }
    val pPlay = mkProc(/*limLevel = limLevel*/)
    val aProc = pPlay.attr
    aProc.put("amp", DoubleObj.newConst[T](amp))
    val c     = mkControl()
    val aCtl   = c.attr
    aCtl.put("fade-out"  , sFadeOut)
    aCtl.put("play"      , pPlay)
    aCtl.put("busy-play" , busyRef)
    aCtl.put("bus"       , chanRef)
    c
  }

  private def mkStream(body: => Pat[_])(implicit tx: T): LStream[T] = {
    val res = LStream[T]()
    val pat: Pat[_] = body
    val peer = {
      import res.context
      pat.expand[T]
    }
    res.peer() = peer
    res
  }

  private def mkControl()(implicit tx: T): Control[T] = {
    val g = Control.Graph {
      import de.sciss.lucre.expr.graph.*
      import de.sciss.proc.ExImport.*

      val self      = ThisRunner()
      val rPlay     = Runner("play")
      val phCue     = "phrase"  .attr[AudioCue](AudioCue.Empty())
      val fadeIn    = 0.1
      val stFadeOut = "fade-out".attr(Stream()).next(0.25)
      val busyPlay  = "busy-play".attr(false)
      val bus       = "bus".attr(1)
      val phDur     = phCue.numFrames / phCue.sampleRate
      val actPlay = Act(
        stFadeOut,  // pick next one
        //  aIn   .set(phCue),
        //  aFdIn .set(fadeIn),    // XXX TODO --- runWith not yet supported
        //  aFdOut.set(stFadeOut),
        //  aDur  .set(phDur),
        busyPlay.set(true),
        rPlay.runWith(
          //    "bus" -> chan,
          "in"        -> phCue,
          "dur"       -> phDur,
          "fade-in"   -> fadeIn,
          "fade-out"  -> stFadeOut,
          "bus"       -> bus,
        ),
      )

      val init = LoadBang()
      init --> actPlay

      rPlay.stoppedOrDone --> Act(
//        PrintLn("phrase play done."),
        busyPlay.set(false),
        self.done,
      )

      rPlay.failed --> Act(
        PrintLn("phrase play failed."),
        busyPlay.set(false),
        self.fail(rPlay.messages.mkString("\n"))
      )
    }
    val c = Control[T]()
    c.graph() = g
    c
  }
}
