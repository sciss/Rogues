/*
 *  MatchSelect.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues.writing

import de.sciss.fscape.{GE, Graph}
import de.sciss.proc.FScape
import de.sciss.rogues.SwapRogue.{SR, T}

object MatchSelect {
  def apply()(implicit tx: T): FScape[T] = {
    val g = Graph {
      import de.sciss.fscape.Ops.*
      import de.sciss.fscape.graph.{AudioFileIn => _, AudioFileOut => _, *}
      import de.sciss.fscape.lucre.graph.*
      import de.sciss.fscape.lucre.graph.Ops.*

      val instrSpanStart  = "start"     .attr[Long]
      val instrSpanStop   = "stop"      .attr[Long]
      val instrNewLen0    = "new-length".attr[Long]
      val dbLen0          = AudioFileIn("database").numFrames
      val phraseLen       = AudioFileIn("phrase"  ).numFrames

      // require(dbSpec.numChannels == 1)
      // require(phSpec.numChannels == 2) // left channel is sound signal, right channel is 'withering'

      def mkDbIn() = AudioFileIn("database") // (dbFile, numChannels = dbSpec.numChannels)

      def mkPhIn() = {
        val in = AudioFileIn("phrase") // (phFile, numChannels = phSpec.numChannels)
        // there is a bloody bug in fscape audio-file-in with the second channel dangling.
        // this seems to fix it. XXX TODO --- is this still the case?
        // Length(in).poll(0, "length-ph-in")
        in out 0
      }

      val SRd = SR.toDouble // "sample-rate".attr(44100.0) // 48000.0
      val fftSize = 1024 // 2048
      val stepDiv = 4
      val sideDur = 0.25
      val numMel  = 42
      val numCoef = 21
      val minFreq = 100.0
      val maxFreq = 14000.0
      // limit, because the Pi is too slow to run the
      // entire 3 minutes in one rotation
      val maxDbDur = 72.0 // 42.0
      val maxDbLen = (maxDbDur * SRd).toLong

      val stepSize    = fftSize / stepDiv
      val sideFrames  = (SRd * sideDur).toInt
      val sideLen     = (sideFrames / stepSize).max(1)
      val covSize     = numCoef * sideLen

      //    - punch-in : start = max(0, min(phrase-length, instr.span.start) - sideFrames); stop = min(phrase-length, start + sideFrames)
      //    - punch-out: start = max(0, min(phrase-length, instr.span.stop )); stop = min(phrase-length, start + sideFrames)
      //    - from this we calculate the offset between the two taps into the db: delay-length = punch-out.start - punch-in.start
      //    - from that we calculate the run length: db.length - sideFrames - delay-length; if that's <= 0, abort here
      //      - from that we calculate the number of window steps (/repetitions)

      val instrNewLen = instrNewLen0 min dbLen0

      // val phraseLen     = phSpec.numFrames
      val punchInStart  = ((phraseLen min instrSpanStart) - sideFrames).max(0)
      val punchInStop   = phraseLen min (punchInStart + sideFrames)

      val punchOutStart = (phraseLen min instrSpanStop).max(0)
      val punchOutStop  = phraseLen min (punchOutStart + sideFrames)

      case class Span(start: GE.L, stop: GE.L) {
        def length: GE.L = stop - start
      }

      val punchLen  = (punchInStop - punchInStart) min (punchOutStop - punchOutStart)
      val punchIn   = Span(punchInStart , punchInStart  + punchLen)
      val punchOut  = Span(punchOutStart, punchOutStart + punchLen)

      // val dbLen         = min(maxDbLen, dbSpec.numFrames)
      val dbLen       = dbLen0 min maxDbLen
      val dbDlyFrames = (instrNewLen - sideFrames).max(0)
      val runFrames   = (dbLen - sideFrames - dbDlyFrames).max(0)
      val runSteps    = (runFrames / stepSize).max(1)

      val dbSpanIn    = Span(0, runFrames)
      val dbSpanOut   = Span(dbDlyFrames, dbDlyFrames + runFrames)

      def mkMatrix(in: GE.D): GE.D = {
        val lap = Sliding(in, fftSize, stepSize) * GenWindow(fftSize, GenWindow.Hann)
        val fft = Real1FFT(lap, fftSize, mode = 2)
        val mag = fft.complex.mag
        val mel = MelFilter(mag, fftSize / 2, bands = numMel,
          minFreq = minFreq, maxFreq = maxFreq, sampleRate = SRd)
        val mfcc = DCT_II(mel.log, numMel, numCoef, zero = 0 /* 1 */)
        mfcc
      }

      def mkPhaseSig(span: Span): GE.D = {
        val in0 = mkPhIn()
        val in1 = /*if (span.start == 0L) in0 else*/ in0.drop(span.start)
        val in = in1.take(span.length)
        val mfcc = mkMatrix(in)
        RepeatWindow(mfcc, size = covSize, num = runSteps)
      }

      def mkDbSig(span: Span): GE.D = {
        val in0 = mkDbIn()
        val in1 = /*if (span.start == 0L) in0 else*/ in0.drop(span.start)
        val in = in1.take(span.length)
        val mfcc = mkMatrix(in)
        val mfccSlid = Sliding(mfcc, size = covSize, step = numCoef)
        mfccSlid
      }

      val sigPunchIn = mkPhaseSig(punchIn)
      val sigPunchOut = mkPhaseSig(punchOut)

      //    val numSteps    = numFrames / stepSize
      //    val numCov      = numSteps - (2 * sideLen)
      //    val numCov1     = numSteps - sideLen - spaceLen

      val sigDbIn   = mkDbSig(dbSpanIn)
      val sigDbOut  = mkDbSig(dbSpanOut)
      val covIn     = Pearson(sigDbIn, sigPunchIn, covSize)
      val covOut    = Pearson(sigDbOut, sigPunchOut, covSize)

      val keys      = (covIn + covOut).take(runSteps)
      val keysEl    = keys.elastic()
      val values    = Frames(keysEl) - 1
      val top       = PriorityQueue(keysEl, values, size = 1) // highest covariances mapped to frames
      val startF    = (top * stepSize) :+ 0L
      MkLong("result", startF)
    }
    val f = FScape[T]()
    f.graph() = g
    f
  }
}
