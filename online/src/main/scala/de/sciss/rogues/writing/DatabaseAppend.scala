/*
 *  DatabaseAppend.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues.writing

import de.sciss.fscape.{GE, Graph}
import de.sciss.proc.FScape
import de.sciss.rogues.SwapRogue.{SR, T}

object DatabaseAppend {
  def apply(verbose: Boolean)(implicit tx: T): FScape[T] = {
    val g = Graph {
      import de.sciss.fscape.Ops.*
      import de.sciss.fscape.graph.{AudioFileIn as _, AudioFileOut as _, *}
      import de.sciss.fscape.lucre.graph.*
      import de.sciss.fscape.lucre.graph.Ops.*

      def mkInApp()   = AudioFileIn("in-app")
      def mkInDb()    = AudioFileIn("in-db")

      val inDb        = mkInDb()
      val inApp0      = mkInApp()
      val captFrames  = "capt-frames".attr[Int]
      val SRd         = SR.toDouble //  "sample-rate".attr(44100.0) // 48000.0

      // Warning: there are still issues with using
      // the same UGens in an If-Then-Els  and
      // its corresponding predicate. The cheesy
      // work around is to make sure we have independent
      // expansion, thus using `def` here instead of `val`.
      def numFrames = captFrames min mkInApp().numFrames.toInt

      def len0  : GE.I = mkInDb().numFrames.toInt
      def fdLen : GE.I = len0 min (numFrames min (SRd * 0.1).toInt) // .toInt

      // adjust to target for 72 phons
      val loudWin   = SRd.toInt
      val inApp0HPF = HPF(inApp0, 150 / SRd)
      val inAppSl   = Sliding(inApp0HPF, size = loudWin, step = loudWin / 2)
      val loud      = Loudness(in = inAppSl, sampleRate = SRd, size = loudWin, spl = 50)

      val (loudRefH, loudTgtH) = {
        val loudH0  = Histogram(loud, bins = 104, lo = 0, hi = 103)
        val loudH   = loudH0.drop(4)
        //  Plot1D(loudH, 128, "LOUD")
        val loudHA  = RunningSum(loudH)
        val loudP   = 0.7 // to obtain a particular percentile
        val loudHAT = loudHA.last * loudP
        val loudM   = WindowIndexWhere(loudHA.toDouble >= loudHAT, 100) + 4 // index corresponds to loudness
        if verbose then loudM.poll("writing loudM")
        (loudM, 63.0)
      }

      val (loudRefM, loudTgtM) = {
        (RunningMax(loud).last, 72.0)
      }

      val loudRef = (loudRefH + loudRefM) * 0.5
      val loudTgt = (loudTgtH + loudTgtM) * 0.5

      val loudCorr = {
        val tgt = loudTgt // 60.0 // 72.0
        val dif = tgt - loudRef
        ((dif.abs.pow(0.85) * dif.signum) * 0.3 /* 1.28 */).dbAmp.min(24)
      }
      val inApp   = (mkInApp() * loudCorr).clip2(1.0)

      val cat: GE.D = If (fdLen /*.elastic(4)*/ sig_== 0) Then {
        //  DC(0).take(1).poll(0, "branch-1")
        inDb ++ inApp
      } Else {
        //  DC(0).take(1).poll(0, "branch-2")
        val preLen  = len0 - fdLen
        val pre     = inDb.take(preLen)
        val cross0  = inDb.drop(preLen)  * Line(1, 0, fdLen).sqrt
        val cross1  = inApp.take(fdLen ) * Line(0, 1, fdLen).sqrt
        val cross   = (cross0 + cross1).clip2(1.0)
        val post    = inApp.drop(fdLen)
        pre ++ cross ++ post
      }
      // AudioFileSpec(AIFF, Int16, numChannels = 1, sampleRate = SR)
      AudioFileOut("out-db" /*file = db1F*/,
        in            = cat,
        sampleRate    = SRd,
        sampleFormat  = 0,
      )
    }

    val f = FScape[T]()
    f.graph() = g
    f
  }
}
