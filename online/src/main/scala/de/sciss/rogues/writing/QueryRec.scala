/*
 *  QueryRec.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues.writing

import de.sciss.proc.Proc
import de.sciss.rogues.SwapRogue.T
import de.sciss.synth.SynthGraph

object QueryRec {
  def apply(micHPF: Float)(implicit tx: T): Proc[T] = {
    val g = SynthGraph {
      import de.sciss.synth.ugen.{DiskOut => _, *}
      import de.sciss.synth.proc.graph.*
      import de.sciss.synth.proc.graph.Ops.*
      import de.sciss.synth.GEOps.*

      // record from the mic input
      val gain  = "gain".ir(5.0f)
      val in0   = PhysicalIn.ar * gain
      val in    = if micHPF > 0f then HPF.ar(in0) else in0
      val dur   = "dur".ir
//      dur.poll(0, "query-rec dur")
      DiskOut.ar("out", in)
      DoneSelf(Done.kr(Line.kr(dur = dur)))
    }
    val p = Proc[T]()
    p.graph() = g
    p
  }
}
