/*
 *  OverwritePerform.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues.writing

import de.sciss.fscape.{GE, Graph}
import de.sciss.proc.FScape
import de.sciss.rogues.SwapRogue.{SR, T}

object OverwritePerform {
  def apply()(implicit tx: T): FScape[T] = {
    val g = Graph {
      import de.sciss.fscape.graph.{AudioFileIn => _, AudioFileOut => _, *}
      import de.sciss.fscape.lucre.graph.*
      import de.sciss.fscape.Ops.*
      import de.sciss.fscape.lucre.graph.Ops.*

      // XXX TODO: `def`s here because of deadlock between
      // If predicate and branch use
      def instrSpanStart  = "start"     .attr[Long]
      def instrSpanStop   = "stop"      .attr[Long]
      def instrNewLen     = "new-length".attr[Long]
      val matchPos        = "match-pos" .attr[Long]

      val WitheringConstant = 0.0078125 // = 1.0/128
      val SRd = SR.toDouble // "sample-rate".attr(44100.0) // 48000.0

      val phPos = instrSpanStart
      val dbPos = matchPos

      // XXX TODO: `def`s here: see above
      def spliceLen   = instrNewLen // min(dbSpan.length, instrNewLen)
      def insSpanLen  = instrSpanStop - instrSpanStart
      def mkInsFdLen  = (insSpanLen / 2) min (spliceLen / 2) min (SRd * 0.1).toInt // .floor

      val inDb = AudioFileIn("in-db") // db0.f, numChannels = 1)
      val inDbW = inDb * (Seq[GE.D](1.0, 0.0): GE.D)
      val inPh0 = AudioFileIn("in-ph") // ph0.f, numChannels = 2)
      val inPh = inPh0 + (Seq[GE.D](0.0, WitheringConstant): GE.D) // withering

      val insPre  = inPh.take(phPos)
      val insPost = inPh.drop(instrSpanStop)
      val insMid  = inDbW.drop(dbPos).take(spliceLen)

      val insCat = If(mkInsFdLen sig_== 0) Then {
        insPre ++ insMid ++ insPost

      } Else {
        val insFdLen = mkInsFdLen
        // we could use .sqrt for left channel and linear for right channel;
        // for simplicity, just use linear for both.
        // NOTE: we do not fade in/out the withering channel, because that
        // would build up stuff at the phrase beginning and end!
        val preOut = inPh.drop(phPos).take(insFdLen) * Line(Seq[GE.D](1.0, 0.0), 0.0, insFdLen) // .sqrt
        val midIn = insMid.take(insFdLen) * Line(Seq[GE.D](0.0, 1.0), 1.0, insFdLen) // .sqrt
        val cross0 = preOut + midIn
        val takeLen: GE.L = spliceLen - (insFdLen * 2)
        val cross1 = insMid.drop(insFdLen).take(takeLen)
        val midOut = insMid.drop(spliceLen - insFdLen) * Line(1.0, Seq[GE.D](0.0, 1.0), insFdLen) // .sqrt
        val postIn = inPh.drop(instrSpanStop - insFdLen).take(insFdLen) * Line(0.0, Seq[GE.D](1.0, 0.0), insFdLen) // .sqrt
        val cross2 = midOut + postIn
        val cross = cross0 ++ cross1 ++ cross2

        insPre ++ cross ++ insPost
      }
      AudioFileOut("out-ph" /*file = ph1F*/ ,
        in = insCat.clip2(1.0),
        sampleRate = SRd,
        sampleFormat = 2,
      )

      val remPre  = inDb.take(dbPos)
      val remPost = inDb.drop(dbPos + spliceLen)

      val remCat = remPre ++ remPost
      AudioFileOut("out-db" /*file = db1F*/ ,
        in = remCat,
        sampleRate = SRd,
        sampleFormat = 0,
      )
    }
    val f = FScape[T]()
    f.graph() = g
    f
  }
}
