/*
 *  Build.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues.writing

import de.sciss.audiofile.{AudioFile, AudioFileSpec, AudioFileType, SampleFormat}
import de.sciss.file.*
import de.sciss.lucre.{Artifact, ArtifactLocation, BooleanObj, DoubleObj, IntObj, LongObj}
import de.sciss.proc.{AudioCue, Control}
import de.sciss.rogues.SwapRogue.{SR, T}

object Build {
  /** @param micGain      linear
    * @param playAmp      linear
    * @param playLimLevel linear
    */
  def apply(baseDir: File, micGain: Double, verbose: Boolean, debug: Boolean, busyPlayRef: BooleanObj[T],
            playChanRef: IntObj[T], playAmp: Double /*, playLimLevel: Double*/, micHPF: Float)
           (implicit tx: T): Control[T] = {
    // ---- files ----
    val dbDirT = baseDir / "db"
    val phDirT = baseDir / "ph"
    val tmpDir = baseDir / "tmp"
    val aDb0  = dbDirT / "db0.aif"
    val aPh0  = phDirT / "ph0.aif"
    val aRec0 = tmpDir / "rec.irc"

    dbDirT.mkdirs()
    phDirT.mkdirs()
    tmpDir.mkdirs()

    AudioFile.openWrite(aDb0, AudioFileSpec(
      AudioFileType.AIFF,
      sampleFormat = SampleFormat.Int16,
      sampleRate = SR,
      numChannels = 1,
    )).close()
    AudioFile.openWrite(aPh0, AudioFileSpec(
      AudioFileType.AIFF,
      sampleFormat = SampleFormat.Float,
      sampleRate = SR,
      numChannels = 2,
    )).close()
    AudioFile.openWrite(aRec0, AudioFileSpec(
      AudioFileType.IRCAM,
      sampleFormat = SampleFormat.Int16,
      sampleRate = SR,
      numChannels = 1,
    )).close()

    val locBaseDir = ArtifactLocation.newConst[T](baseDir.toURI)

    def mkCue(f: File): AudioCue.Obj[T] = {
      val uri   = f.toURI
      val a     = Artifact[T](locBaseDir, uri)
      val spec  = AudioFile.readSpec(f)
//      val cue   = AudioCue(uri, spec, 0L, 1.0)
      AudioCue.Obj[T](a, spec, LongObj.newConst(0L), DoubleObj.newConst(0.0))
//      AudioCue.Obj.newConst[T](cue)
    }

    // ---- objects ----
    val iDbCount        = IntObj.newVar[T](0)
    val iPhCount        = IntObj.newVar[T](0)
    val cueDatabase     = AudioCue.Obj.newVar[T](mkCue(aDb0))
    val cueTmpFile      = AudioCue.Obj.newVar[T](mkCue(aRec0))
    val cuePhrase       = AudioCue.Obj.newVar[T](mkCue(aPh0))

    val cFillDatabase   = FillDatabase(verbose = verbose)
    val aFillDatabase   = cFillDatabase.attr
    val fDatabaseAppend = DatabaseAppend(verbose = verbose)
    val pQueryRec       = QueryRec(micHPF = micHPF)
    val aQueryRec       = pQueryRec.attr
    aQueryRec.put("gain", DoubleObj.newVar[T](micGain))

    aFillDatabase.put("append-db" , fDatabaseAppend)
    aFillDatabase.put("database"  , cueDatabase)
    aFillDatabase.put("db-count"  , iDbCount)
    aFillDatabase.put("query-rec" , pQueryRec)
    aFillDatabase.put("tmp-file"  , cueTmpFile)
    val fMatchSelect      = MatchSelect()
    val fOverwritePerform = OverwritePerform()
    val cOverwriteSelect  = OverwriteSelect()
    val aOverwriteSelect  = cOverwriteSelect.attr
    aOverwriteSelect.put("phrase", cuePhrase)
    val cRender           = Render(verbose = verbose, debug = debug)
    val aRender           = cRender.attr
    aRender.put("database"          , cueDatabase)
    aRender.put("database-fill"     , cFillDatabase)
    aRender.put("db-count"          , iDbCount)
    aRender.put("match-select"      , fMatchSelect)
    aRender.put("overwrite-perform" , fOverwritePerform)
    aRender.put("overwrite-select"  , cOverwriteSelect)
    aRender.put("ph-count"          , iPhCount)
    aRender.put("phrase"            , cuePhrase)
    val cPlayPhrase = PlayPhrase(chanRef = playChanRef, busyRef = busyPlayRef, amp = playAmp /*, limLevel = playLimLevel*/)
    val aPlayPhrase = cPlayPhrase.attr
    aPlayPhrase.put("phrase", cuePhrase)
    val cLoop = Loop(verbose = verbose)  // aka 'Control' in "Writing (simultan)"
    val aLoop = cLoop.attr
    aLoop.put("database"    , cueDatabase)
    aLoop.put("db-count"    , iDbCount)
    aLoop.put("ph-count"    , iPhCount)
    aLoop.put("phrase"      , cuePhrase)
    aLoop.put("play-phrase" , cPlayPhrase)
    aLoop.put("render"      , cRender)
    cLoop
  }
}
