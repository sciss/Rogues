/*
 *  FanControl.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import com.fazecast.jSerialComm.SerialPort
import de.sciss.audiofile.{AudioFile, AudioFileSpec, AudioFileType, SampleFormat}
import de.sciss.file.File
import de.sciss.osc
import org.rogach.scallop.ScallopConf

import java.io.{BufferedReader, FileInputStream, InputStream, InputStreamReader, OutputStream}
import java.net.InetSocketAddress
import scala.swing.event.{ButtonClicked, ValueChanged}
import scala.swing.{Button, CheckBox, Component, Dimension, Graphics2D, GridPanel, Label, MainFrame, Slider, Swing}

/*  Corresponds to `read-serial-test.py` on the Pico, receiving data via USB serial.
 *
 *  Sends bytes 'cRGB\n' to set LED colour.
 */
object FanControl:
  case class ConfigImpl(
                         debug        : Boolean         = false,
                         verbose      : Boolean         = false,
                         serialDevice : String          = SerialSupport.defaultDevice,
                         fanGui       : Boolean         = false,
                       ) extends Config

  trait Config {
    def debug       : Boolean
    def verbose     : Boolean
    def serialDevice: String
    def fanGui      : Boolean
  }

  def main(args: Array[String]): Unit =
    object p extends ScallopConf(args):
      import org.rogach.scallop.{ScallopOption => Opt, *}

      printedName = "FanControl"
      private val default: Config = ConfigImpl()

      val debug: Opt[Boolean] = toggle(default = Some(default.debug),
        descrYes = "Debug operation.",
      )
      val verbose: Opt[Boolean] = toggle(default = Some(default.verbose),
        descrYes = "Verbose operation.",
      )
      val device: Opt[String] = opt(default = Some(default.serialDevice),
        descr = s"Serial device name (default: ${default.serialDevice}).",
      )
      val gui: Opt[Boolean] = toggle(default = Some(default.fanGui),
        descrYes = "Show fan GUI.",
      )

      verify()
      val config: Config = ConfigImpl(
        debug         = debug     (),
        verbose       = verbose   (),
        serialDevice  = device    (),
        fanGui        = gui       (),
      )
    end p

    implicit val c: Config = p.config
    val port = SerialSupport.openPort(c.serialDevice)
    run(port)
    
  type Result = Int => Unit

  def dummy(): Result = _ => ()
  
  def run(port: SerialPort)(implicit c: Config): Result =
    val out = {
      port.getOutputStream
    }

    val bytes = new Array[Byte](11)
    bytes( 0) = 'r' .toByte
    bytes(10) = '\n'.toByte

    def mkCheckBox(): CheckBox = {
      val cb = new CheckBox("Relay")
      cb.reactions += {
        case ButtonClicked(_) =>
          val state = if (cb.selected) 1 else 0
          sendFan(state)
      }
      cb
    }

    lazy val cbOne = mkCheckBox()

    def sendFan(state: Int): Unit = {
      var i = 1

      def push(v: Int): Unit = {
        bytes(i) = ((v % 10)  + 48).toByte; i += 1
        bytes(i) = 48.toByte; i += 1
        bytes(i) = 48.toByte; i += 1
      }
      push(state); push(0); push(0)
      if (c.debug) {
        print(new String(bytes, "UTF-8"))
      }
      out.write(bytes)
      out.flush()
    }

    //    lazy val lb: Component = Button("Next") {
    //      val bytes = "color\n".getBytes("UTF-8")
    //      out.write(bytes)
    //      out.flush()
    //    }

    lazy val lb: Component = new GridPanel(3, 1) {
      contents += cbOne
    }

    if c.fanGui then Swing.onEDT {
      val f = new MainFrame {
        override def closeOperation(): Unit = {
          port.closePort()
          super.closeOperation()
        }
      }
      f.contents = lb
      f.pack().centerOnScreen()
      f.open()
    }

    { (state: Int) =>
      sendFan(state)
    }

  end run