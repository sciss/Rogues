/*
 *  SerialTest.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import com.fazecast.jSerialComm.SerialPort

object SerialTest:
  def main(args: Array[String]): Unit =
    val ports = SerialPort.getCommPorts()
    ports.foreach { p =>
      println(s"$p: baud rate ${p.getBaudRate}, ${p.getNumDataBits}/${p.getParity}/${p.getNumStopBits}, path ${p.getSystemPortPath}, descr1 ${p.getDescriptivePortName}, descr2 ${p.getPortDescription}, system ${p.getSystemPortName}")
    }
