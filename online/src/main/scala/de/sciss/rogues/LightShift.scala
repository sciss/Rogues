/*
 *  LightShift.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.lucre.Disposable
import de.sciss.lucre.synth.{RT, Synth}
import de.sciss.model.Model
import de.sciss.model.impl.ModelImpl
import de.sciss.proc.Universe
import de.sciss.rogues.SwapRogue.{LimiterLookAhead, T}
import de.sciss.synth.UGenSource.Vec
import de.sciss.synth.message
import de.sciss.synth.{GE, SynthGraph, addAfter}
import de.sciss.numbers.Implicits.*
import de.sciss.osc

object LightShift {
  case class ConfigImpl(
    shiftNumBands   : Int     = 6,
    shiftLowestFreq : Float   = 120f,
    shiftHighestFreq: Float   = 18000f,
    shiftMainAmp    : Seq[Float] = Seq(-9f, -9f),
    shiftMainLim    : Float   = -9f,
    shiftFreqRaster : Float   = 1.0f / 50,
    shiftDebug      : Boolean = false,
    shiftShelf      : Boolean = true,
  ) extends Config
  
  trait Config {
    def shiftNumBands     : Int
    def shiftLowestFreq   : Float
    def shiftHighestFreq  : Float
    def shiftFreqRaster   : Float
    /** Decibels */
    def shiftMainAmp      : Seq[Float]
    /** Decibels, negative */
    def shiftMainLim      : Float
    def shiftDebug        : Boolean
    def shiftShelf        : Boolean
  }

  case class Report(rmsAcc: Double)

  trait Result extends Disposable[RT] with Model[Report] {
    def shiftSet(values: Vec[Float])(implicit tx: RT): Unit

    /** Must be called from model listener only */
    def clearRMSAcc(): Unit
  }

  def dummy(): Result = new Result with ModelImpl[Report] {
    override def shiftSet(values: Vec[Float])(implicit tx: RT): Unit = ()

    override def clearRMSAcc(): Unit = ()

    override def dispose()(implicit tx: RT): Unit = ()
  }

  def apply()(implicit tx: T, config: Config, universe: Universe[T]): Result = {
    val s = universe.auralContext.get.server
    val g = SynthGraph {
      import de.sciss.synth.Import._
      import de.sciss.synth.ugen.{DiskIn => _, PartConv => _, _}
      import de.sciss.synth.Ops.stringToControl

      val n       = config.shiftNumBands    // 6
      val fLo     = config.shiftLowestFreq  // 120.0 // 60.0
      val fHi     = config.shiftHighestFreq // 18000.0 // 16000.0
      val in: GE  = In.ar(0, 2)
      var mix     = 0.0 : GE
      var next: GE = in

//      def highpass(in: GE, freq: GE, rq: GE = 1.0): GE = {
//        val rqS     = rq.sqrt
//        val w0      = math.Pi * 2 * freq * SampleDur.ir
//        val cos_w0  = w0.cos
//        val i       = 1.0 + cos_w0
//        val sin_w0  = w0.sin
//        val alpha   = sin_w0 * 0.5 * rqS
//        val b0rz    = (1.0 + alpha).reciprocal
//        val a0      = i * 0.5    * b0rz
//        val a1      = -i         * b0rz
//        val a2      = a0
//        val b1      = cos_w0 * 2 * b0rz
//        val b2      = (1.0 - alpha) * -b0rz
//
//        SOS.ar(
//          SOS.ar(in, a0 = a0, a1 = a1, a2 = a2, b1 = b1, b2 = b2),
//          a0 = a0, a1 = a1, a2 = a2, b1 = b1, b2 = b2
//        )
//      }

      def lowpassB6(in: GE, freq: GE, rq: GE = 1.0): GE = {
        val rqS     = rq.sqrt
        val w0      = math.Pi * 2 * freq * SampleDur.ir
        val cos_w0  = w0.cos
        val i       = 1.0 - cos_w0
        val sin_w0  = w0.sin
        val alpha   = sin_w0 * 0.5 * rqS
        val b0rz    = (1.0 + alpha).reciprocal
        val a0      = i * 0.5    * b0rz
        val a1      = i          * b0rz
        val a2      = a0
        val b1      = cos_w0 * 2 * b0rz
        val b2      = (1.0 - alpha) * -b0rz
        //  a0.poll(0, "a0")
        //  a1.poll(0, "a1")
        //  a2.poll(0, "a2")
        //  b1.poll(0, "b1")
        //  b2.poll(0, "b2")

        SOS.ar(
          SOS.ar(
            SOS.ar(in,
              a0 = a0, a1 = a1, a2 = a2, b1 = b1, b2 = b2),
            a0 = a0, a1 = a1, a2 = a2, b1 = b1, b2 = b2
          ),
          a0 = a0, a1 = a1, a2 = a2, b1 = b1, b2 = b2
        )
      }

//      def lowpassS3(in: GE, freq: GE, rq: GE = 1.0): GE =
//        LPF.ar(LPF.ar(LPF.ar(in, freq), freq), freq)

      def lowpass(in: GE, freq: GE, rq: GE = 1.0): GE =
        lowpassB6(in, freq, rq)

      val shiftAmtVec0  = "shift".kr(Vec.fill(n)(0f))
      val shiftAmtVec1  = shiftAmtVec0.roundTo(config.shiftFreqRaster)
      val shiftAmtVec   = shiftAmtVec1.lag(1f)

      for (band <- 0 until n) {
        val fHPF  = band    .linExp(0f, n.toFloat, fLo, fHi)
        val fLPF  = (band+1).linExp(0f, n.toFloat, fLo, fHi)
        // println(f"band $band, hpf $fHPF%1.1f lpf $fLPF%1.1f")
        var flt   = next
        //  if (band > 0)   flt = highpass(flt, fHPF, rq = 2.0) else flt = flt * 0.5
        //  next = flt
        if (band < n-1) flt = lowpass(flt, fLPF, rq = 2.0) // else flt = flt * 0.5
        next = next - flt

        val shiftAmt  = shiftAmtVec.out(n) // .clip(0, 1)
//        val shiftFreq = shiftAmt.linLin(0, 1, 0, fLPF - fHPF)
        val USE_PITCH = true
        val shift = if USE_PITCH then {
          val tol = 0.05
          val pchRatio = shiftAmt.max(tol).linExp(tol, 1, 1.0, 0.25)
          val pch0 = PitchShift.ar(in, pitchRatio = pchRatio)
          // val pch  = HPZ1.ar(pch0) // HPF.ar(pch0)
          val pch   = HPF.ar(pch0, (fHPF * 0.25).max(20))
          val pan   = shiftAmt.min(tol).linLin(0, tol, -1, +1)
          LinXFade2.ar(in, pch, pan = pan)
        } else {
          val shiftFreq = shiftAmt * (fLPF - fHPF)
          // val shiftFreq = shiftAmt * 1000
          HPF.ar(FreqShift.ar(next, -shiftFreq), shiftFreq.max(30))
        }
        mix += shift // flt * gains(band)
      }

      //mix = highpass(DelayN.ar(in, 0.1, 0.1), 4000, rq = 2)
      val amp   = "amp".kr(Seq.fill(2)(0.25))
      val mixA  = mix * amp

      val rms   = A2K.kr(Lag.ar(mixA.squared, 0.1))
      val rmsM  = rms.out(0) max rms.out(1)
      val rmsTr = Impulse.kr(1)
      SendReply.kr(rmsTr, rmsM, "/$rms")

      val mixB  = if !config.shiftShelf then mixA else BHiShelf.ar(mixA, freq = 4800, rs = 3, gain = -4.5)
      val lim   = Limiter.ar(mixB, level = config.shiftMainLim.dbAmp, dur = LimiterLookAhead)

      if config.shiftDebug then {
        val PP_IN  = Peak.ar(mixA, 0)
        val PP_OUT = Peak.ar(lim, 0)
        PP_IN .out(0).ampDb.poll(1, "LIM IN  [0]")
        PP_IN .out(1).ampDb.poll(1, "LIM IN  [1]")
        PP_OUT.out(0).ampDb.poll(1, "LIM OUT [0]")
        PP_OUT.out(1).ampDb.poll(1, "LIM OUT [1]")
        rmsM         .ampDb.poll(1, "RMS")
      }

      ReplaceOut.ar(0, lim)
    }

    val syn = Synth.play(g, Some("shift"))(s.defaultGroup, addAction = addAfter, args = Seq(
      "amp" -> config.shiftMainAmp.map(_.dbAmp),
    ))

    new Result with ModelImpl[Report] {
      private val SynId       = syn.peer.id
      private var rmsAcc      = 0.0
      private val T0          = System.currentTimeMillis()
      private var rmsReport   = T0
      private val resp = message.Responder.add(syn.server.peer) {
        case osc.Message("/$rms", SynId, _, rmsM: Float) =>
          rmsAcc += rmsM
          val T = System.currentTimeMillis()
          if T > rmsReport then {
            rmsReport = T + 10000L
            if config.shiftDebug then println(f"Accumulative RMS after ${(T - T0) / 1000}%d s: ${rmsAcc.ampDb}%1.1f dBRMS.")
            dispatch(Report(rmsAcc))
          }
      }
      scala.concurrent.stm.Txn.afterRollback(_ => resp.remove())(tx.peer)
      syn.onEnd(resp.remove())

      override def clearRMSAcc(): Unit =
        rmsAcc = 0.0

      override def dispose()(implicit tx: RT): Unit = syn.dispose()

      override def shiftSet(values: Vec[Float])(implicit tx: RT): Unit =
        syn.set("shift" -> values)
    }
  }
}
