/*
 *  Phoresis.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.osc
import de.sciss.rogues.SwapRogue as Main
import de.sciss.rogues.SwapRogue.autoNodeId
import org.rogach.scallop.{ScallopConf, ScallopOption as Opt, given}
import Main.{SR, T, log}
import de.sciss.audiofile.AudioFile
import de.sciss.file.*
import de.sciss.log.Level
import de.sciss.lucre.{ArtifactLocation, BooleanObj, DoubleObj, IntObj, Random, RandomObj}
import de.sciss.proc.{AudioCue, Proc, Runner, SoundProcesses, TimeRef, Universe}
import de.sciss.synth.{Curve, GE, SynthGraph, addToTail}
import de.sciss.numbers.Implicits.*
import de.sciss.proc.Implicits.*
import de.sciss.lucre.Txn.peer as txPeer
import de.sciss.lucre.synth.Synth
import de.sciss.rogues.TransmitColor.NTagMemSize

import java.io.File
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import scala.concurrent.stm.Ref
import scala.util.control.NonFatal

object Phoresis {

  case class ConfigImpl(
                         debug                : Boolean = false,
                         phoresisSendIp       : String  = "127.0.0.1",
                         phoresisSendPort     : Int     = 5005,
                         phoresisReceiveIp    : String  = "127.0.0.1",
                         phoresisReceivePort  : Int     = 5006,
                         phoresisAmp          : Float   = 0f, // -30.0f, // decibels
                         phoresisGendyLoad    : Float   = 0f,
                         phoresisGendySave    : Float   = 0.5f,
                         phoresisColorFreq    : Float   = 12f,
                         phoresisAdjacentSound: Int     = 1,
                         narcissistMinDly     : Float   = 4.0f  * 60,
                         narcissistMaxDly     : Float   = 6.0f  * 60,
                         narcissistMinDur     : Float   = 0.25f * 60,
                         narcissistMaxDur     : Float   = 1.5f  * 60,
                         dirAudio             : File    = new File("audio_work"),
                         testSoundFile        : Option[File] = None,
                         testSoundAmp         : Float   = -10f, // decibels
                         useLED               : Boolean = false,
                         nodeId               : Int     = -1,
                       ) extends Config {
  }

  trait Config {
    def debug               : Boolean
    def phoresisSendIp      : String
    def phoresisSendPort    : Int
    def phoresisReceiveIp   : String
    def phoresisReceivePort : Int
    /** decibels */
    def phoresisAmp         : Float
    /** zero to one */
    def phoresisGendyLoad   : Float
    /** zero to one */
    def phoresisGendySave   : Float
    /** Hz */
    def phoresisColorFreq   : Float
    /** index for file name */
    def phoresisAdjacentSound: Int
    /** seconds */
    def narcissistMinDly    : Float
    /** seconds */
    def narcissistMaxDly    : Float
    /** seconds */
    def narcissistMinDur    : Float
    /** seconds */
    def narcissistMaxDur    : Float
    def nodeId              : Int
    def dirAudio            : File
  }

  def main(args: Array[String]): Unit = {
    Main.printInfo()

    object p extends ScallopConf(args) {
      printedName = "Rogues - Phoresis"
      private val default = ConfigImpl()

      val debug: Opt[Boolean] = toggle(default = Some(default.debug),
        descrYes = "Enter debug mode (verbosity).",
      )
      val phoresisSendIp: Opt[String] = opt(default = Some(default.phoresisSendIp),
        descr = s"Phoresis RFID server IP (default: ${default.phoresisSendIp}).",
      )
      val phoresisSendPort: Opt[Int] = opt(default = Some(default.phoresisSendPort),
        descr = s"Phoresis RFID server port (default: ${default.phoresisSendPort}).",
      )
      val phoresisReceiveIp: Opt[String] = opt(default = Some(default.phoresisReceiveIp),
        descr = s"Phoresis RFID client IP (default: ${default.phoresisReceiveIp}).",
      )
      val phoresisReceivePort: Opt[Int] = opt(default = Some(default.phoresisReceivePort),
        descr = s"Phoresis RFID client port (default: ${default.phoresisReceivePort}).",
      )
      val phoresisAmp: Opt[Float] = opt(default = Some(default.phoresisAmp),
        descr = s"Phoresis amplitude, in decibels (default: ${default.phoresisAmp}).",
      )
      val phoresisGendyLoad: Opt[Float] = opt(default = Some(default.phoresisGendyLoad),
        descr = s"Phoresis loading sonification, gendy amount 0 to 1 (default: ${default.phoresisGendyLoad}).",
      )
      val phoresisGendySave: Opt[Float] = opt(default = Some(default.phoresisGendySave),
        descr = s"Phoresis saving sonification, gendy amount 0 to 1 (default: ${default.phoresisGendySave}).",
      )
      val phoresisColorFreq: Opt[Float] = opt(default = Some(default.phoresisColorFreq),
        descr = s"Phoresis colour frequenzy in Hz (default: ${default.phoresisColorFreq}).",
        validate = _ >= 0.1f,
      )
      val phoresisAdjacentSound: Opt[Int] = opt(default = Some(default.phoresisAdjacentSound),
        descr = s"Phoresis adjacent-index sound index (default: ${default.phoresisAdjacentSound}).",
        validate = _ > 0,
      )
      val narcissistMinDly: Opt[Float] = opt(default = Some(default.narcissistMinDly),
        descr = s"Narcissist minimum delay in seconds (default: ${default.narcissistMinDly}).",
        validate = _ > 0f,
      )
      val narcissistMaxDly: Opt[Float] = opt(default = Some(default.narcissistMaxDly),
        descr = s"Narcissist maximum delay in seconds (default: ${default.narcissistMaxDly}).",
        validate = _ > 0f,
      )
      val narcissistMinDur: Opt[Float] = opt(default = Some(default.narcissistMinDur),
        descr = s"Narcissist minimum duration in seconds (default: ${default.narcissistMinDur}).",
        validate = _ > 0f,
      )
      val narcissistMaxDur: Opt[Float] = opt(default = Some(default.narcissistMaxDur),
        descr = s"Narcissist maximum duration in seconds (default: ${default.narcissistMaxDur}).",
        validate = _ > 0f,
      )
      val testSoundFile: Opt[File] = opt(default = default.testSoundFile,
        descr = s"Play a looped test sound file to observe RW sonification.",
      )
      val testSoundAmp: Opt[Float] = opt(default = Some(default.testSoundAmp),
        descr = s"Test sound file amplitude, in decibels (default: ${default.testSoundAmp}).",
      )
      val useLED: Opt[Boolean] = toggle(name = "use-led", default = Some(default.useLED),
        descrYes = "Use actual LED for display.",
      )
      val dirAudio: Opt[File] = opt(default = Some(default.dirAudio),
        descr = s"Audio file directory (default: ${default.dirAudio})"
      )

      val nodeIdValue = autoNodeId()

      verify()
      implicit val config: ConfigImpl = ConfigImpl(
        debug = debug(),
        phoresisSendIp      = phoresisSendIp(),
        phoresisSendPort    = phoresisSendPort(),
        phoresisReceiveIp   = phoresisReceiveIp(),
        phoresisReceivePort = phoresisReceivePort(),
        phoresisAmp         = phoresisAmp(),
        phoresisGendyLoad   = phoresisGendyLoad(),
        phoresisGendySave   = phoresisGendySave(),
        phoresisColorFreq   = phoresisColorFreq(),
        phoresisAdjacentSound = phoresisAdjacentSound(),
        narcissistMinDly    = narcissistMinDly(),
        narcissistMaxDly    = narcissistMaxDly(),
        narcissistMinDur    = narcissistMinDur(),
        narcissistMaxDur    = narcissistMaxDur(),
        testSoundFile       = testSoundFile.toOption,
        testSoundAmp        = testSoundAmp(),
        nodeId              = nodeIdValue,
        useLED              = useLED(),
        dirAudio            = dirAudio().getAbsoluteFile,
      )
    }
    import p.config
    run()
  }

  def run()(implicit config: ConfigImpl): Unit = {
    if config.debug then log.level = Level.Debug
    val colrOut = if !config.useLED then TransmitColor.dummy() else {
      implicit val tcCfg: TransmitColor.Config = TransmitColor.ConfigImpl()
      val port = SerialSupport.openPort(tcCfg.serialDevice)
      TransmitColor.run(port)
    }
    SoundBody.boot { implicit tx =>
      implicit universe =>
        _ /*s*/ =>
          config.testSoundFile.foreach { fTest =>
            playSoundLoop(fTest)
          }
          val ca      = ChannelAllocator()
          val colrMem = TransmitColor.memory()

          for _ <- 0 until 100 do {
            colrMem.setColor(util.Random.nextInt(0x1000000))
          }

          val colrSwitch = TransmitColor.switch(colrMem, colrOut)
          apply(ca, colrSwitch)
          ()
    }
  }

  type Result = Unit

//  def signalPresence(state: Boolean)(implicit tx: T): Unit = {
//
//  }

  private object ChanKey extends ChannelKey

  private def playSoundLoop(f: File)(implicit tx: T, config: ConfigImpl, universe: Universe[T]): Result = {
    import universe.{cursor, scheduler}
    val p = Proc[T]()
    p.graph() = SynthGraph {
      import de.sciss.synth.ugen.{VDiskIn => _, *}
      import de.sciss.synth.Import.*
      import de.sciss.synth.proc.graph.*
      import de.sciss.synth.proc.graph.Ops.*
      val amp     = "amp".kr(0.5)
      val disk    = VDiskIn.ar("disk", loop = 1)
      val diskSt: GE = Seq(disk.out(0), disk.out(1))
      val sig     = HPF.ar(diskSt, 50) * amp
      PhysicalOut.ar(0, sig)
    }
    val a     = p.attr
    val spec  = AudioFile.readSpec(f)
    val cueV  = AudioCue(f.toURI, spec, offset = 0L, gain = 1.0)
    val cue   = AudioCue.Obj.newConst[T](cueV)
    a.put("disk", cue)
    a.put("amp", DoubleObj.newConst[T](config.testSoundAmp.dbAmp))
    val r     = Runner(p)
    r.run()
  }

  private final val COOKIE_1  = 0x50 // 'P'
  private final val COOKIE_2  = 0x68 // 'h'

  def apply(channelAllocator: ChannelAllocator, colrSwitch: TransmitColor.Switch[TransmitColor.Memory, TransmitColor.Result])
           (implicit tx: T, config: Config, universe: Universe[T]): Result = {
    import universe.{cursor, scheduler}

    val colrMem       = colrSwitch.branchA
    val colrSet       = colrSwitch.branchB

    val lastLoadRef   = Ref(0L)
    val lastSaveRef   = Ref(0L)
    val shouldSaveRef = Ref(false)
    val twoSecFr      = (TimeRef.SampleRate *  2).toLong
    val tenSecFr      = (TimeRef.SampleRate * 10).toLong
    val thirtySecFr   = (TimeRef.SampleRate * 30).toLong
    val replayFr      = (TimeRef.SampleRate * (1.0 / config.phoresisColorFreq)).toLong
    val presTokenRef  = Ref(-1)   // schedule for signal presence
    val rwTokenRef    = Ref(-1)   // schedule for read/write time-out

    val random: Random[T] = RandomObj()

    // ---- signal indicator ----

    val dirAudio  = config.dirAudio // new File(userHome, "Documents/projects/Klangnetze/audio_work")
    val nameSig   = s"adjacent-things-${config.phoresisAdjacentSound}-phoretic.aif"
    val artSig    = dirAudio / nameSig
    val specSig   = AudioFile.readSpec(artSig)
    log.info(s"adjacent cue spec $specSig")
    val cueSig0   = AudioCue(artSig.toURI, specSig, offset = 0L, gain = 1.0)

    def mkCueSig(state: Boolean)(implicit tx: T): AudioCue.Obj[T] = {
      val durH    = specSig.duration * 0.5
      val offSec0 = random.nextDouble().linLin(0.0, 1.0, 0, durH - 2.0)
      val offSec  = if state then offSec0 + durH else offSec0
      val offFr   = (TimeRef.SampleRate * offSec).toLong
      val cueSig  = cueSig0.copy(offset = offFr)
      AudioCue.Obj.newConst[T](cueSig)
    }

    val pSig      = Proc[T]()
    pSig.name     = "rfid-sig"
    pSig.graph()  = SynthGraph {
      import de.sciss.synth.ugen.{DiskIn => _, VDiskIn => _, *}
      import de.sciss.synth.Import.*
      import de.sciss.synth.proc.graph.*
      import de.sciss.synth.proc.graph.Ops.*
//      val freq  = "freq".kr(440)
//      val osc   = Pulse.ar(freq)

      val disk  = DiskIn.ar("cue", loop = 0)
      val osc   = disk.out(0)
      if config.debug then {
        osc.poll(0, "rfid-sig-play")
      }
      val amp   = "amp".kr(0.1)
      val dur   = "dur".kr(1.2)
      val env   = EnvGen.ar(Env.perc(attack = 0.02, release = dur, curve = Curve.welch), levelScale = amp)
      DoneSelf(Done.kr(env))
      val bus   = "bus".ir
      PhysicalOut.ar(bus, osc * env)
    }
    val pSigAttr  = pSig.attr
    val amp       = config.phoresisAmp.dbAmp
//    val vrFreq    = DoubleObj.newVar[T](0.0)
    val vrCueSig  = AudioCue.Obj.newVar[T](mkCueSig(false))
    val vrBus     = IntObj.newVar[T](0)
    val vrSigDur  = DoubleObj.newVar[T](0.0)
    pSigAttr.put("amp" , DoubleObj.newConst[T](amp))
    pSigAttr.put("bus" , vrBus)
    pSigAttr.put("dur" , vrSigDur)
//    pSigAttr.put("freq", vrFreq)
    pSigAttr.put("cue", vrCueSig)
    val rSig = Runner[T](pSig)
//    val pSigH = tx.newHandle(pSig)
    rSig.react { implicit tx => rState =>
      if rState.idle then channelAllocator.releaseChannel(ChanKey)
    }

    // ---- read/write sonification ----

//    val pRW     = Proc[T]()
//    pRW.name    = "rfid-rw"
    val gRW = SynthGraph {
      import de.sciss.synth.ugen.*
      import de.sciss.synth.Import.*
      import de.sciss.synth.proc.graph.*
//      import de.sciss.synth.proc.graph.Ops.*
      import de.sciss.synth.Ops.stringToControl
      val in      = In.ar(0, 2)
      val pAmt    = "amt".kr // pAudio("amt", ParamSpec(0, 1), default(0.0))
      val amt     = Lag.ar(pAmt, 2.0)
      val minFreq = amt * 69 + 12
      val scale   = amt * 13 + 0.146
      val gendy   = Gendy1.ar(2, 3, 1, 1,
        minFreq   = minFreq,
        maxFreq   = minFreq * 8,
        ampScale  = scale,
        durScale  = scale,
        initCPs   = 7,
        kNum      = 7) * in
      val flt = Compander.ar(gendy, gendy, 0.7, 1, 0.1, 0.001, 0.02)
      // mix(in, flt, pMix)
      val gate    = "gate".kr(1)
      val env     = EnvGen.ar(Env.asr(attack = 1.5, release = 3.0, curve = Curve.welch), gate = gate)
      val done    = Done.kr(env)
//      DoneSelf(done)
      if config.debug then {
        gate.poll(done, "rfid-rw-free")
      }
      FreeSelf.kr(Done.kr(env))
      val wWet    = env
      val wDry    = 1.0 - env
      val mix     = in * wDry + flt * wWet
      ReplaceOut.ar(0, mix)
      if config.debug then {
        gate.poll(0, "rfid-rw-play")
      }
    }
//    pRW.graph() = gRW
//    val pRWAttr = pSig.attr
//    val vrRWGate = BooleanObj.newVar[T](true)
//    val vrRWAmt  = DoubleObj .newVar[T](0.0)
//    pRWAttr.put("gate", vrRWGate)
//    pRWAttr.put("amt" , vrRWAmt )
//    val rRW     = Runner[T](pRW)

    val synRWRef = Ref(Option.empty[Synth])
    val s = universe.auralContext.get.server

    def releaseSynRW()(implicit tx: T): Unit =
      synRWRef.swap(None).foreach(_.set("gate" -> 0))

    // ---- narcissism ----

    val narcDlyTokenRef   = Ref(-1)   // schedule for narcissism
    val narcDurTokenRef   = Ref(-1)   // schedule for narcissism stop
    val colorReplayTokenRef = Ref(-1)
    val colorPhoresis     = Ref(false)
    val otherColorsRef    = Ref(Array.empty[Int])
    val otherColorsIdxRef = Ref(0)

    def unbecomeNarc()(implicit tx: T): Unit = {
      log.info("narcissim stop")
      colrSwitch.selectA()
      if !colorPhoresis() then {
        colrSet.setColor(0)
      }
      nextNarc()
    }

    def becomeNarc()(implicit tx: T): Unit = {
      if !colorPhoresis() then {
        log.info("narcissism start")
        colrSwitch.selectB()
        val delaySec  = random.nextDouble().linLin(0.0, 1.0, config.narcissistMinDur, config.narcissistMaxDur)
        val delayFr   = (TimeRef.SampleRate * delaySec).toLong
        val now       = scheduler.time
        val token     = scheduler.schedule(now + delayFr) { implicit tx =>
          unbecomeNarc()
        }
        scheduler.cancel(narcDurTokenRef.swap(token))

      } else {
        nextNarc()
      }
    }

    def nextNarc()(implicit tx: T): Unit = {
      val delaySec  = random.nextDouble().linLin(0.0, 1.0, config.narcissistMinDly, config.narcissistMaxDly)
      log.info(f"narcissism in ${delaySec}%1.1f seconds")
      val delayFr   = (TimeRef.SampleRate * delaySec).toLong
      val now       = scheduler.time
      val token     = scheduler.schedule(now + delayFr) { implicit tx =>
        becomeNarc()
      }
      scheduler.cancel(narcDlyTokenRef.swap(token))
    }

    def replayNextColor()(implicit tx: T): Unit = {
      val token = scheduler.schedule(scheduler.time + replayFr) { implicit tx =>
        val idx     = otherColorsIdxRef()
        val clr     = otherColorsRef()
        if idx < clr.length then {
          val rgb = clr(idx)
          otherColorsIdxRef() = idx + 1
          colrSet.setColor(rgb)
          replayNextColor()

        } else {
          log.info("replayColors done")
          otherColorsRef() = Array.empty[Int]
          colorPhoresis() = false
          colrSet.setColor(0)
          nextNarc()
        }
      }
      scheduler.cancel(colorReplayTokenRef.swap(token))
    }

    def replayColors()(implicit tx: T): Unit = if otherColorsRef().nonEmpty then {
      log.info("replayColors()")
      colorPhoresis() = true
      colrSwitch.selectA()
      otherColorsIdxRef() = 0
      replayNextColor()
    }

    // ---- OSC nodes ----

    val rcvSrc    = new InetSocketAddress(config.phoresisReceiveIp, config.phoresisReceivePort)
    val trnsTgt   = new InetSocketAddress(config.phoresisSendIp   , config.phoresisSendPort)
    val trnsCfg   = osc.UDP.Config()
    val rcvCfg    = osc.UDP.Config()
    trnsCfg.localIsLoopback   = config.phoresisReceiveIp == "127.0.0.1"
    rcvCfg.localSocketAddress = rcvSrc
    val oscTrns = osc.UDP.Transmitter (trnsTgt, trnsCfg)
    val oscRcv  = osc.UDP.Receiver    (rcvCfg )

    def saveAndSendColors()(implicit tx: T): Unit = {
      log.info("saveAndSendColors()")
      val now = scheduler.time
      lastSaveRef() = now
      shouldSaveRef() = false
      val used = math.min(NTagMemSize, colrMem.used)
      if used > 0 then {
        val sz0 = used * 3 + 4
        val sz = (sz0 + 3) / 4 * 4 // entire pages
        val arr = new Array[Byte](sz)
        arr(0) = COOKIE_1
        arr(1) = COOKIE_2
        arr(2) = (config.nodeId >> 8).toByte
        arr(3) = config.nodeId.toByte
        colrMem.getBytes(arr, 4)
        val data = ByteBuffer.wrap(arr)
        tx.afterCommit {
          oscTrns ! osc.Message("/save", data)
        }
      }
    }

    oscRcv.action = { (p, _ /*addr*/) => p match {
      case osc.Message("/present", stateI: Int) =>
        val state = stateI != 0
        log.info(s"RFID present: $state")
        SoundProcesses.step[T]("presence") { implicit tx =>
//          vrFreq()  = if state then 660.0 else 440.0
          vrCueSig()  = mkCueSig(state = state)
          vrSigDur()  = random.nextDouble().linLin(0.0, 1.0, 1.0, 4.0)
          vrBus()     = channelAllocator.allocChannel(ChanKey)
          rSig.run()

          if state then {
            val now         = scheduler.time
            val shouldLoad  = now - lastLoadRef() > tenSecFr
            val shouldSave  = now - lastSaveRef() > tenSecFr
            log.info(s"RFID should-load: $shouldLoad, should-save: $shouldSave")
            if shouldLoad || shouldSave then {
              val token = scheduler.schedule(now + twoSecFr) { implicit tx =>
                // start the sonic indicator
                // vrRWGate()  = true
                val gendyAmt = if shouldLoad then config.phoresisGendyLoad else config.phoresisGendySave
                val now2    = scheduler.time
                val synRW   = Synth.play(gRW, Some("rfid-rw"))(s.defaultGroup, args = Seq(
                  "amt" -> gendyAmt,
                ), addAction = addToTail)
                val token2  = scheduler.schedule(now2 + thirtySecFr) { implicit tx =>
                  // vrRWGate()  = false
                  if synRWRef().contains(synRW) then {
                    releaseSynRW()
                  }
                }
                scheduler.cancel(rwTokenRef.swap(token2))
                // rRW.run()
                synRWRef.swap(Some(synRW)).foreach(_.free())

                if shouldLoad then {
                  lastLoadRef()   = now2
                  shouldSaveRef() = shouldSave
                  tx.afterCommit {
                    oscTrns ! osc.Message("/load", /* max-len */ 500)
                  }
                } else /* implies shouldSave */ {
                  saveAndSendColors()
                }
              }
              scheduler.cancel(presTokenRef.swap(token))
            }
          } else {
            scheduler.cancel(presTokenRef.swap(-1)) // card was removed, cancel delayed action
          }
        }

      case osc.Message("/data", bb: ByteBuffer) =>
        SoundProcesses.step[T]("phoresis-data") { implicit tx =>
          val sz    = bb.remaining()
          val data  = new Array[Byte](sz)
          bb.get(data)  // well, do we really need to make a copy?
          if sz >= 4 && data(0) == COOKIE_1 && data(1) == COOKIE_2 then {
            val thatId = ((data(2) & 0xFF) << 8) | (data(3) & 0xFF)
            if thatId == config.nodeId then {
              log.info("Phoresis, data was ours")
              // XXX TODO: should we react somehow?
            } else {
              val numColr = (sz - 4) / 3
              log.info(s"Phoresis, data came from $thatId, $numColr colours, exciting!")
              val colors = new Array[Int](numColr)
              var i = 0
              var j = 4
              while i < numColr do {
                val r = data(j) & 0xFF; j += 1
                val g = data(j) & 0xFF; j += 1
                val b = data(j) & 0xFF; j += 1
                val rgb = (r << 16) | (g << 8) | b
                colors(i) = rgb
                i += 1
              }
              otherColorsRef() = colors
              if shouldSaveRef() then saveAndSendColors() else replayColors()
            }
          } else {
            log.warn("Phoresis data does not begin with cookies")
            if shouldSaveRef() then saveAndSendColors()
          }
        }

      case osc.Message("/done", cmd: String) =>
        log.warn(s"RFID done $cmd")
        SoundProcesses.step[T]("phoresis-done") { implicit tx =>
//          vrRWGate()  = false
          releaseSynRW()
          scheduler.cancel(rwTokenRef.swap(-1))
          if cmd == "save" then replayColors()
        }

      case osc.Message("/fail", cmd: String) =>
        log.warn(s"RFID fail $cmd")
        SoundProcesses.step[T]("phoresis-fail") { implicit tx =>
//          vrRWGate()  = false
          releaseSynRW()
          scheduler.cancel(rwTokenRef.swap(-1))
          if cmd == "save" then replayColors()
        }

      case p =>
        log.warn(s"Ignoring RFID OSC packet ${p.name}")
    }}

    nextNarc()

    tx.afterCommit {
      try {
        oscTrns .connect()
        oscRcv  .connect()
        oscTrns ! osc.Message("/register")
      } catch {
        case NonFatal(ex) =>
          log.error("Could not start Phoresis OSC client")
          ex.printStackTrace()
      }
    }
  }
}
