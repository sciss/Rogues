/*
 *  SerialSupport.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import com.fazecast.jSerialComm.SerialPort

object SerialSupport:
  val defaultDevice = "/dev/ttyACM1" // "/dev/ttyUSB0"
  val baudRate      = 115200

  def openPort(device: String): SerialPort =
    val ports = SerialPort.getCommPorts()
    val port = ports.find(_.getSystemPortPath == device).getOrElse(sys.error(s"Device $device not found"))
    val opened = port.openPort()
    val baudOk = port.setBaudRate(baudRate)
    if !baudOk then println(s"BAUD RATE $baudRate IS NOT VALID")
    require(opened, s"Could not open $device")
    port
