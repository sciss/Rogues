/*
 *  Writing.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.log.Level
import de.sciss.lucre.{BooleanObj, DoubleObj, DoubleVector, Expr, ExprLike, IntObj}
import de.sciss.numbers.Implicits.*
import de.sciss.proc.{Proc, Runner, TimeRef, Universe}
import de.sciss.synth.{Curve, SynthGraph}
import de.sciss.synth.UGenSource.Vec
import de.sciss.rogues.SwapRogue as Main
import Main.{T, log}
import de.sciss.file.*
import de.sciss.lucre.expr.SourcesAsRunnerMap
import de.sciss.model.Change
import org.rogach.scallop
import org.rogach.scallop.{ScallopConf, ScallopOption as Opt, given}

object Writing {
  case class ConfigImpl(
                         writingDebug       : Boolean = false,
                         verbose            : Boolean = false,
                         writingMicGain     : Float   = 2.0f,
                         writingPlayGain    : Float   = 0.0f,
//                         writingLimiterLevel: Float   = -12f,
                         writingBaseDir     : File    = file("/mnt/ramdisk"), // File.tempDir,
                         writingFan         : Boolean = true,
                         serialDevice       : String  = SerialSupport.defaultDevice,
                         micHPF             : Float   = 50f,
                       ) extends Config {

    val hasSerial: Boolean = serialDevice != "none"
  }

  trait Config {
    def writingDebug     : Boolean
    /** Decibels */
    def writingMicGain   : Float

    /** Decibels */
    def writingPlayGain : Float

//    /** Decibels (negative) */
//    def writingLimiterLevel : Float

    def writingBaseDir  : File

    def writingFan      : Boolean

    def micHPF: Float

    def verbose         : Boolean
  }

  def main(args: Array[String]): Unit = {
    Main.printInfo()

    object p extends ScallopConf(args) {
      printedName = "Rogues - Writing"
      private val default = ConfigImpl()

      val baseDir: Opt[File] = opt(default = Some(default.writingBaseDir),
        descr = s"Base directory for audio files (default: ${default.writingBaseDir}).",
      )
      val debug: Opt[Boolean] = toggle(default = Some(default.writingDebug),
        descrYes = "Enter debug mode (verbosity, control files).",
      )
      val verbose: Opt[Boolean] = toggle(default = Some(default.verbose),
        descrYes = "Verbose operation.",
      )
      val micGain: Opt[Float] = opt(default = Some(default.writingMicGain),
        descr = s"Writing microphone gain, in decibels (default: ${default.writingMicGain}).",
      )
      val playGain: Opt[Float] = opt(default = Some(default.writingPlayGain),
        descr = s"Writing playback gain, in decibels (default: ${default.writingPlayGain}).",
      )
//      val limiterLevel: Opt[Float] = opt(default = Some(default.writingLimiterLevel),
//        descr = s"Writing limiter level, in decibels, negative (default: ${default.writingLimiterLevel}).",
//        validate = x => x <= 0
//      )
      val micHPF: Opt[Float] = opt(default = Some(default.micHPF),
        descr = s"Microphone input high pass filter frequency or zero (default: ${default.micHPF}).",
      )

      val fanControl: Opt[Boolean] = toggle(default = Some(default.writingFan),
        descrYes = "Control fan while processing.",
        descrNo  = "Do not control fan while processing.",
      )
      val device: Opt[String] = opt(default = Some(default.serialDevice),
        descr = s"Serial device name, or 'none' (default: ${default.serialDevice}).",
      )

      verify()
      implicit val config: ConfigImpl = ConfigImpl(
        writingDebug        = debug(),
        verbose             = verbose(),
        writingBaseDir      = baseDir(),
        writingMicGain      = micGain(),
        writingPlayGain     = playGain(),
//        writingLimiterLevel = limiterLevel(),
        micHPF              = micHPF(),
        writingFan          = fanControl(),
        serialDevice        = device(),
      )
    }
    import p.config
    run()
  }

  trait Result {
    def runner: Runner[T]

    def release()(implicit tx: T): Unit

    def busyRec   ()(implicit tx: T): ExprLike[T, Boolean]
    def busyRender()(implicit tx: T): ExprLike[T, Boolean]
  }

  def run()(implicit config: ConfigImpl): Unit = {
//    if (config.writingDebug) {
//      log.level = Level.Debug
//      val pid = ProcessHandle.current().pid
//      println(s"PID $pid")
//      de.sciss.fscape.Log.stream  .level = Level.Info
//      de.sciss.fscape.Log.control .level = Level.Info
//    }

    Main.init()
    SoundBody.boot { implicit tx => implicit universe => _ /*s*/ =>
      // val channelAllocator = ChannelAllocator()
      val renderRef = BooleanObj.newVar[T](false)
      val playRef   = BooleanObj.newVar[T](false)
      val chanRef   = IntObj.newConst[T](0)
      val awakeRef  = BooleanObj.newConst[T](true)
      val res       = apply(busyRenderRef = renderRef, busyPlayRef = playRef, playChanRef = chanRef,
        awakeRef = awakeRef)

      if config.writingFan then {
        lazy val port = SerialSupport.openPort(config.serialDevice)
        implicit val cfgFan: FanControl.Config = FanControl.ConfigImpl(
          debug         = config.writingDebug,
          verbose       = config.verbose,
          serialDevice  = config.serialDevice,
        )
        lazy val fan: FanControl.Result = FanControl.run(port)
        tx.afterCommit { if config.hasSerial then fan; () }

        res.busyRender().changed.react { implicit tx => ch =>
          import ch.{now => busy}
          tx.afterCommit {
            if config.writingDebug then println(s"Writing busy render: $busy")
            if config.hasSerial then fan(if busy then 1 else 0)
          }
        }
      }

      res.runner.run()

      res.runner.reactNow { implicit tx => state =>
        // println(s"WRITING STATE $state")
        if (state.idle) {
          log.info("Timbre released")
          tx.afterCommit {
            sys.exit()
          }
        }
      }
    }
  }

  /** Note: caller must call `.runner.run()` */
  def apply(/*channelAllocator: ChannelAllocator,*/ busyRenderRef: BooleanObj[T], busyPlayRef: BooleanObj[T],
            playChanRef: IntObj[T], awakeRef: BooleanObj[T])
           (implicit tx: T, config: Config, universe: Universe[T]): Result = {
    val c = writing.Build(
      baseDir       = config.writingBaseDir,
      micGain       = config.writingMicGain.dbAmp,
      verbose       = config.verbose,
      debug         = config.writingDebug,
      busyPlayRef   = busyPlayRef,
      playChanRef   = playChanRef,
      playAmp       = config.writingPlayGain.dbAmp,
//      playLimLevel  = config.writingLimiterLevel.dbAmp,
      micHPF        = config.micHPF
    )
    val cAttr = c.attr

    val vrNext        = BooleanObj.newVar[T](true )
    val vrBusyRec     = BooleanObj.newVar[T](false)
//    val vrBusyRender  = BooleanObj.newVar[T](false)
    cAttr.put("next", vrNext)
    cAttr.put("busy-rec"    , vrBusyRec    )
    cAttr.put("busy-render" , busyRenderRef)
    cAttr.put("awake", awakeRef)
    val r = Runner(c)
//    r.prepare(
//      new SourcesAsRunnerMap(
//        Map("busy-render" -> Left(tx.newHandle(vrBusyRender))) // doesn't work
//      )
//    )
//    r.run()
    //    r.reactNow { implicit tx => state =>
    //      if (state.idle) done(tx)
    //    }

    new Result {
      override val runner: Runner[T] = r

      override def busyRec    ()(implicit tx: T): ExprLike[T, Boolean] = vrBusyRec
      override def busyRender ()(implicit tx: T): ExprLike[T, Boolean] = busyRenderRef

      override def release()(implicit tx: T): Unit =
        vrNext() = false
    }
  }
}
