/*
 *  CrypsisStage.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.lucre.Txn.peer
import de.sciss.numbers.Implicits._
import de.sciss.proc.TimeRef
import de.sciss.rogues.{SwapRogue => Main}
import Main.{T, log}

import scala.concurrent.stm.Ref

class CrypsisStage extends Stage.Running with ChannelKey { chanKey =>
  private val crypRef   = Ref(Option.empty[Crypsis.Result])
  //  private val rcvRef    = Ref(Disposable.empty[T])
  private val tokenRef    = Ref(-1)
  private val schTimeRef  = Ref(0L)

  override def stage: Stage = Stage.Crypsis

  private def calcAmpDb()(implicit tx: T, body: SoundBody): Float = {
    import body.{config}
    val ampDb0 = config.crypSpeakerAmp
    val ampDb = /*if (config.isLoudCard) (ampDb0 - 9) else*/ if body.isAwake then ampDb0 else {
      val dim = body.random.nextFloat().linLin(0, 1, 9, 12)
      ampDb0 - dim
    }
    log.info(f"crypsis amp ${ampDb}%1.1f dB")
    ampDb
  }

  override def lull ()(implicit tx: T, body: SoundBody): Unit = {
    updateAmp()
    scheduleRelease() // possibly shorten current playing
  }

  override def awake()(implicit tx: T, body: SoundBody): Unit = {
    import body.{config, random, universe}
    if random.nextDouble() > 0.25 then release() else updateAmp()
  }

  private def updateAmp()(implicit tx: T, body: SoundBody): Unit = {
    crypRef() match {
      case Some(cryp) =>
        val ampDb = calcAmpDb()
        cryp.setAmpDb(ampDb)

      case None => ()
    }
  }

  private def scheduleRelease()(implicit tx: T, body: SoundBody): Unit = {
    import body.{config, random, universe}
    // when lulled, play shorter, so that after awakening,
    // we proceed faster to other stages
    val durF        = if body.isAwake then 1f else 0.4f
    val minDur      = config.initCrypMinDur * durF
    val maxDur      = config.initCrypMaxDur * durF
    val dlySec      = random.nextDouble().linLin(0.0, 1.0, minDur, maxDur)
    val dlyFr       = (dlySec * TimeRef.SampleRate).toLong
    val sch         = universe.scheduler
    val schTime     = sch.time + dlyFr
    val oldSchTime  = schTimeRef()
    val oldToken    = tokenRef()
    // don't reschedule if already running and new schedule would be longer
    if oldToken == -1 || oldSchTime > schTime then {
      log.info(f"Crypsis duration in seconds: $dlySec%1.1f")
      val token = sch.schedule(schTime) { implicit tx =>
        release()
      }
      schTimeRef() = schTime
      sch.cancel(tokenRef.swap(token))
    }
  }

  override def start()(implicit tx: T, body: SoundBody): Unit = {
    import body.{config, random, universe}
    val spacePos  = body.spacePos
    val modFreq   = if (spacePos.isEmpty) config.crypModFreq else {
      val sz: Int = spacePos.size.clip(config.minSpacePos, config.maxSpacePos)
      sz.linLin(config.minSpacePos.toFloat, config.maxSpacePos.toFloat, config.crypModMinFreq, config.crypModMaxFreq)
    }
    log.info(f"Crypsis mod-freq is $modFreq%1.2f Hz")
    val channel   = body.allocChannel(chanKey)
    val ampDb     = calcAmpDb()
    val cryp      = Crypsis.applyWith(ampDb = ampDb, modFreq = modFreq, channel = channel)
    crypRef()     = Some(cryp)
    /*val lCryp: Disposable[T] =*/ cryp.runner.reactNow { implicit tx => state =>
      if (state.idle) {
        // lCryp.dispose()
        if (crypRef.swap(None).contains(cryp)) {
          cryp.runner.dispose()
          body.releaseChannel(chanKey)
          body.released(stage)
        }
      }
    }
    scheduleRelease()
  }

  override def release()(implicit tx: T, body: SoundBody): Unit = {
    crypRef().foreach { cryp =>
      log.debug("Releasing crypsis (and biphase rcv)")
      cryp.release()
    }
    //    rcvRef.swap(Disposable.empty).dispose()
    val sch = body.universe.scheduler
    sch.cancel(tokenRef.swap(-1))
  }
}
