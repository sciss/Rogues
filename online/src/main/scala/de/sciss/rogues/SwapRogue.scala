/*
 *  SwapRogue.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import akka.pattern.Patterns
import com.fazecast.jSerialComm.SerialPort
import de.sciss.file.*
import de.sciss.log.{Level, Logger}
import de.sciss.lucre.{BooleanObj, Cursor, IntObj}
import de.sciss.lucre.synth.InMemory

import java.awt.{Color, RenderingHints}
import java.awt.geom.{AffineTransform, Path2D}
import java.awt.image.BufferedImage
import java.util.Timer
import scala.swing.{Component, Dimension, Graphics2D, MainFrame, Point, Swing}
import scala.swing.event.{Key, KeyPressed}
import de.sciss.numbers.Implicits.*
import de.sciss.proc.{FScape, Pattern, SoundProcesses}
import de.sciss.synth.Curve
import org.rogach.scallop.{ScallopConf, ScallopOption as Opt}
import de.sciss.rogues.SwapRogue as Main

import java.awt.event.{KeyAdapter, KeyEvent}
import java.net.InetAddress
import math.{abs, max, min}
import javax.imageio.ImageIO
import javax.swing.JComponent
import scala.collection.immutable.ArraySeq
import scala.util.Random
import scala.util.control.NonFatal

object SwapRogue:

  type T = InMemory.Txn

  final val SR = 48000  // Hz

  final val AssumedSensorRate = 13.65 // fps

  final val LimiterLookAhead = 0.01 // seconds

  trait ConfigAll extends Config
    with Visual.Config
    with DetectSpace.Config
    with Crypsis.Config
    with SpaceTimbre.Config
    with Accelerate.Config
    with Silence.Config
    with Writing.Config
    with ReceiveSensors.Config
    with FanControl.Config
    with LightShift.Config
    with TransmitColor.Config
    with Phoresis.Config

  val log: Logger = new Logger("rogues")

  def thisIP(): String = {
    import sys.process._
    // cf. https://unix.stackexchange.com/questions/384135/
    val ifConfig = Seq("ip", "a", "show", "eth0").!!
    val ifConfigPat = "inet "
    val line = ifConfig.split("\n").map(_.trim).find(_.startsWith(ifConfigPat)).getOrElse("")
    val i0 = line.indexOf(ifConfigPat)
    val i1 = if (i0 < 0) 0 else i0 + ifConfigPat.length
    val i2 = line.indexOf("/", i1)
    if (i0 < 0 || i2 < 0) {
      val local = InetAddress.getLocalHost.getHostAddress
      Console.err.println(s"No assigned IP4 found in eth0! Falling back to $local")
      local
    } else {
      line.substring(i1, i2)
    }
  }

  private final val NodeIdWindow    = 1
  private final val NodeIdStripey   = 2
  private final val NodeIdTrittico  = 3
  private final val NodeIdTower     = 4

  private val dotToNodeId = Map.apply[Int, Int](
    35 -> NodeIdWindow,
    43 -> NodeIdStripey,
    45 -> NodeIdTrittico,
    36 -> NodeIdTower,
  )

  def resolveNodeId(ip: String): Int = {
    val dot0 = {
      val i = ip.lastIndexOf(".") + 1
      try {
        val last = ip.substring(i).toInt
        dotToNodeId(last)
      } catch {
        case NonFatal(_) => -1
      }
    }
    val res = if (dot0 >= 0) dot0 else 0
    if (dot0 < 0) println(s"Warning - could not determine 'dot' for IP $ip - assuming $res")
    res
  }

  def version : String = buildInfString("version")
  def builtAt : String = buildInfString("builtAtString")
  def name    : String = buildInfString("name")
  def fullName: String = s"$name v$version"

//  def scalaVersion: String = buildInfString("scalaVersion")

  private def buildInfString(key: String): String = try {
    val clazz = Class.forName("de.sciss.rogues.BuildInfo")
    val m = clazz.getMethod(key)
    m.invoke(null).toString
  } catch {
    case NonFatal(_) => "?"
  }

  def printInfo(): Unit = {
    println(s"$fullName, built $builtAt")
  }
  
  case class ConfigImpl(
                         nodeId           : Int     = -1,
                         verbose          : Boolean = true,
                         isLaptop         : Boolean = false,
                         visual           : Boolean = true,
                         sound            : Boolean = true,
                         writing          : Boolean = true,
                         debug            : Boolean = false,
                         serialDevice     : String  = SerialSupport.defaultDevice,
                         sleepTime        : Float   = 18.8f, // 56.2f, // 45f,
                         lightShift       : Boolean = true,
                         flare            : Boolean = true,
                         micHPF           : Float   = 50f,
                         wakeDlyMin       : Float   = 2.7f,
                         wakeDlyMax       : Float   = 8.1f,
                         wakeCheckDur     : Float   = 2.5f,
                         exhaustionThresh : Float   = 0.15f,
                         exhaustionDur    : Float   = 300f,
                         rfid             : Boolean = false,
                         // --- Visual ---
                         radius           : Int     = 240,
                         numBlades        : Int     = 6,
                         margin           : Int     = 0,
                         fps              : Int     = 50,
                         fullScreen       : Boolean = true,
                         imageIndex       : Int     = -1, // 51,
                         centerIndex      : Int     = 0,
                         smooth           : Boolean = false,
                         rotScanSpeed     : Double  = 1.0e-6,
                         // --- Sensors ---
                         ldrThresh        : Int     = 100,
                         ldrNoise         : Int     =  10,
                         capThresh        : Int     = 200,
                         capNoise         : Float   =   3f,
                         // --- Crypsis ---
                         debugRec         : Boolean = false,
                         crypMicAmp       : Float = 30.0f, // 4.0f,
                         crypSpeakerAmp   : Float = 3f, // 6.0f,
                         //                         crypLimiterLevel : Float = -12f,
                         cmpThreshIn      : Float = 16f, // 10f,
                         cmpThreshOut     : Float = 21f, // 15f,
                         crypAchilles     : Float = 0.98f,
                         crypModFreq      : Float = 0.15f, // 0.25f, // 0.5f, // 5.6f,
                         crypModMinFreq   : Float = 0.07f,
                         crypModMaxFreq   : Float = 0.25f,
                         initCrypMinDur   : Float = 60.0f,
                         initCrypMaxDur   : Float = 180.0f,
                         // --- Accelerate ---
                         accelMicAmp      : Float = 20.0f,
                         accelSigAmp      : Float = -12f, // -10.5f, // 1.0f,
                         accelCmpThresh   : Float = 15f,
                         accelMinFactor   : Float = 16f,
                         accelMaxFactor   : Float = 64f,
                         accelRecTime     : Float = 600f,
                         initAccMinDur    : Float = 60.0f,
                         initAccMaxDur    : Float = 180.0f,
                         // --- DetectSpace ---
                         unknownLatency   : Int = -64, // frames
                         spaceCorrection  : Float = -100f, // -26f, // cm
                         minSpacePos      : Int = 6,
                         maxSpacePos      : Int = 24,
                         sweepAmp         : Float = -33f, // -30f,
                         numPosThresh     : Int = 3,
                         jackBlockSize    : Int = 1024,
                         jackNumBlocks    : Int = 3,
                         dirAudio         : File = new File("audio_work"),
                         spaceMaxCm       : Float = 12000.0f,
                         detectSpacePeriod: Float = 1200f,
                         // --- SpaceTimbre ---
                         spaceMinCm       : Float = 60.0f, // 10.0
                         // spaceMaxCm: Float = 12000.0f, // 2000.0
                         spaceMinFreq     : Float = 150.0f,
                         spaceMaxFreq     : Float = 18000.0f,
                         spaceAmp         : Float = -33f, // -36.0f, // decibels
                         spaceCurve       : Curve = Curve.exp,
                         spaceAmpMaxDamp  : Float = 9.0f, // 6.0f, // decibels
                         spaceMinDur      : Float = 32f,
                         spaceMaxDur      : Float = 80f,
                         // --- Silence ---
                         silenceAmp       : Float = -33f, // -36f, // -24f, // -30f,
                         silenceFreq      : Float = 34f,
                         silenceProb      : Float = 0.22f, // 0.15f,
                         silenceMinDur    : Float = 30f,
                         silenceMaxDur    : Float = 90f,
                         // --- Writing ---
                         writingFan       : Boolean = true,
                         writingMicGain   : Float   = 2.0f,
                         writingPlayGain  : Float   = -3f, // 0.0f,
                         // writingLimiterLevel: Float = -12f,
                         writingBaseDir   : File    = file("/mnt/ramdisk"), // File.tempDir,
                         writingDebug     : Boolean = false,
                         // --- ReceiveSensors ---
                         debugSensors     : Boolean       = false,
                         printSensors     : Boolean       = false,
                         sensorGui        : Boolean       = false,
                         numLDRSensors    : Int           = 6,
                         numCapSensors    : Int           = 6,
                         buttonSensors    : Boolean       = true,
                         recordSensors    : Option[File]  = None,
                         sensorOsc        : Boolean       = false,
                         sensorOscIP      : String        = "192.168.77.78",
                         sensorOscPort    : Int           = 57130,
                         sensorThreshWake : Float         = 0.25f,
                         sensorThreshShift: Float         = 0.12f,
                         sensorAttackShift: Float         = 0.95f,
                         sensorDecayShift : Float         = 0.995f,
                         sensorAttackFlare: Float         = 0.1f,
                         sensorDecayFlare : Float         = 0.1f,
                         sensorDiffMinBoost: List[Float]  = 1.0f / 32 :: Nil,
                         sensorDiffMaxBoost: List[Float]  = 1.0f /  4 :: Nil,
                         sensorDiffShrinkThresh: Float    = 0.5f,
                         // --- FanControl ---
                         fanGui           : Boolean       = false,
                         // --- LightShift ---
                         shiftLowestFreq  : Float = 120f,
                         shiftHighestFreq : Float = 18000f,
                         shiftMainAmp     : List[Float] = List(-9f, 9f),
                         shiftMainLim     : Float = -9f,
                         shiftFreqRaster  : Float = 1.0f / 50,
                         shiftDebug       : Boolean = false,
                         shiftShelf       : Boolean = true,
                        // --- Phoresis ---
                         phoresisSendIp     : String  = "127.0.0.1",
                         phoresisSendPort   : Int     = 5005,
                         phoresisReceiveIp  : String  = "127.0.0.1",
                         phoresisReceivePort: Int     = 5006,
                         phoresisAmp        : Float   = 0f, // -30.0f, // decibels
                         phoresisGendyLoad  : Float   = 0f,
                         phoresisGendySave  : Float   = 0.5f,
                         phoresisColorFreq  : Float   = 12f,
                         phoresisAdjacentSound: Int     = 1,
                         narcissistMinDly   : Float   = 4.0f * 60,
                         narcissistMaxDly   : Float   = 6.0f * 60,
                         narcissistMinDur   : Float   = 0.25f * 60,
                         narcissistMaxDur   : Float   = 1.5f * 60,
                       )
    extends ConfigAll {

    val hasSerial: Boolean = serialDevice != "none"

    override def colorGui: Boolean = false

    override def isWindow   : Boolean = nodeId == NodeIdWindow
    override def isStripey  : Boolean = nodeId == NodeIdStripey
    override def isTrittico : Boolean = nodeId == NodeIdTrittico

    override def shiftNumBands: Int = numLDRSensors
  }

  trait Config {
    def nodeId: Int

    def isLaptop  : Boolean
    def isWindow  : Boolean
    def isStripey : Boolean
    def isTrittico: Boolean

    def radius: Int

    def margin: Int

    def serialDevice: String

    def capThresh: Int
    def capNoise: Float

    def visual  : Boolean
    def sound   : Boolean
    def writing : Boolean

    def hasSerial: Boolean

    /** seconds */
    def sleepTime: Float

    def lightShift: Boolean

    def flare: Boolean

    /** seconds */
    def wakeDlyMin    : Float
    /** seconds */
    def wakeDlyMax    : Float
    /** seconds */
    def wakeCheckDur  : Float

    /** Accumulative RMS (linear) */
    def exhaustionThresh: Float

    /** seconds */
    def exhaustionDur: Float

    /** if true, use rfid reader ('phoretic') */
    def rfid: Boolean
  }

  def autoNodeId(): Int = {
    import sys.process._
    val nodeName = Seq("uname", "-n").!!.trim
    if (nodeName == "aleph") 0 else if (nodeName.startsWith("raspberrypi")) {
      // nodeName.substring("klangpi".length).toInt % 32
      val ip = thisIP()
       resolveNodeId(ip)
    } else {
      Console.err.println(s"WARNING: Cannot parse node-name $nodeName. Falling back to node-id 0")
      0
    }
  }

  def shutdown(): Unit = {
    import sys.process._
    log.info("Issuing shutdown")
    val cmd = Seq("sudo", "shutdown", "now")
    cmd.!
  }

  private val imageIndexMap = Map[Int, Int](
    0 -> 49,
    1 -> 51,
    2 -> 54,
    3 -> 63,
    4 -> 1635,
  ) .withDefaultValue(51)

  def autoImageIndex(nodeId: Int): Int =
    imageIndexMap(nodeId)

  def init(): Unit = {
    SoundProcesses.init()
    FScape        .init()
    Pattern       .init()
  }

  def main(args: Array[String]): Unit = {
    Main.printInfo()

    object p extends ScallopConf(args) {
      import org.rogach.scallop.*

      printedName = "SwapRogue"
      private val default = ConfigImpl()

      val imageIndex: Opt[Int] = opt(/*default = Some(default.imageIndex),*/
        descr = s"Index of rotation center element (default: ${default.imageIndex}).",
//        validate = x => x >= 0 && x < centers.size,
      )
      val centerIndex: Opt[Int] = opt(default = Some(default.centerIndex),
        descr = s"Index of rotation center element (default: ${default.centerIndex}).",
        validate = x => x >= 0, // && x < centers.size,
      )
      val radius: Opt[Int] = opt(default = Some(default.radius),
        descr = s"Envelope radius, greater than zero (default: ${default.radius}).",
        validate = x => x > 0,
      )
      val margin: Opt[Int] = opt(default = Some(default.margin),
        descr = s"Window margin in pixels (default: ${default.margin}).",
        validate = x => x >= 0,
      )
      val numBlades: Opt[Int] = opt(default = Some(default.numBlades),
        descr = s"Number of blades, 3 or larger (default: ${default.numBlades}).",
        validate = x => x >= 3
      )
      val rotScanSpeed: Opt[Double] = opt(default = Some(default.rotScanSpeed),
        descr = s"Rotation scan speed (default: ${default.rotScanSpeed}).",
        validate = x => x > 0.0
      )
      val fps: Opt[Int] = opt(default = Some(default.fps),
        descr = s"Window refresh frames-per-second (default: ${default.fps}).",
        validate = x => x > 0,
      )
      val ldrThresh: Opt[Int] = opt(default = Some(default.ldrThresh),
        descr = s"LDR change threshold (default: ${default.ldrThresh}).",
        validate = x => x > 0,
      )
      val capThresh: Opt[Int] = opt(default = Some(default.capThresh),
        descr = s"Capacitive change threshold. Lower values produce more movement. (default: ${default.capThresh}).",
        validate = x => x > 0,
      )
      val ldrNoise: Opt[Int] = opt(default = Some(default.ldrNoise),
        descr = s"LDR noise floor divider (default: ${default.ldrNoise}).",
        validate = x => x > 0,
      )
      val capNoise: Opt[Float] = opt(default = Some(default.capNoise),
        descr = s"Capacitive noise floor divider. Higher values produce more movement. (default: ${default.capNoise}).",
        validate = x => x > 0f,
      )
      val fullScreen: Opt[Boolean] = toggle(default = Some(default.fullScreen),
        descrYes = "Put window into full-screen mode (default)",
        descrNo  = "Do not put window into full-screen mode",
      )
      val smooth: Opt[Boolean] = toggle(default = Some(default.smooth),
        descrYes = "Use smooth interpolation.",
      )
      val debug: Opt[Boolean] = toggle(default = Some(default.debugSensors),
        descrYes = "Debug mode.",
      )
      val verbose: Opt[Boolean] = toggle(default = Some(default.verbose),
        descrYes = "Verbosity on (default)",
        descrNo  = "Verbosity off",
      )
      val debugSensors: Opt[Boolean] = toggle(default = Some(default.debugSensors),
        descrYes = "Debug sensor reception.",
      )
      val isLaptop: Opt[Boolean] = toggle(default = Some(default.isLaptop),
        descrYes = "Run on laptop without GPIO and sensors.",
      )
      val nodeId: Opt[Int] = opt(
        descr = "Node-id or -1 (default) to automatically detect via 'uname'.",
      )
      val visual: Opt[Boolean] = toggle(default = Some(default.visual),
        descrYes = "Activate visual layer (default)",
        descrNo = "Do not activate visual layer"
      )
      val sound: Opt[Boolean] = toggle(default = Some(default.sound),
        descrYes = "Activate sound layer (default)",
        descrNo = "Do not activate sound layer"
      )
      val writing: Opt[Boolean] = toggle(default = Some(default.writing),
        descrYes  = "Activate writing sound layer (default)",
        descrNo   = "Do not activate writing sound layer"
      )
      val dirAudio: Opt[File] = opt(default = Some(default.dirAudio),
        descr = s"Audio file directory (default: ${default.dirAudio})"
      )
      val serialDevice: Opt[String] = opt(default = Some(default.serialDevice),
        descr = s"Serial device name for Pico (default: ${default.serialDevice}).",
      )
      val sleepTime: Opt[Float] = opt(default = Some(default.sleepTime),
        descr = s"Sleepiness time in seconds (default: ${default.sleepTime}).",
        validate = x => x > 0f,
      )
      val lightShift: Opt[Boolean] = toggle(default = Some(default.lightShift),
        descrYes = "Light-shift on (default)",
        descrNo = "Light-shift off",
      )
      val flare: Opt[Boolean] = toggle(default = Some(default.flare),
        descrYes = "Light-flare on (default)",
        descrNo  = "Light-flare off",
      )
      val micHPF: Opt[Float] = opt(default = Some(default.micHPF),
        descr = s"Microphone input high pass filter frequency or zero (default: ${default.micHPF}).",
      )
      val wakeDlyMin: Opt[Float] = opt(default = Some(default.wakeDlyMin),
        descr = s"Wake-up minimum delay in seconds (default: ${default.wakeDlyMin}).",
        validate = _ >= 0
      )
      val wakeDlyMax: Opt[Float] = opt(default = Some(default.wakeDlyMax),
        descr = s"Wake-up maximum delay in seconds (default: ${default.wakeDlyMax}).",
        validate = _ >= 0
      )
      val wakeCheckDur: Opt[Float] = opt(default = Some(default.wakeCheckDur),
        descr = s"Wake-up sensor check duration in seconds (default: ${default.wakeCheckDur}).",
        validate = _ >= 0
      )
      val exhaustionThresh: Opt[Float] = opt(default = Some(default.exhaustionThresh),
        descr = s"Cumulative RMS exhaustion threshold, or zero (default: ${default.exhaustionThresh}).",
        validate = _ >= 0
      )
      val exhaustionDur: Opt[Float] = opt(default = Some(default.exhaustionDur),
        descr = s"Exhaustion power-nap duration, seconds (default: ${default.exhaustionDur}).",
        validate = _ > 0
      )
      val rfid: Opt[Boolean] = toggle(default = Some(default.rfid),
        descrYes = "Use RFID reader aka phoretic mode",
      )

      // --- Crypsis ---
      val crypMicAmp: Opt[Float] = opt(default = Some(default.crypMicAmp),
        descr = s"Crypsis microphone boost, decibels (default: ${default.crypMicAmp}).",
      )
      val crypSpeakerAmp: Opt[Float] = opt(default = Some(default.crypSpeakerAmp),
        descr = s"Crypsis speaker amplitude, decibels (default: ${default.crypSpeakerAmp}).",
      )
//      val crypLimiterLevel: Opt[Float] = opt(default = Some(default.crypLimiterLevel),
//        descr = s"Crypsis limiter level, in decibels, negative (default: ${default.crypLimiterLevel}).",
//        validate = x => x <= 0
//      )
      val cmpThreshIn: Opt[Float] = opt(default = Some(default.cmpThreshIn),
        descr = s"Crypsis input compression threshold in neg.decibels (default: ${default.cmpThreshIn}).",
      )
      val cmpThreshOut: Opt[Float] = opt(default = Some(default.cmpThreshOut),
        descr = s"Crypsis output compression threshold in neg.decibels (default: ${default.cmpThreshOut}).",
      )
      // --- Accelerate ---
      val accelMicAmp: Opt[Float] = opt(default = Some(default.accelMicAmp),
        descr = s"Acceleration microphone boost, decibels (default: ${default.accelMicAmp}).",
      )
      val accelSigAmp: Opt[Float] = opt(default = Some(default.accelSigAmp),
        descr = s"Acceleration signal boost, decibels (default: ${default.accelSigAmp}).",
      )
      // --- DetectSpace ---
      val unknownLatency: Opt[Int] = opt(default = Some(default.unknownLatency),
        descr = s"Add unknown latency in sample frames to IR measurement (default: ${default.unknownLatency}).",
      )
      val minSpacePos: Opt[Int] = opt(default = Some(default.minSpacePos),
        descr = s"Minimum number of spatial positions, center clipping will be adjusted (default: ${default.minSpacePos}).",
        validate = _ > 1
      )
      val maxSpacePos: Opt[Int] = opt(default = Some(default.maxSpacePos),
        descr = s"Maximum number of spatial positions, center clipping will be adjusted (default: ${default.maxSpacePos}).",
        validate = _ > 1
      )
      val spaceCorrection: Opt[Float] = opt(default = Some(default.spaceCorrection),
        descr = s"Add correction in cm to space measurements (default: ${default.spaceCorrection}).",
      )
      val sweepAmp: Opt[Float] = opt(default = Some(default.sweepAmp),
        descr = s"Sweep amplitude, in decibels (default: ${default.sweepAmp}).",
      )
      val numPosThresh: Opt[Int] = opt(default = Some(default.numPosThresh),
        descr = s"Number of threshold attempts when unifying positions (default: ${default.numPosThresh}).",
        validate = _ > 0
      )
      val jackBlockSize: Opt[Int] = opt(default = Some(default.jackBlockSize),
        descr = s"Jack Audio Server block size (default: ${default.jackBlockSize}).",
        validate = _ > 0
      )
      val jackNumBlocks: Opt[Int] = opt(default = Some(default.jackNumBlocks),
        descr = s"Jack Audio Server number of blocks (default: ${default.jackNumBlocks}).",
        validate = _ > 0
      )
      val detectSpacePeriod: Opt[Float] = opt(default = Some(default.detectSpacePeriod),
        descr = s"Detect-space period in seconds (default: ${default.detectSpacePeriod}).",
      )
      // --- SpaceTimbre ---
      val spaceMinCm: Opt[Float] = opt(default = Some(default.spaceMinCm),
        descr = s"Minimum assumed spatial position in cm (default: ${default.spaceMinCm}).",
      )
      val spaceMaxCm: Opt[Float] = opt(default = Some(default.spaceMaxCm),
        descr = s"Maximum assumed spatial position in cm (default: ${default.spaceMaxCm}).",
      )
      val spaceMinFreq: Opt[Float] = opt(default = Some(default.spaceMinFreq),
        descr = s"Minimum assumed space-timbre frequency in Hz (default: ${default.spaceMinFreq}).",
      )
      val spaceMaxFreq: Opt[Float] = opt(default = Some(default.spaceMaxFreq),
        descr = s"Maximum assumed space-timbre frequency in Hz (default: ${default.spaceMaxFreq}).",
      )
      val spaceAmp: Opt[Float] = opt(default = Some(default.spaceAmp),
        descr = s"Space-timbre amplitude, in decibels (default: ${default.spaceAmp}).",
      )
      val spaceAmpMaxDamp: Opt[Float] = opt(default = Some(default.spaceAmpMaxDamp),
        descr = s"Space-timbre maximum amplitude damping, in decibels (default: ${default.spaceAmpMaxDamp}).",
      )

      import SpaceTimbre.ReadCurve

      val spaceCurve: Opt[Curve] = opt(default = Some(default.spaceCurve),
        descr = s"Space-timbre frequency distribution curve; lin, exp, sine, welch, 2.0 etc. (default: ${default.spaceCurve}).",
      )
      // --- Silence ---
      val silenceAmp: Opt[Float] = opt(default = Some(default.silenceAmp),
        descr = s"Silent click amp, in decibels (default: ${default.silenceAmp}).",
      )
      val silenceFreq: Opt[Float] = opt(default = Some(default.silenceFreq),
        descr = s"Silent click frequency, in Hz (default: ${default.silenceFreq}).",
        validate = _ > 0
      )
      val silenceProb: Opt[Float] = opt(default = Some(default.silenceProb),
        descr = s"Probability (0 until 1) of going silent after a sound layer finishes (default: ${default.silenceProb}).",
        validate = { x => x >= 0 && x < 1 }
      )
      val silenceMinDur: Opt[Float] = opt(default = Some(default.silenceMinDur),
        descr = s"Silence minimum duration, in seconds (default: ${default.silenceMinDur}).",
        validate = _ > 0
      )
      val silenceMaxDur: Opt[Float] = opt(default = Some(default.silenceMaxDur),
        descr = s"Silence maximum duration, in seconds (default: ${default.silenceMaxDur}).",
        validate = _ > 0
      )
      // --- Writing ---
      val writingMicGain: Opt[Float] = opt(default = Some(default.writingMicGain),
        descr = s"Writing microphone gain, in decibels (default: ${default.writingMicGain}).",
      )
      val writingPlayGain: Opt[Float] = opt(default = Some(default.writingPlayGain),
        descr = s"Writing playback gain, in decibels (default: ${default.writingPlayGain}).",
      )
      val writingBaseDir: Opt[File] = opt(default = Some(default.writingBaseDir),
        descr = s"Base directory for audio files (default: ${default.writingBaseDir}).",
      )
      val writingFan: Opt[Boolean] = toggle(default = Some(default.writingFan),
        descrYes = "Control fan while processing.",
        descrNo = "Do not control fan while processing.",
      )
      val writingDebug: Opt[Boolean] = toggle(default = Some(default.writingDebug),
        descrYes = "Enter writing debug mode (verbosity).",
      )
      // --- ReceiveSensors ---
      val numLDRSensors: Opt[Int] = opt(name = "num-ldr-sensors", default = Some(default.numLDRSensors),
        descr = s"Number of LDR sensor values per frame (default: ${default.numLDRSensors}).",
      )
      val numCapSensors: Opt[Int] = opt(name = "num-cap-sensors", default = Some(default.numCapSensors),
        descr = s"Number of capacitive sensor values per frame (default: ${default.numCapSensors}).",
      )
      val sensorOsc: Opt[Boolean] = toggle(default = Some(default.sensorOsc),
        descrYes = "Send sensor data to OSC.",
      )
      val sensorOscIP: Opt[String] = opt(name = "osc-ip", default = Some(default.sensorOscIP),
        descr = s"Target OSC IP address (default: ${default.sensorOscIP}).",
      )
      val sensorOscPort: Opt[Int] = opt(name = "osc-port", default = Some(default.sensorOscPort),
        descr = s"Target OSC port (default: ${default.sensorOscPort}).",
      )
      val sensorThreshWake: Opt[Float] = opt(default = Some(default.sensorThreshWake),
        descr = s"Threshold for waking up (default: ${default.sensorThreshWake}).",
      )
      val sensorThreshShift: Opt[Float] = opt(default = Some(default.sensorThreshShift),
        descr = s"Threshold for shifting frequency bands (default: ${default.sensorThreshShift}).",
      )
      val sensorAttackShift: Opt[Float] = opt(default = Some(default.sensorAttackShift),
        descr = s"Attack coefficient for shifting frequency bands (default: ${default.sensorAttackShift}).",
      )
      val sensorDecayShift: Opt[Float] = opt(default = Some(default.sensorDecayShift),
        descr = s"Decay coefficient for shifting frequency bands (default: ${default.sensorDecayShift}).",
      )
      val sensorAttackFlare: Opt[Float] = opt(default = Some(default.sensorAttackFlare),
        descr = s"Attack coefficient for light flare (default: ${default.sensorAttackFlare}).",
      )
      val sensorDecayFlare: Opt[Float] = opt(default = Some(default.sensorDecayFlare),
        descr = s"Decay coefficient for light flare (default: ${default.sensorDecayFlare}).",
      )
      val sensorDiffMinBoost: Opt[List[Float]] = opt(default = Some(default.sensorDiffMinBoost),
        descr = s"Differentiation minimum boost (default ${default.sensorDiffMinBoost})"
      )
      val sensorDiffMaxBoost: Opt[List[Float]] = opt(default = Some(default.sensorDiffMaxBoost),
        descr = s"Differentiation maximum boost (default ${default.sensorDiffMaxBoost})"
      )
      val sensorDiffShrinkThresh: Opt[Float] = opt(default = Some(default.sensorDiffShrinkThresh),
        descr = s"Differentiation shrink threshold (default: ${default.sensorDiffShrinkThresh}).",
      )

      // --- LightShift ---
      val shiftLowestFreq: Opt[Float] = opt(default = Some(default.shiftLowestFreq),
        descr = s"Light-shift lowest band cut-off in Hz (default: ${default.shiftLowestFreq}).",
        validate = x => x > 20f && x < 20000f,
      )
      val shiftHighestFreq: Opt[Float] = opt(default = Some(default.shiftHighestFreq),
        descr = s"Light-shift highest band cut-off in Hz (default: ${default.shiftHighestFreq}).",
        validate = x => x > 20f && x < 20000f,
      )
      val shiftMainAmp: Opt[List[Float]] = opt(default = Some(default.shiftMainAmp),
        descr = s"Light-shift gain, in decibels (default: ${default.shiftMainAmp}).",
      )
      val shiftMainLim: Opt[Float] = opt(default = Some(default.shiftMainLim),
        descr = s"Light-shift limiter level, in decibels, negative (default: ${default.shiftMainLim}).",
        validate = x => x <= 0
      )
      val shiftFreqRaster: Opt[Float] = opt(default = Some(default.shiftFreqRaster),
        descr = s"Light-shift frequency raster 0<x<1 (default: ${default.shiftFreqRaster}).",
        validate = x => x > 0f && x < 1f,
      )
      val shiftDebug: Opt[Boolean] = toggle(default = Some(default.shiftDebug),
        descrYes = "Debug light-shift stage.",
      )
      val shiftShelf: Opt[Boolean] = toggle(default = Some(default.shiftDebug),
        descrYes = "Use high shelf in light-shift stage (default).",
        descrNo  = "Do not use high shelf in light-shift stage.",
      )

      // --- Phoresis ---
      val phoresisSendIp: Opt[String] = opt(default = Some(default.phoresisSendIp),
        descr = s"Phoresis RFID server IP (default: ${default.phoresisSendIp}).",
      )
      val phoresisSendPort: Opt[Int] = opt(default = Some(default.phoresisSendPort),
        descr = s"Phoresis RFID server port (default: ${default.phoresisSendPort}).",
      )
      val phoresisReceiveIp: Opt[String] = opt(default = Some(default.phoresisReceiveIp),
        descr = s"Phoresis RFID client IP (default: ${default.phoresisReceiveIp}).",
      )
      val phoresisReceivePort: Opt[Int] = opt(default = Some(default.phoresisReceivePort),
        descr = s"Phoresis RFID client port (default: ${default.phoresisReceivePort}).",
      )
      val phoresisAmp: Opt[Float] = opt(default = Some(default.phoresisAmp),
        descr = s"Phoresis amplitude, in decibels (default: ${default.phoresisAmp}).",
      )
      val phoresisGendyLoad: Opt[Float] = opt(default = Some(default.phoresisGendyLoad),
        descr = s"Phoresis loading sonification, gendy amount 0 to 1 (default: ${default.phoresisGendyLoad}).",
      )
      val phoresisGendySave: Opt[Float] = opt(default = Some(default.phoresisGendySave),
        descr = s"Phoresis saving sonification, gendy amount 0 to 1 (default: ${default.phoresisGendySave}).",
      )
      val phoresisColorFreq: Opt[Float] = opt(default = Some(default.phoresisColorFreq),
        descr = s"Phoresis colour frequenzy in Hz (default: ${default.phoresisColorFreq}).",
        validate = _ >= 0.1f,
      )
      val phoresisAdjacentSound: Opt[Int] = opt(default = Some(default.phoresisAdjacentSound),
        descr = s"Phoresis adjacent-index sound index (default: ${default.phoresisAdjacentSound}).",
        validate = _ > 0,
      )
      val narcissistMinDly: Opt[Float] = opt(default = Some(default.narcissistMinDly),
        descr = s"Narcissist minimum delay in seconds (default: ${default.narcissistMinDly}).",
        validate = _ > 0f,
      )
      val narcissistMaxDly: Opt[Float] = opt(default = Some(default.narcissistMaxDly),
        descr = s"Narcissist maximum delay in seconds (default: ${default.narcissistMaxDly}).",
        validate = _ > 0f,
      )
      val narcissistMinDur: Opt[Float] = opt(default = Some(default.narcissistMinDur),
        descr = s"Narcissist minimum duration in seconds (default: ${default.narcissistMinDur}).",
        validate = _ > 0f,
      )
      val narcissistMaxDur: Opt[Float] = opt(default = Some(default.narcissistMaxDur),
        descr = s"Narcissist maximum duration in seconds (default: ${default.narcissistMaxDur}).",
        validate = _ > 0f,
      )

      verify()

      private val nodeIdValue     = nodeId    .getOrElse(autoNodeId())
      private val imageIndexValue = imageIndex.getOrElse(autoImageIndex(nodeIdValue))
      private val calcLatency     = unknownLatency() + (SR * LimiterLookAhead).toInt

      val config: ConfigAll = ConfigImpl(
        imageIndex              = imageIndexValue,
        centerIndex             = centerIndex   (),
        radius                  = radius        (),
        margin                  = margin        (),
        numBlades               = numBlades     (),
        rotScanSpeed            = rotScanSpeed  (),
        fps                     = fps           (),
        ldrThresh               = ldrThresh     (),
        capThresh               = capThresh     (),
        ldrNoise                = ldrNoise      (),
        capNoise                = capNoise      (),
        fullScreen              = fullScreen    (),
        smooth                  = smooth        (),
        verbose                 = verbose       (),
        debugSensors            = debugSensors  (),
        isLaptop                = isLaptop      (),
        nodeId                  = nodeIdValue,
        dirAudio                = dirAudio      (),
        visual                  = visual        (),
        sound                   = sound         (),
        writing                 = writing       (),
        debug                   = debug         (),
        serialDevice            = serialDevice  (),
        sleepTime               = sleepTime     (),
        lightShift              = lightShift    (),
        flare                   = flare         (),
        micHPF                  = micHPF        (),
        wakeDlyMin              = wakeDlyMin    (),
        wakeDlyMax              = wakeDlyMax    (),
        wakeCheckDur            = wakeCheckDur  (),
        exhaustionThresh        = exhaustionThresh(),
        exhaustionDur           = exhaustionDur (),
        rfid                    = rfid          (),
        // --- Crypsis ---
        crypMicAmp              = crypMicAmp(),
        crypSpeakerAmp          = crypSpeakerAmp(),
        cmpThreshIn             = cmpThreshIn(),
        cmpThreshOut            = cmpThreshOut(),
        // --- Accelerate ---
        accelMicAmp             = accelMicAmp(),
        accelSigAmp             = accelSigAmp(),
        // --- DetectSpace ---
        unknownLatency          = calcLatency,
        minSpacePos             = minSpacePos(),
        maxSpacePos             = maxSpacePos(),
        spaceCorrection         = spaceCorrection(),
        sweepAmp                = sweepAmp(),
        numPosThresh            = numPosThresh(),
        jackBlockSize           = jackBlockSize(),
        jackNumBlocks           = jackNumBlocks(),
        detectSpacePeriod       = detectSpacePeriod(),
        // --- SpaceTimbre ---
        spaceMinCm              = spaceMinCm(),
        spaceMaxCm              = spaceMaxCm(),
        spaceMinFreq            = spaceMinFreq(),
        spaceMaxFreq            = spaceMaxFreq(),
        spaceAmp                = spaceAmp(),
        spaceAmpMaxDamp         = spaceAmpMaxDamp(),
        spaceCurve              = spaceCurve(),
        // --- Silence ---
        silenceAmp              = silenceAmp(),
        silenceFreq             = silenceFreq(),
        silenceProb             = silenceProb(),
        silenceMinDur           = silenceMinDur(),
        silenceMaxDur           = silenceMaxDur(),
        // --- Writing ---
        writingMicGain          = writingMicGain(),
        writingPlayGain         = writingPlayGain(),
        writingBaseDir          = writingBaseDir(),
        writingFan              = writingFan(),
        writingDebug            = writingDebug(),
        // --- ReceiveSensors ---
        numLDRSensors           = numLDRSensors(),
        numCapSensors           = numCapSensors(),
        sensorOsc               = sensorOsc(),
        sensorOscIP             = sensorOscIP(),
        sensorOscPort           = sensorOscPort(),
        sensorThreshWake        = sensorThreshWake(),
        sensorThreshShift       = sensorThreshShift(),
        sensorAttackShift       = sensorAttackShift(),
        sensorDecayShift        = sensorDecayShift(),
        sensorAttackFlare       = sensorAttackFlare(),
        sensorDecayFlare        = sensorDecayFlare(),
        sensorDiffMinBoost      = sensorDiffMinBoost(),
        sensorDiffMaxBoost      = sensorDiffMaxBoost(),
        sensorDiffShrinkThresh  = sensorDiffShrinkThresh(),
        // --- LightShift ---
        shiftLowestFreq         = shiftLowestFreq(),
        shiftHighestFreq        = shiftHighestFreq(),
        shiftMainAmp            = shiftMainAmp(),
        shiftMainLim            = shiftMainLim(),
        shiftFreqRaster         = shiftFreqRaster(),
        shiftDebug              = shiftDebug(),
        shiftShelf              = shiftShelf(),
        // --- Phoresis ---
        phoresisSendIp          = phoresisSendIp(),
        phoresisSendPort        = phoresisSendPort(),
        phoresisReceiveIp       = phoresisReceiveIp(),
        phoresisReceivePort     = phoresisReceivePort(),
        phoresisAmp             = phoresisAmp(),
        phoresisGendyLoad       = phoresisGendyLoad(),
        phoresisGendySave       = phoresisGendySave(),
        phoresisColorFreq       = phoresisColorFreq(),
        phoresisAdjacentSound   = phoresisAdjacentSound(),
        narcissistMinDly        = narcissistMinDly(),
        narcissistMaxDly        = narcissistMaxDly(),
        narcissistMinDur        = narcissistMinDur(),
        narcissistMaxDur        = narcissistMaxDur(),
      )
    }

    implicit val c: ConfigAll = p.config

    if (c.verbose ) log.level = Level.Info
    if (c.debugSensors   ) log.level = Level.Debug

    log.info(s"Node-id is ${c.nodeId}")

    init()

    if !c.isLaptop then {
      // disable screen blanking
      import sys.process._
      Seq("xset", "s", "off").!
      Seq("xset", "-dpms").!
    }

    implicit val system: InMemory = Main.mkSystem()

    lazy val port: SerialPort = SerialSupport.openPort(c.serialDevice)

    val sensors = if c.isLaptop then ReceiveSensors.dummy else {
//      implicit val sCfg: ReceiveSensors.Config = ReceiveSensors.ConfigImpl(
//        debugSensors = c.debugSensors,
//      )
      ReceiveSensors.run(port)
    }
    val sBody = sensors.body

    val fan: FanControl   .Result = if c.writing && c.writingFan then FanControl.run(port) else FanControl.dummy()
    lazy val colrMem      = TransmitColor.memory()
    val colrOut: TransmitColor.Result = if c.flare then TransmitColor.run(port) else TransmitColor.dummy()
    lazy val colrSwitch   = TransmitColor.switch(colrMem, colrOut)
    val colrSet: TransmitColor.Result = if c.rfid  then colrSwitch else colrOut

    if c.buttonSensors then {
      var lastPressed       = false
      var buttonTimeOut     = 0L
      sBody.addListener { case _ =>
        val butVal          = sBody.buttons // sensorVals(sensorVals.length - 1)
        val extButPressed   = (butVal & 1) == 1
        val now             = System.currentTimeMillis()
        if extButPressed != lastPressed then {
          lastPressed       = extButPressed
          buttonTimeOut     = now + 1000L
          log.info(s"Power-Off button ${if extButPressed then "pressed" else "released"}")
        }
        if extButPressed && now >= buttonTimeOut then {
          try {
            fan(0)
            SoundProcesses.step[T]("led-off") { implicit tx =>
              colrSet.setColor(0)
            }
          } finally {
            Main.shutdown()
          }
        }
      }
    }

    implicit val rnd: Random = new Random()

    val awakeRef: BooleanObj.Var[T] = system.step { implicit tx =>
      BooleanObj.newVar(false)
    }

    def startVisual(lightShift: LightShift.Result): Unit =
      Swing.onEDT(runVisual(sensors.body, fan = fan, colrSet = colrSet, lightShift = lightShift, awakeRef = awakeRef))

    val doBoot = c.sound || c.writing
    if doBoot then {
      SoundBody.bootWith { implicit tx => implicit universe => _ /*s*/ =>
        val ls = if c.lightShift  then LightShift() else LightShift.dummy()
        colrOut.setColor(0)
        if c.lightShift || c.flare then {
          tx.afterCommit {
            val arrShiftOff   = 0
            val arrShiftNum   = c.numLDRSensors
            val arrFlareOff1  = 0
            val arrFlareOff2  = c.numLDRSensors
            val arrFlareNum   = Math.min(c.numLDRSensors, c.numCapSensors)
            val shiftAdd      = -c.sensorThreshShift
            val flareAdd      = 0f // -c.sensorThreshShift
            val shiftMul      = 1.0f / (1.0f + shiftAdd)
            val flareMul      = 1.0f / (1.0f + flareAdd)
            val shiftDelaySec = rnd.between(8.0  /*4.0*/, 24.0 /*12.0*/)  // seconds
            val flareDelaySec = rnd.between(16.0 /*4.0*/, 48.0 /*12.0*/)  // seconds
            val shiftDelayFr    = (shiftDelaySec * AssumedSensorRate).toInt
            val flareDelayFr    = (flareDelaySec * AssumedSensorRate).toInt
            val shiftDelayBufSz = shiftDelayFr + 1
            val flareDelayBufSz = flareDelayFr + 1
            // println(f"shiftDelaySec $shiftDelaySec%1.1f, shiftDelayBufSz $shiftDelayBufSz%d, flareDelaySec $flareDelaySec%1.1f, flareDelayBufSz $flareDelayBufSz%d")
            var shiftIdx        = 0
            var flareIdx        = 0
            val shiftDelayBuf   = Array.ofDim[Float](shiftDelayBufSz, arrShiftNum)
            val flareDelayBuf   = new Array[Int](flareDelayBufSz)
            val flareColors     = Array.tabulate(arrFlareNum) { ci =>
              val hue = ci.linLin(0f, arrFlareNum.toFloat, 0f, 1f)
              java.awt.Color.HSBtoRGB(hue, 1f, 1f)
            }
            var satMul = 1.0f
            val hsvArr = new Array[Float](3)

//                var T0  = 0L
//                var CNT = -1

            sBody.addListener { case _ =>
//                  CNT += 1
//                  if CNT == 0 then {
//                    T0  = System.currentTimeMillis()
//                  } else if CNT == 100 then {
//                    val T1 = System.currentTimeMillis()
//                    val dt = (T1 - T0) / 1000.0
//                    val freq = CNT / dt
//                    println(f"MEASURED SENSOR FREQ IS $freq%1.2f Hz.")
//                  }

              val arrDiffShift    = sBody.diffValuesShift
              val shiftVals       = shiftDelayBuf(shiftIdx)
              var i = 0
              while i < arrShiftNum do {
                val v   = arrDiffShift(i + arrShiftOff).toFloat
                val vN  = Math.max(0f, Math.min(1f, (v + shiftAdd) * shiftMul))
                val vR  = 1f - vN
                val vP  = vR * vR
                val vL  = 1f - vP
                shiftVals(i) = vL
                i += 1
              }
              val arrFlareShift = sBody.diffValuesFlare
              var flareRed      = 0f
              var flareGreen    = 0f
              var flareBlue     = 0f
              i = 0
              while i < arrFlareNum do {
                val v1  = arrFlareShift(i + arrFlareOff1).toFloat
                val v2  = arrFlareShift(i + arrFlareOff2).toFloat
                val vN1 = Math.max(0f, Math.min(1f, (v1 + flareAdd) * flareMul))
                val vN2 = Math.max(0f, Math.min(1f, (v2 + flareAdd) * flareMul))
                val vR1 = 1f - vN1
                val vR2 = 1f - vN2
                val vP1 = vR1 * vR2
                val vP2 = vR1 * vR2
                val vL1 = 1f - vP1
                val vL2 = 1f - vP2
                val vL  = vL1 * vL2
                val fc  = flareColors(i)
                flareRed   += ((fc >> 16) & 0xFF) * vL
                flareGreen += ((fc >>  8) & 0xFF) * vL
                flareBlue  += ( fc        & 0xFF) * vL
                i += 1
              }
              val flareGain   = 1f
              val flareRedI   = Math.min(0xFF, (flareRed   * flareGain).toInt)
              val flareGreenI = Math.min(0xFF, (flareGreen * flareGain).toInt)
              val flareBlueI  = Math.min(0xFF, (flareBlue  * flareGain).toInt)
              val flareRGB    = (flareRedI << 16) | (flareGreenI << 8) | flareBlueI
              flareDelayBuf(flareIdx) = flareRGB

              shiftIdx = (shiftIdx + 1) % shiftDelayBufSz
              flareIdx = (flareIdx + 1) % flareDelayBufSz
              val shiftDelaySeq   = ArraySeq.unsafeWrapArray(shiftDelayBuf(shiftIdx))
              val flareDelayRGB0  = flareDelayBuf(flareIdx)
              val flareDelayR     = (flareDelayRGB0 >> 16) & 0xFF
              val flareDelayG     = (flareDelayRGB0 >>  8) & 0xFF
              val flareDelayB     =  flareDelayRGB0        & 0xFF
              val saturation      = {
                val m = Math.max(flareDelayR, Math.max(flareDelayG, flareDelayB))
                if m == flareDelayR then {
                  val ms = Math.max(flareDelayG, flareDelayB)
                  m - ms
                } else if m == flareDelayG then {
                  val ms = Math.max(flareDelayR, flareDelayB)
                  m - ms
                } else {
                  val ms = Math.max(flareDelayR, flareDelayG)
                  m - ms
                }
              }
              if saturation > 127 then {
                if satMul > 0.5f then satMul -= 0.01f
              } else {
                if satMul < 1f then satMul += 0.01f
              }
              val flareDelayRGB = if satMul >= 1f then flareDelayRGB0 else {
                java.awt.Color.RGBtoHSB(flareDelayR, flareDelayG, flareDelayB, hsvArr)
                java.awt.Color.HSBtoRGB(hsvArr(0), hsvArr(1) * satMul, hsvArr(2) * satMul)
              }

              SoundProcesses.step[T]("shift-and-flare") { implicit tx =>
                ls.shiftSet(shiftDelaySeq)
                colrSet.setColor(flareDelayRGB)
              }
            }
          }
        }

        val wrPlayRef         = BooleanObj.newVar[T](false)
        val wrChanRef         = IntObj    .newVar[T](0)
        val channelAllocator  = ChannelAllocator.applyWith(wrPlay = wrPlayRef, wrCh = wrChanRef)
        val wrBusyRenderRef   = BooleanObj.newVar[T](false)
        // println(s"isWindow? ${c.isWindow}; isStripey? ${c.isStripey}; isTrittico? ${c.isTrittico}")
        // we pause accel-recording during writing-rendering only on 'Window', since here the microphone
        // is located right next to the fan
        val pauseAccelRef     = if c.isWindow then wrBusyRenderRef else BooleanObj.newConst[T](false)
        if c.sound then SoundBody(channelAllocator, pauseAccelRef = pauseAccelRef, awakeRef = awakeRef)
        if c.writing then {
          val writing = Writing(
            busyRenderRef = wrBusyRenderRef,
            busyPlayRef   = wrPlayRef,
            playChanRef   = wrChanRef,
            awakeRef      = awakeRef,
          )
          if c.writingFan && c.hasSerial then {
            writing.busyRender().changed.react { implicit tx =>
              ch =>
                import ch.{now => busy}
                tx.afterCommit {
                  if c.debug      then println(s"Writing busy render: $busy")
                  if c.hasSerial  then fan(if busy then 1 else 0)
                }
            }
          }
          writing.runner.run()  // explicitly required
        }

        if c.rfid then {
          Phoresis(channelAllocator, colrSwitch)
        }

        if c.visual then tx.afterCommit {
          startVisual(lightShift = ls)
        }
      }
    } else {
      if c.visual then startVisual(lightShift = LightShift.dummy())
    }
  }

  def mkSystem(): InMemory = InMemory()

  /** Must be called on the EDT. */
  def runVisual(sensorBody: SensorBody, fan: FanControl.Result, colrSet: TransmitColor.Result,
                lightShift: LightShift.Result, awakeRef: BooleanObj.Var[T])
               (implicit cursor: Cursor[T], c: ConfigAll, rnd: Random): Unit = {
    val centers = Center.read(c.imageIndex)
    val visual  = Visual(centers)

    new MainFrame {
      if c.fullScreen then {
        peer.setUndecorated(true)
        visual.componentJ.setCursor(java.awt.Toolkit.getDefaultToolkit.createCustomCursor(
          new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB), new Point(0, 0), "hidden"
        ))
        visual.componentJ.addKeyListener(
          new KeyAdapter {
            override def keyPressed(e: KeyEvent): Unit =
              if e.getKeyCode == KeyEvent.VK_ESCAPE then {
                fan(0)
                SoundProcesses.step[T]("led-off") { implicit tx =>
                  colrSet.setColor(0)
                }
                closeOperation()
              }
          }
        )
      } else {
        title = "SwapRogue"
      }

      contents  = visual.component
      pack()
      centerOnScreen()
      open()
      visual.component.requestFocus()
    }

    var ci          = c.centerIndex
    var tUpdate     = System.currentTimeMillis()
    var tDebug      = tUpdate

    val histSize            = 200 // 20 Hz rate -- 10 seconds history
    val arrCenterOff        = c.numLDRSensors
    val arrCenterNum        = c.numCapSensors
    var arrCenterStartIdx   = 0
    val arrCenterMax        = centers.size // Center.map(c.imageIndex).size
    val arrWakeOff          = 0
    val arrWakeNum          = c.numLDRSensors
    val sensorCenterHistory = Array.ofDim[Int](arrCenterNum, histSize)
    val sensorCenterSum     = new Array[Int](arrCenterNum)
    var hasHistory          = false
    var histOff             = 0
    val thresh              = c.capThresh // c.ldrThresh
    val noiseMul            = 1.0f / c.capNoise  // c.ldrNoise
    var holdUpdate          = 2000
    var awake               = false
    var tSleepy             = 0L    // time after which we get sleepy
    var tRested             = 0L    // time after which we may awake
    val tSleepDurMS         = (c.sleepTime * rnd.nextDouble().linExp(0, 1, 0.8, 1.25) * 1000).toLong

    val wakeDelaySec        = rnd.between(c.wakeDlyMin /* 2.7 4.0*/ , c.wakeDlyMax /* 8.1 12.0*/) // seconds
    val wakeDelayFr         = (wakeDelaySec   * AssumedSensorRate).toInt
    val wakeCheckFr         = (c.wakeCheckDur * AssumedSensorRate).toInt
    val wakeDelayBufSz      = wakeDelayFr + wakeCheckFr + 1
    var wakeIdxA            = 0
    var wakeIdxB            = wakeCheckFr
    val wakeDelayBuf        = Array.ofDim[Double](wakeDelayBufSz, arrWakeNum) // careful: 'arraycopy' is not type-safe
    val exhaustionDurMS     = (c.exhaustionDur * 1000).toLong
    val syncExhaustion      = new AnyRef

    def tickSleepCycle(isExcited: Boolean, t1: Long): Unit = {
      val shouldBeWake = isExcited && syncExhaustion.synchronized {
        (t1 > tRested) && {
          tSleepy = t1 + tSleepDurMS
          true
        }
      }

      if awake then {
        if t1 > tSleepy then { // XXX TODO: should synchronize `tSleepy`?
          log.info("fall asleep")
          awake = false
          syncExhaustion.synchronized {
            tRested = Math.max(tRested, t1 + 10000L)
          }
          Swing.onEDT {
            visual.closeIris()
          }
          SoundProcesses.step[T]("sleep") { implicit tx =>
            awakeRef() = false
          }
        }
      } else {
        if shouldBeWake then {
          log.info("awake")
          awake = true
          if arrCenterMax > arrCenterNum then arrCenterStartIdx = rnd.between(0, arrCenterMax - arrCenterNum + 1)
          Swing.onEDT {
            visual.openIris()
          }
          SoundProcesses.step[T]("awake") { implicit tx =>
            awakeRef() = true
          }
        }
      }
    }

    if c.exhaustionThresh > 0 then lightShift.addListener {
      case LightShift.Report(rmsAcc) =>
        println(s"REPORT ${rmsAcc}")
        if rmsAcc > c.exhaustionThresh then {
          val t1  = System.currentTimeMillis()
          log.info("resting due to exhaustion")
          lightShift.clearRMSAcc()
          syncExhaustion.synchronized {
            tRested = t1 + exhaustionDurMS
            tSleepy = 0L
          }
        }
    }

    if c.isLaptop then {
      val t = new Timer()

      def randomMove(): Unit =
        val dly = rnd.between(6000, 16000)
        t.schedule({ () =>
          val idx = rnd.between(0, 6)
          Swing.onEDT {
            visual.setCenterIndex(idx)
          }
          randomMove()
        }, dly)

      Swing.onEDT {
        visual.openIris()
      }
      randomMove()

      t.schedule({ () =>
        SoundProcesses.step[T]("awake") { implicit tx =>
          awakeRef() = true
        }
      }, 4000)

      val dtSense = (1000.0/AssumedSensorRate).toLong
      t.schedule({ () =>
        val t1 = System.currentTimeMillis()
        tickSleepCycle(isExcited = true, t1 = t1)
      }, dtSense, dtSense)
    }

    sensorBody.addListener { case _ =>
      val arrRaw      = sensorBody.rawValues
      val arrDiffNew  = sensorBody.diffValuesShift
      val t1          = System.currentTimeMillis()
      System.arraycopy(arrDiffNew, arrWakeOff, wakeDelayBuf(wakeIdxA), 0, arrWakeNum)
      wakeIdxA         = (wakeIdxA + 1) % wakeDelayBufSz
      wakeIdxB         = (wakeIdxB + 1) % wakeDelayBufSz
      val arrDiffDlyA  = wakeDelayBuf(wakeIdxA)
      val arrDiffDlyB  = wakeDelayBuf(wakeIdxB)

      var j = 0
      var isExcited = false
      while j < arrWakeNum do {
        val vA = arrDiffDlyA(j + arrWakeOff)
        val vB = arrDiffDlyB(j + arrWakeOff)
        if vA > c.sensorThreshWake && vB > c.sensorThreshWake then {
          isExcited = true
          j = arrWakeNum
        } else {
          j += 1
        }
      }

      tickSleepCycle(isExcited = isExcited, t1 = t1)

      var i = 0
      while i < arrCenterNum do {
        val h = sensorCenterHistory(i)
        val v = arrRaw(i + arrCenterOff)
        if hasHistory then {
          val vOld = h(histOff)
          h(histOff) = v
          sensorCenterSum(i) += v - vOld
        } else {
          java.util.Arrays.fill(h, v)
          sensorCenterSum(i) = v * histSize
        }
        i += 1
      }
      histOff = (histOff + 1) % histSize
      if !hasHistory then hasHistory = true

      if t1 - tUpdate > holdUpdate then {
        var i = 0
        var mi = -1
        var mt = thresh
        while i < arrCenterNum do {
          val v = arrRaw(i + arrCenterOff)
          val mean = sensorCenterSum(i)/histSize
          val noiseFloor = (mean * noiseMul).toInt
          val t = abs(v - mean)
          if (t > noiseFloor && t > mt) {
            mt = t
            mi = i
          }
          i += 1
        }

        if c.debugSensors && t1 - tDebug > 1000 then {
          println(s"arr ${arrRaw.slice(arrCenterOff, arrCenterOff + arrCenterNum).mkString(", ")}")
          println(s"sen ${sensorCenterSum.iterator.map(_ / histSize).mkString(", ")}; mi $mi, mt $mt")
          tDebug = t1
        }

        if mi != -1 && mi != ci then {
          val ciP = arrCenterStartIdx + mi
          log.debug(s"visual.setCenterIndex($ciP)")

          ci      = mi
          tUpdate = t1
          holdUpdate = rnd.between(1500, 6000 /* 4500 */)
          if awake then Swing.onEDT {
            if visual.isIrisOpen() then visual.setCenterIndex(ciP)
          }
        }
      }
    }
  }

//    val t = new Timer()
//    t.scheduleAtFixedRate({ () =>
//      visual.repaint()
//      visual.toolkit.sync()
//    }, c.refreshPeriod.toLong, c.refreshPeriod.toLong)

