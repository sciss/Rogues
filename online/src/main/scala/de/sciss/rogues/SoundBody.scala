/*
 *  SoundBody.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.lucre.Txn.peer
import de.sciss.lucre.synth.{InMemory, Server}
import de.sciss.lucre.{BooleanObj, Cursor, Random, RandomObj, Workspace}
import de.sciss.numbers.Implicits.*
import de.sciss.proc.{AuralSystem, SoundProcesses, TimeRef, Universe}
import de.sciss.synth.Client
import de.sciss.synth.UGenSource.Vec

import scala.concurrent.stm.TMap
//import de.sciss.tagfalter.Biphase.MessageSpaceId
import de.sciss.rogues.{SwapRogue => Main}
import Main.{ConfigAll, T, log}

import scala.concurrent.stm.Ref

object SoundBody {
  private[this] val sync        = new AnyRef
  private[this] var _instance   = Option.empty[SoundBody]

  private val INTER_DELAY_MIN = 1.5f
  private val INTER_DELAY_MAX = 4.5f

  def instance: Option[SoundBody] = sync.synchronized(_instance)

  def boot(done: T => Universe[T] => Server => Unit): Unit = {
    implicit val system: InMemory = Main.mkSystem()
    bootWith(done)
  }

  def bootWith(done: T => Universe[T] => Server => Unit)(implicit system: InMemory): Unit = {
    val as = AuralSystem(global = true)
    val sCfg = Server.Config()
    sCfg.inputBusChannels   = 1
    sCfg.outputBusChannels  = 2
    sCfg.deviceName = Some("Rogues")
    val cCfg = Client.Config()
    system.step { implicit tx =>
      as.react { tx => {
        case AuralSystem.Running(s) =>
          tx.afterCommit {
            // s.peer.dumpOSC()
            SoundProcesses.step[T]("booted") { implicit tx =>
              implicit val ws: Workspace[T] = Workspace.Implicits.dummy[T]
              implicit val u: Universe[T] = Universe[T]()
              done(tx)(u)(s)
            }
          }

        case _ => ()
      }
      }
      as.start(sCfg, cCfg)
    }
  }

  def apply(channelAllocator: ChannelAllocator, pauseAccelRef: BooleanObj[T], awakeRef: BooleanObj[T])
           (implicit tx: T, config: ConfigAll, universe: Universe[T]): SoundBody = {
    require (instance.isEmpty)
    implicit val random: Random[T] = RandomObj()
    val res = new Impl(random, channelAllocator, pauseAccelRef = pauseAccelRef, awakeRef = awakeRef)
    sync.synchronized {
      _instance = Some(res)
    }
    res.start()
    res
  }

//  private val HEX = "0123456789ABCDEF"

//  private case class JoyReceive(nodeId: Int, rcv: Biphase.Receive)

  private final class Impl(val random: Random[T], channelAllocator: ChannelAllocator,
                           pauseAccelRef: BooleanObj[T], awakeRef: BooleanObj[T])
                          (implicit val universe: Universe[T], val config: ConfigAll)
    extends SoundBody {

    implicit def body: SoundBody = this

    // it takes longer on 'Window' due to pausing
    private val accelRecDly     = config.accelRecTime * (if config.isWindow then 1.5f else 1.0f)

    private val stageRef        = Ref[Stage](Stage.Empty)
    private val targetStageRef  = Ref[Stage](Stage.Empty)
    private val stagePosRef     = Ref[Vec[Float]](Vec.empty)
    private val runningRef      = Ref[Option[Stage.Running]](None)
    private val accelRecRef     = Ref[Option[Accelerate.RecResult]](None)
    private val accelRecTime    = Ref(0L)
    private val detectSpaceTime = Ref(0L)
    private val stageSaveNext   = Ref[Stage](Stage.Empty)
//    private val rcvSpaceId      = Ref(0L)
//    private val timeSpaceId     = Ref(0L)
//    private val commFreqSeqRef  = Ref[Vec[Float]](Vec.empty)
//    private val joyBlockTime    = Ref(0L)   // no joy response until this point in time has been reached
//    private val joyRcvRef       = Ref[Vec[JoyReceive]](Vec.empty)
    private val joyResponse     = Ref(false)
    private val holdOn          = Ref(false)
    private val holdBlockTime   = Ref(0L)   // no hold response until this point in time has been reached

    // 6 bytes in `MessageSpaceId` times 10 bits per byte, one second extra time out
//    private val TimeOutSpaceId    = ((config.bitPeriod/1000 * (10 * MessageSpaceId.NumBytes) + 1f) * TimeRef.SampleRate).toLong
    // less frequent in Eisenerz
    private val DetectSpacePeriod0 = /*if (config.isEisenerz) config.detectSpacePeriod * 1.8f else*/ config.detectSpacePeriod
    private val DetectSpacePeriod  = DetectSpacePeriod0 * 1.5.pow((config.nodeId % 32)/32.0)
//    private val TimeOutReboot     = (TimeRef.SampleRate * config.rebootTimeOut).toLong

    override def allocChannel   (key: ChannelKey)(implicit tx: T): Int  = channelAllocator.allocChannel   (key)
    override def releaseChannel (key: ChannelKey)(implicit tx: T): Unit = channelAllocator.releaseChannel (key)

    private def renewTimeOut()(implicit tx: T): Unit = {
      ()
//      val sch = universe.scheduler
//      val tokenTimeOut = sch.schedule(sch.time + TimeOutReboot) { implicit tx =>
//        log.warn("SoundBody time-out. Going to reboot")
//        tx.afterCommit {
//          import sys.process._
//          Seq("sudo", "reboot", "now").!
//        }
//      }
//      val tokenOld = tokenTimeOutRef.swap(tokenTimeOut)
//      sch.cancel(tokenOld)
    }

    override def isAwake(implicit tx: T): Boolean = awakeRef.value

    def start()(implicit tx: T): Unit = {
      holdOn() = !awakeRef.value
      awakeRef.changed.react { implicit tx => ch =>
        holdOn() = !ch.now
        if ch.now then awake()
        else lull()
      }
      renewTimeOut()
//      startBiphaseRcv()
      targetStage_=(Stage.Crypsis)
    }

//    private def received(m: Biphase.Message)(implicit tx: T): Unit = {
//      log.info(s"Received $m")
//
//      val sch = universe.scheduler
//      val now = sch.time
//      m match {
//        case Biphase.MessageHoldOn =>
//          if (now > holdBlockTime() && !holdOn()) {
//            holdOn() = true
//            runningRef().foreach(_.release())
//            val dlySec    = random.nextFloat().linLin(0f, 1f, 10f, 20f)
//            log.info(f"Hold on for $dlySec%1.1fs")
//            val dlyFr     = (dlySec * TimeRef.SampleRate).toLong
//            sch.schedule(now + dlyFr) { implicit tx =>
//              holdOn() = false
//              tryLaunchTarget()
//            }
//          }
//
//        case m @ Biphase.MessageSpaceId(nodeId, _ /*f1*/, _ /*f2*/) =>
//          val joyRcvOld = joyRcvRef()
//          val rlsIdx0   = joyRcvOld.indexWhere(_.nodeId == nodeId)
//          // release the receiver for the existing node, or otherwise release the oldest
//          // so we never have more than two receivers
//          val rlsIdx    = if (rlsIdx0 >= 0) rlsIdx0 else if (joyRcvOld.size > 1) 1 else -1
//          val joyRcvMed = if (rlsIdx < 0) joyRcvOld else {
//            val joyRls = joyRcvOld(rlsIdx)
//            joyRls.rcv.dispose()
//            joyRcvOld.patch(rlsIdx, Nil, 1)
//          }
//          val joyAtk    = mkJoyReceiver(m)
//          val joyRcvNew = joyAtk +: joyRcvMed
//          joyRcvRef()   = joyRcvNew
//
//        case Biphase.MessageJoy(_ /*nodeId*/) =>
//          val commFreqOpt = thisCommFreq
//          if (now > joyBlockTime() && !joyResponse() && commFreqOpt.isDefined) {
//            val dlySec    = random.nextFloat().linLin(0f, 1f, 1f, 4f)
//            val dlyFr     = (dlySec * TimeRef.SampleRate).toLong
//            val timeResp  = now + dlyFr
//            joyBlockTime() = timeResp + (TimeRef.SampleRate * 10).toLong
//            joyResponse() = true
//            runningRef().foreach(_.release())
//            sch.schedule(timeResp) { implicit tx =>
//              log.info("Sending joy response")
//              val mResp = Biphase.MessageJoy(config.nodeId)
//              Biphase.send(mResp.encode, freq = commFreqOpt.get, ampDb = config.encAmpComm) { implicit tx =>
//                joyResponse() = false
//                val dlySec    = random.nextFloat().linLin(0f, 1f, 2f, 4f)
//                log.info(f"Hold on for $dlySec%1.1fs")
//                val dlyFr     = (dlySec * TimeRef.SampleRate).toLong
//                sch.schedule(sch.time + dlyFr) { implicit tx =>
//                  tryLaunchTarget()
//                }
//              }
//            }
//          }
//
//        case _ => ()  // `Message` is sealed, does not occur
//      }
//    }
//
//    private def mkJoyReceiver(m: Biphase.MessageSpaceId)(implicit tx: T): JoyReceive = {
//      val rcvAtk = Biphase.receive(f1 = m.f1.toFloat, f2 = m.f2.toFloat) { implicit tx => byte =>
//        val hexHi = (byte >> 4) & 0x0F
//        val hexLo =  byte       & 0x0F
//        log.debug(s"Received node comm 0x${HEX(hexHi)}${HEX(hexLo)}")
//        if (byte == Biphase.CMD_JOY) {
//          // Note: we don't actually wait for the nodeId byte
//          received(Biphase.MessageJoy(0))
//        }
//      }
//      JoyReceive(m.nodeId, rcvAtk)
//    }
//
//    private def startBiphaseRcv()(implicit tx: T): Unit = {
//      /*val rcvGlobal =*/ Biphase.receive(f1 = config.biphaseF1a, f2 = config.biphaseF2a) { implicit tx => byte =>
//        // if (byte == Biphase.CMD_HOLD_ON)
//        val hexHi = (byte >> 4) & 0x0F
//        val hexLo =  byte       & 0x0F
//        log.debug(s"Received global comm 0x${HEX(hexHi)}${HEX(hexLo)}")
//        val now = universe.scheduler.time
//        if (byte == Biphase.CMD_HOLD_ON) {
//          rcvSpaceId()  = 0L
//          received(Biphase.MessageHoldOn)
//
//        } else if (byte == Biphase.CMD_SPACE_ID) {
//          rcvSpaceId()  = 1L
//          timeSpaceId() = now
//
//        } else {
//          val rcvSpaceIdOld = rcvSpaceId()
//          val byteCntOld = (rcvSpaceIdOld & 0xFF).toInt
//          if (byteCntOld > 0 && byteCntOld < MessageSpaceId.NumBytes) {
//            if (now - timeSpaceId() < TimeOutSpaceId) {
//              val payloadOld    = rcvSpaceIdOld & 0xFFFFFFFFFFFF00L
//              val payloadNew    = (payloadOld | (byte & 0xFF)) << 8
//              val byteCntNew    = byteCntOld + 1
//              val rcvSpaceIdNew = payloadNew | byteCntNew
//              rcvSpaceId()      = rcvSpaceIdNew
//              if (byteCntNew == MessageSpaceId.NumBytes) {
//                // println(s"payloadNew = $payloadNew")
//                val nodeId  =  ((payloadNew >> (5 * 8)) & 0xFF).toInt
//                val f1      = (((payloadNew >> (4 * 8)) & 0x7F).toInt << 7) |
//                  ((payloadNew >> (3 * 8)) & 0x7F).toInt
//                val f2      = (((payloadNew >> (2 * 8)) & 0x7F).toInt << 7) |
//                  ((payloadNew >> (1 * 8)) & 0x7F).toInt
//                val mId = Biphase.MessageSpaceId(nodeId = nodeId, f1 = f1, f2 = f2)
//                received(mId)
//              }
//            } else {
//              log.debug(s"Global comm time out")
//              rcvSpaceId() = 0L
//            }
//          }
//        }
//      }
//    }
//
//    override def allCommFreq(implicit tx: T): Vec[Float] = {
//      val commFreqSeq = commFreqSeqRef()
//      val xs = Vec(config.biphaseF1a, config.biphaseF2a)
//      if (commFreqSeq.isEmpty) xs else xs :+ commFreqSeq(0) :+ commFreqSeq(1)
//    }
//
//    override def otherCommFreq(implicit tx: T): Vec[Float] =
//      Vec(config.biphaseF1a, config.biphaseF2a)
//
//    override def thisCommFreq(implicit tx: T): Option[Biphase.Freq] = {
//      val commFreqSeq = commFreqSeqRef()
//      if (commFreqSeq.isEmpty) None else {
//        val f = Biphase.Freq(
//          f1a = commFreqSeq(0), f1b = commFreqSeq(2), f2a = commFreqSeq(1), f2b = commFreqSeq(3)
//        )
//        Some(f)
//      }
//    }

    override def accelerateRec(implicit tx: T): Option[Accelerate.RecResult] =
      accelRecRef()

    override def stage(implicit tx: T): Stage = stageRef()

    override def targetStage(implicit tx: T): Stage = targetStageRef()

    override def targetStage_=(value: Stage)(implicit tx: T): Unit = {
      // val oldTarget = targetStageRef.swap(value)
      targetStageRef() = value
      val sch = universe.scheduler
      // try to start next stage with a second delay
      val INTER_DELAY = random.nextFloat().linLin(0f, 1f, INTER_DELAY_MIN, INTER_DELAY_MAX)
      sch.schedule(sch.time + (TimeRef.SampleRate * INTER_DELAY).toLong) { implicit tx =>
        tryLaunchTarget()
      }
    }

    private def lull()(implicit tx: T): Unit = {
      runningRef().foreach { r =>
        val st = r.stage
        log.info(s"Lulling $st")
        r.lull() // release()
      }
    }


    private def awake()(implicit tx: T): Unit = {
      runningRef() match {
        case Some(r)  => r.awake()
        case None     => tryLaunchTarget()
      }
    }

    private def tryLaunchTarget()(implicit tx: T): Unit = {
      if (runningRef().isDefined || joyResponse() /*|| holdOn()*/) {
        log.info(s"launch skipped. running? ${runningRef().isDefined}; joy? ${joyResponse()}; hold? ${holdOn()}")
        return
      }

      val st0       = targetStageRef()
      val st = if !holdOn() then st0 else {
        if random.nextBoolean() then Stage.Crypsis else Stage.Silence
      }
      stageRef()    = st
      val running   = st.run()
      runningRef()  = Some(running)
      val timeNow   = universe.scheduler.time

      def hasAccel(): Boolean =
        accelerateRec.isDefined && {
          val timeRec = accelRecTime()
          timeRec != 0L && (timeNow - timeRec) / TimeRef.SampleRate >= accelRecDly
        }

      def hasSpaceTimbre(): Boolean = detectSpaceTime() != 0L

      def randomStage(): Stage = {
        val _a  = hasAccel()
        val num = (if _a then 1 else 0) + (if hasSpaceTimbre() then 1 else 0) + 2
        random.nextInt(num) match {
          case 0 => Stage.Crypsis
          case 1 => Stage.Silence
          case 2 => if _a then Stage.Accelerate else Stage.SpaceTimbre
          case 3 => Stage.SpaceTimbre
        }
      }

      val nextSt0: Stage = if holdOn() then st else st match {
        case Stage.Crypsis =>
          val timeDetectOld = detectSpaceTime()
          if !hasSpaceTimbre() || (timeNow - timeDetectOld) / TimeRef.SampleRate >= DetectSpacePeriod then {
            detectSpaceTime() = timeNow
            Stage.DetectSpace
          } else {
            log.info("(skip detect-space this time)")
            Stage.SpaceTimbre
          }

        case Stage.DetectSpace =>
          holdBlockTime() = timeNow + (TimeRef.SampleRate * 10).toLong
          Stage.SpaceTimbre

        case Stage.SpaceTimbre =>
          // XXX TODO: warning, `accelRecRef` may no longer be the same after the current stage
          // finishes, once we add another process that occasionally frees the ref!
          if hasAccel() then {
            Stage.Accelerate
          } else {
            Stage.Crypsis
          }

        case Stage.Accelerate => Stage.Crypsis
        case Stage.Silence    =>
          val st = stageSaveNext()
          if st != Stage.Empty then st else randomStage()

//        case Stage.Joy  =>
//          joyBlockTime() = timeNow + (TimeRef.SampleRate * 10).toLong
//          stageSaveNext()

        case _ => randomStage() // Stage.Crypsis // Stage.Empty
      }

      val nextSil = st != Stage.Silence && random.nextFloat() < config.silenceProb
//      val nextJoy = !nextSil && st != Stage.Joy && random.nextFloat() < config.joyProb
      val nextSt  = if (nextSil) {
        stageSaveNext() = nextSt0
        Stage.Silence
      } else /*if (nextJoy) {
        stageSaveNext() = nextSt0
        Stage.Joy
      } else*/ {
        nextSt0
      }
      targetStageRef() = nextSt

      log.info(s"Starting $st")
      running.start()

      renewTimeOut()
    }

    override def spacePos(implicit tx: T): Vec[Float] = stagePosRef()

//    private def shuffleN(xs: Vec[Float], n: Int)(implicit tx: T): Vec[Float] = {
//      val xsSize = xs.size
//      val buf = if (xsSize >= n) {
//        xs.toArray
//      } else if (xsSize == 0) {
//        Array.fill(n)(1f)
//      } else {
//        Array.tabulate(n) { i =>
//          if (i < xsSize) xs(i) else xs.last
//        }
//      }
//
//      def swap(i1: Int, i2: Int): Unit = {
//        val tmp = buf(i1)
//        buf(i1) = buf(i2)
//        buf(i2) = tmp
//      }
//
//      var m = buf.length
//      while (m >= 2) {
//        val k = random.nextInt(m)
//        m -= 1
//        swap(m, k)
//      }
//
//      val res = Vector.newBuilder[Float]
//      res.sizeHint(n)
//      var i = 0
//      while (i < n) {
//        res += buf(i)
//        i += 1
//      }
//      res.result()
//    }

    override def spacePos_=(value: Vec[Float])(implicit tx: T): Unit = {
      stagePosRef() = value
      random.setSeed(value.sum.toLong)

//      // calculate comm freq based on this
//      val commPosSeq: Vec[Float] = {
//        val sh = shuffleN(value, 4).sorted
//        val d1 = sh(1) - sh(0)
//        val d2 = sh(2) - sh(1)
//        val d3 = sh(3) - sh(2)
//        if (d1 <= d2 && d1 <= d3) sh
//        else if (d2 <= d1 && d2 <= d3) {
//          Vector(sh(1), sh(2), sh(3), sh(0))
//        } else {
//          Vector(sh(2), sh(3), sh(0), sh(1))
//        }
//      }
//      val skipFreq    = otherCommFreq /*.flatMap(tup => tup._1 ::  tup._2 :: Nil)*/.sorted
//      val commFreqSeq = commPosSeq.map { cm =>
//        import config.{commMaxFreq, commMinFreq, spaceMaxCm, spaceMinCm}
//        val cmClip  = cm.clip(spaceMinCm, spaceMaxCm)
//        val f0      = cmClip.linLin(spaceMinCm, spaceMaxCm, commMinFreq, commMaxFreq)
//        skipFreq.foldLeft(f0) { (f1, fBlock) =>
//          // XXX TODO: should we use relative frequencies, such as `fBlock * 0.99` ?
//          //  I think Goertzel is on a linear spectrum, so probably fine like this
//          val fBlockLo = fBlock - 100f
//          val fBlockHi = fBlock + 100f
//          if (f1 >= fBlockLo && f1 <= fBlockHi) fBlockHi else f1
//        }
//      }
//      log.info(s"This comm freq $commFreqSeq")
//      commFreqSeqRef() = commFreqSeq

      //      val f1a = p1a.linLin(spaceMinCm, spaceMaxCm, idMinFreq, idMaxFreq)

      // we launch accel-rec after the first space info becomes available
      if (accelRecRef().isEmpty) {
        val mn          = value.min
        val mx          = value.max
        val mean        = value.sum / value.size
        val skew        = mean.linLin(mn, mx, 0.0f, 1.0f)
        val accelFactor = skew.linExp(0.0f, 1.0f, config.accelMinFactor, config.accelMaxFactor)
        val accelBufDur = config.accelRecTime / accelFactor
        log.info(f"New accel factor $accelFactor%1.1f; buf dur $accelBufDur%1.1f")
        val rc = Accelerate.recWith(accelFactor = accelFactor, accelBufDur = accelBufDur, pauseRef = pauseAccelRef)
        accelRecRef()   = Some(rc)
        accelRecTime()  = universe.scheduler.time
      }
    }

    override def released(st: Stage)(implicit tx: T): Unit = {
      if (stage == st) {
        log.info(s"Released $st")
        runningRef() = None
        tryLaunchTarget()
      }
    }
  }
}
trait SoundBody extends ChannelAllocator {
  implicit val universe: Universe[T]

  implicit def config: ConfigAll

  /** Currently known spatial positions in cm. Empty if unknown. */
  def spacePos(implicit tx: T): Vec[Float]
  def spacePos_=(value: Vec[Float])(implicit tx: T): Unit

  def accelerateRec(implicit tx: T): Option[Accelerate.RecResult]

  def random: Random[T]

  def stage(implicit tx: T): Stage

  def targetStage(implicit tx: T): Stage

  def targetStage_=(value: Stage)(implicit tx: T): Unit

  def released(stage: Stage)(implicit tx: T): Unit

  def isAwake(implicit tx: T): Boolean

  //  /** Currently known communication frequencies in Hz,
//   * including the global ones; thus always returns at
//   * least two elements (one tuple f1a, f2a).
//   */
//  def allCommFreq(implicit tx: T): Vec[Float]
//
//  /** Currently known communication frequencies in Hz,
//   * including the global ones, but not the own individual frequencies.
//   */
//  def otherCommFreq(implicit tx: T): Vec[Float]

//  def thisCommFreq(implicit tx: T): Option[Biphase.Freq]
}