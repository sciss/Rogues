/*
 *  Physiology.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import org.rogach.scallop.ScallopConf
import de.sciss.rogues.SwapRogue as Main
import Main.{T, SR}
import de.sciss.lucre.{Disposable, Random, RandomObj}
import de.sciss.lucre.synth.{Buffer, Synth}
import de.sciss.file.*
import de.sciss.proc.{SoundProcesses, TimeRef, Universe}
import de.sciss.synth.Import.*
import de.sciss.synth.{SynthGraph, freeSelf}
import org.rogach.scallop.{ScallopConf, ScallopOption as Opt, given}
import de.sciss.lucre.Txn.peer as txPeer

import java.io.{DataInputStream, File, FileInputStream}
import scala.concurrent.stm.Ref

object Physiology:
  object Fragments {
    def read(name: String, gainDb: Float, fadeIn: Float, fadeOut: Float)(implicit config: Config): Fragments = {
      val baseDir = config.dirAudio / s"${name}_mark.bin"
      val in      = new DataInputStream(new FileInputStream(baseDir))
      try {
        val cookie = in.readInt()
        require (cookie == 0x6d61726b)  // "mark"
        val numMark = in.readShort().toInt
        val markers = Array.fill(numMark)(in.readInt())
        new Fragments(name, gainDb = gainDb, markers = markers, fadeIn = fadeIn, fadeOut = fadeOut)

      } finally {
        in.close()
      }
    }
  }
  class Fragments(val name: String, val gainDb: Float, val markers: Array[Int], val fadeIn: Float, val fadeOut: Float)

  private val MaxFragIdx  = 3
  private val fragments   = new Array[Fragments](MaxFragIdx)

  def init()(implicit config: Config): Unit = {
    fragments(0) = Fragments.read("apfel", gainDb =  0f, fadeIn = 0.00f, fadeOut = 0.00f)
    fragments(1) = Fragments.read("ader" , gainDb =  6f, fadeIn = 0.02f, fadeOut = 0.04f)
    fragments(2) = Fragments.read("boden", gainDb = -6f, fadeIn = 0.10f, fadeOut = 0.10f)
  }

  case class ConfigImpl(
                         debug          : Boolean = false,
                         physioAmp      : Float   = -6f,
                         dirAudio       : File    = new File("audio_work"),
                         physioFragIdx  : Int     = 0,
                       ) extends Config

  trait Config {
    /** Decibels */
    def physioAmp: Float

    def physioFragIdx: Int

    def dirAudio  : File

    def debug     : Boolean
  }

  def main(args: Array[String]): Unit = {
    Main.printInfo()

    object p extends ScallopConf(args) {
      printedName = "Rogues - Physiology"
      private val default = ConfigImpl()

      val debug: Opt[Boolean] = toggle(default = Some(default.debug),
        descrYes = "Debug operation.",
      )
      val physioAmp: Opt[Float] = opt(default = Some(default.physioAmp),
        descr = s"Silent click amp, in decibels (default: ${default.physioAmp}).",
      )
      val physioFragIdx: Opt[Int] = opt(default = Some(default.physioFragIdx),
        descr = s"Physiology fragment index 0 to $MaxFragIdx (default: ${default.physioFragIdx}).",
        validate = x => x >= 0 && x <= MaxFragIdx
      )
      val dirAudio: Opt[File] = opt(default = Some(default.dirAudio),
        descr = s"Audio file directory (default: ${default.dirAudio})"
      )

      verify()
      implicit val config: Config = ConfigImpl(
        debug         = debug    (),
        physioAmp     = physioAmp(),
        physioFragIdx = physioFragIdx(),
        dirAudio      = dirAudio().getAbsoluteFile,
      )
    }
    import p.config
    run()
  }

  def run()(implicit config: Config): Unit = {
    init()
    SoundBody.boot { implicit tx =>
      implicit universe =>
        _ /*s*/ =>
          apply(/*s*/)
          ()
    }
  }

  type Result = Disposable[T]

  def apply(/*s: Server*/)(implicit tx: T, config: Config, universe: Universe[T]): Result =
    applyWith(amp = config.physioAmp.dbAmp)

  def applyWith(amp: Float)(implicit tx: T, config: Config, universe: Universe[T]): Result = {
    val rnd   = RandomObj[T]()
    val frag  = fragments(config.physioFragIdx)
    val res   = new Apply(rnd, frag, amp)
    res.next()
    res
  }

  private class Apply(rnd: Random[T], frag: Fragments, amp: Float)
                     (implicit config: Config, universe: Universe[T]) extends Result {
    import universe.{scheduler => sched, cursor}
    import frag.markers

    private val disposed = Ref(false)

    override def dispose()(implicit tx: T): Unit = disposed() = true

    def next()(implicit tx: T): Unit = if !disposed() then {
      val s = universe.auralContext.get.server
      val g = SynthGraph {
        import de.sciss.synth.Import._
        import de.sciss.synth.Ops.stringToControl
        import de.sciss.synth.proc.graph.{DiskOut => _}
        import de.sciss.synth.ugen.{DiskIn => _, PartConv => _, _}
        val bufId   = "buf".ir
        val bufDur  = BufDur.ir(bufId)
        val p       = PlayBuf.ar(numChannels = 1, buf = bufId, speed = 1f, loop = 0f, doneAction = freeSelf)
        val fadeIn  = "fade-in" .ir(0f)
        val fadeOut = "fade-out".ir(0f)
        val sustain = (bufDur - (fadeIn + fadeOut)).max(0f)
        val env     = Env.linen(fadeIn, sustain, fadeOut)
        val eg      = EnvGen.kr(env)
        val sig     = p * "amp".kr(1.0f) * eg
        PhysicalOut.ar(0, sig)
      }

      val dirAudio        = config.dirAudio
      val fileAudio       = dirAudio / s"${frag.name}.aif"
      val pathAudio       = fileAudio.path
      val fragIdx         = rnd.nextInt(markers.length - 1)
      val fileStartFrame  = markers(fragIdx)
      val fileStopFrame   = markers(fragIdx + 1)
      val fileNumFrames   = fileStopFrame - fileStartFrame
      val buf = Buffer(s)(numFrames = fileNumFrames)
      buf.readChannel(pathAudio, channels = 1 :: Nil, fileStartFrame = fileStartFrame)

      if config.debug then println(s"Physio span [$fileStartFrame, $fileStopFrame]")

      val syn = Synth.play(g, Some("physio"))(s.defaultGroup,
        args = Seq(
          "buf"       -> buf.id,
          "amp"       -> (amp * frag.gainDb.dbAmp),
          "fade-in"   -> frag.fadeIn,
          "fade-out"  -> frag.fadeOut,
        ),
        dependencies = buf :: Nil
      )
      syn.onEndTxn { implicit tx =>
        buf.dispose()
        tx.afterCommit {
          SoundProcesses.step[T]("physio-next") { implicit tx =>
            if !disposed() then {
              val dt    = rnd.nextDouble().linExp(0, 1, 0.5, 5.0)
              if config.debug then println(f"Physio gap $dt%1.1fs")
              val dtFr  = (dt * TimeRef.SampleRate).toLong
              sched.schedule(sched.time + dtFr) { implicit tx =>
                next()
              }
            }
          }
        }
      }
    }
  }