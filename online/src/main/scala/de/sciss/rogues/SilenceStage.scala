/*
 *  SilenceStage.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.lucre.Disposable
import de.sciss.lucre.Txn.peer
import de.sciss.numbers.Implicits._
import de.sciss.proc.{SoundProcesses, TimeRef}
import de.sciss.rogues.{SwapRogue => Main}
import Main.{T, log}

import scala.concurrent.stm.Ref

class SilenceStage extends Stage.Running with ChannelKey { chanKey =>
  private val synRef    = Ref(Disposable.empty[T])
  private val tokenRef  = Ref(-1)

  override def stage: Stage = Stage.Silence

  override def awake()(implicit tx: T, body: SoundBody): Unit = {
    import body.{config, random, universe}
    if random.nextDouble() > 0.25 then release()
  }

  override def start()(implicit tx: T, body: SoundBody): Unit = {
    import body.{config, random, universe}
    val channel = body.allocChannel(chanKey)
    val syn     = Silence.applyWith(amp = config.silenceAmp.dbAmp, freq = config.silenceFreq, channel = channel)
    synRef.swap(syn).dispose()

    syn.onEndTxn { implicit tx =>
      tx.afterCommit {
        import universe.cursor
        SoundProcesses.step[T]("silence-done") { implicit tx =>
          body.releaseChannel(chanKey)
          body.released(stage)
        }
      }
    }

    val sch     = universe.scheduler
    val durSec  = random.nextFloat().linLin(0f, 1f, config.silenceMinDur, config.silenceMaxDur)
    log.info(f"Silence duration $durSec%1.1f s")
    val durFr   = (TimeRef.SampleRate * durSec).toLong
    val token   = sch.schedule(sch.time + durFr) { implicit tx =>
      release()
    }
    val oldToken = tokenRef.swap(token)
    sch.cancel(oldToken)
  }

  override def release()(implicit tx: T, body: SoundBody): Unit = {
    synRef.swap(Disposable.empty).dispose()
    val sch = body.universe.scheduler
    sch.cancel(tokenRef.swap(-1))
  }
}
