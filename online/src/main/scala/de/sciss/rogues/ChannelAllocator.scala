/*
 *  ChannelAllocator.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.lucre.Txn.peer
import de.sciss.lucre.{BooleanObj, IntObj, Random, RandomObj}
import de.sciss.rogues.SwapRogue.{T, log}

import scala.concurrent.stm.{Ref, TMap}

object ChannelAllocator {
  /** Without taking 'Writing' channel into account */
  def apply()(implicit tx: T): ChannelAllocator = {
    val wrPlay  = BooleanObj.newConst[T](false)
    val wrCh    = IntObj.newVar[T](0)
    applyWith(wrPlay = wrPlay, wrCh = wrCh)
  }

  def applyWith(wrPlay: BooleanObj[T], wrCh: IntObj.Var[T])(implicit tx: T): ChannelAllocator = {
    implicit val random: Random[T] = RandomObj()
    object WritingChanKey extends ChannelKey
    val res = new Impl(random /*, writingPlayRef = wrPlay, writingChanRef = wrCh*/)
    wrPlay.changed.react { implicit tx => ch =>
      if ch.now then {
        val wrChInt = res.allocChannel(WritingChanKey)
        wrCh() = wrChInt
      } else {
        res.releaseChannel(WritingChanKey)
      }
    }
    res
  }

  private class Impl(val random: Random[T] /*, val writingPlayRef: BooleanObj[T], val writingChanRef: IntObj[T]*/)
    extends ChannelAllocator {

    private val chanAlloc0    = Ref(0)
    private val chanAlloc1    = Ref(0)
    private val chanAllocMap  = TMap.empty[ChannelKey, Boolean]

    override def allocChannel(key: ChannelKey)(implicit tx: T): Int = {
      releaseChannel(key) // just in case
      val a0 = chanAlloc0()
      val a1 = chanAlloc1()
      val chB = if a0 == 0 then {
        if a1 == 0 then random.nextBoolean()
        else false
      } else if a1 == 0 then true else {
        val bal = a1 / (a0 + a1).toDouble
        random.nextDouble() >= bal
      }
      val ch = if chB then 1 else 0
      log.info(f"Alloc channel ($a0/$a1) -> $ch")
      val chA = if chB then chanAlloc1 else chanAlloc0
      chA += 1
      chanAllocMap.put(key, chB)
      ch
    }

    override def releaseChannel(key: ChannelKey)(implicit tx: T): Unit =
      chanAllocMap.remove(key) match {
        case Some(chB)  =>
          val a   = if chB then chanAlloc1 else chanAlloc0
          val ch  = if chB then 1 else 0
          val chCnt = a() - 1
          log.info(f"Release channel $ch -> $chCnt")
          a() = chCnt
        case None =>
      }

  }
}
trait ChannelAllocator {
  def allocChannel  (key: ChannelKey)(implicit tx: T): Int
  def releaseChannel(key: ChannelKey)(implicit tx: T): Unit

//  def writingPlayRef: BooleanObj[T]
//  def writingChanRef: IntObj[T]
}
