/*
 *  DetectSpace.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.numbers.Implicits.*
import de.sciss.proc.TimeRef
//import de.sciss.tagfalter.Biphase.{MessageHoldOn, MessageSpaceId}
import de.sciss.rogues.SwapRogue as Main
import de.sciss.rogues.SwapRogue.{T, log}

import java.util.Calendar

class DetectSpaceStage extends Stage.Running with ChannelKey { chanKey =>
  override def stage: Stage = Stage.DetectSpace

  private final val INIT_DELAY_MIN = 9      // seconds
  private final val INIT_DELAY_MAX = 11     // seconds
  private final val INTER_DELAY_MIN = 0.6f  // seconds
  private final val INTER_DELAY_MAX = 1.4f  // seconds

  override def start()(implicit tx: T, body: SoundBody): Unit = {
    import body.{config, random, universe}
//    Biphase.send(MessageHoldOn.encode, ampDb = config.encAmp) { implicit tx =>
      log.info("Biphase HOLD send finished.")
      // implicit val cfgDetect = DetectSpace.ConfigImpl()
      val sch  = universe.scheduler
      val INIT_DELAY = random.nextFloat().linLin(0f, 1f, INIT_DELAY_MIN, INIT_DELAY_MAX)
      sch.schedule(sch.time + (TimeRef.SampleRate * INIT_DELAY).toLong) { implicit tx =>
        val cal     = Calendar.getInstance()
        val hour    = cal.get(Calendar.HOUR_OF_DAY)
        // println(s"hour $hour")
        val sweepUp = (hour + config.nodeId) % 2 == 0
        val channel = body.allocChannel(chanKey)
        DetectSpace(sweepUp = sweepUp, channel = channel) { implicit tx => resSpace =>
          log.info("DetectSpace finished.")
          // log.info(resSpace.posCm.mkString("Pos [cm]: ", ", ", ""))
          //        spaceTimbre(resSpace.posCm)
          body.spacePos_=(resSpace.posCm)
//          val thisFreq = body.thisCommFreq.get
//          val f1  = thisFreq.f1a.toInt
//          val f2  = thisFreq.f2a.toInt
//          val mId = MessageSpaceId(nodeId = config.nodeId, f1 = f1, f2 = f2)
//          Biphase.send(mId.encode, ampDb = config.encAmp) { implicit tx =>
            val INTER_DELAY = random.nextFloat().linLin(0f, 1f, INTER_DELAY_MIN, INTER_DELAY_MAX)
            sch.schedule(sch.time + (TimeRef.SampleRate * INTER_DELAY).toLong) { implicit tx =>
              body.releaseChannel(chanKey)
              body.released(stage)
            }
//          }
        }
      }
//    }
  }

  override def release()(implicit tx: T, body: SoundBody): Unit = ()

  //  override def release()(implicit tx: T, body: SoundBody): Unit = {
  //    import body.universe.scheduler
  //    import body.random
  //
  //    val INTER_DELAY = random.nextFloat().linLin(0f, 1f, INTER_DELAY_MIN, INTER_DELAY_MAX)
  //    scheduler.schedule(scheduler.time + (TimeRef.SampleRate * INTER_DELAY).toLong) { implicit tx =>
  //      body.released(stage)
  //    }
  //  }
}
