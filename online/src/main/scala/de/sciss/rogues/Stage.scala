/*
 *  Stage.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.proc.TimeRef
import de.sciss.rogues.{SwapRogue => Main}
import Main.T

object Stage {
  case object Empty extends Stage with Stage.Running {
    final val id = 0

    override def stage: Stage = this

    override def run(): Stage.Running = this

    override def start()(implicit tx: T, body: SoundBody): Unit = ()

    override def release()(implicit tx: T, body: SoundBody): Unit = {
      import body.universe.scheduler
      scheduler.schedule(scheduler.time + TimeRef.SampleRate.toLong) { implicit tx =>
        body.released(this)
      }
    }
  }

  case object Crypsis extends Stage {
    final val id = 1

    override final def run(): Stage.Running = new CrypsisStage
  }

  case object DetectSpace extends Stage {
    final val id = 2

    override final def run(): Stage.Running = new DetectSpaceStage
  }

  case object SpaceTimbre extends Stage {
    final val id = 3

    override final def run(): Stage.Running = new SpaceTimbreStage
  }

  case object Accelerate extends Stage  {
    final val id = 4

    override def run(): Stage.Running = new AccelerateStage
  }

  case object Silence extends Stage {
    final val id = 5

    override def run(): Stage.Running = new SilenceStage
  }

//  case object Joy extends Stage {
//    final val id = 6
//
//    override def run(): Stage.Running = new JoyStage
//  }

  trait Running {
    def stage: Stage

    def start   ()(implicit tx: T, body: SoundBody): Unit
    def release ()(implicit tx: T, body: SoundBody): Unit
    
    /** The default is to release the stage */
    def lull    ()(implicit tx: T, body: SoundBody): Unit = release()
    /** The default is to do nothing */
    def awake   ()(implicit tx: T, body: SoundBody): Unit = ()
  }
}
sealed trait Stage {
  def id: Int

  def run(): Stage.Running
}
