/*
 *  TransmitColor.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import com.fazecast.jSerialComm.SerialPort
import de.sciss.audiofile.{AudioFile, AudioFileSpec, AudioFileType, SampleFormat}
import de.sciss.file.File
import de.sciss.lucre.synth.RT
import de.sciss.osc
import org.rogach.scallop.ScallopConf
import de.sciss.lucre.Txn.{peer => txPeer}

import java.io.{BufferedReader, FileInputStream, InputStream, InputStreamReader, OutputStream}
import java.net.InetSocketAddress
import scala.concurrent.stm.{Ref, TArray}
import scala.swing.event.ValueChanged
import scala.swing.{Button, Component, Dimension, Graphics2D, GridPanel, Label, MainFrame, Slider, Swing}

/*  Corresponds to `read-serial-test.py` on the Pico, receiving data via USB serial.
 *
 *  Sends bytes 'cRGB\n' to set LED colour.
 */
object TransmitColor {
  case class ConfigImpl(
                         debug        : Boolean         = false,
                         verbose      : Boolean         = false,
                         serialDevice : String          = SerialSupport.defaultDevice,
                         colorGui     : Boolean         = false,
                   ) extends Config

  trait Config:
    def debug       : Boolean
    def verbose     : Boolean
    def serialDevice: String
    def colorGui    : Boolean

  def main(args: Array[String]): Unit =
    object p extends ScallopConf(args) {
      import org.rogach.scallop.{ScallopOption => Opt, *}

      printedName = "TransmitColor"
      private val default: Config = ConfigImpl()

      val debug: Opt[Boolean] = toggle(default = Some(default.debug),
        descrYes = "Debug operation.",
      )
      val verbose: Opt[Boolean] = toggle(default = Some(default.verbose),
        descrYes = "Verbose operation.",
      )
      val device: Opt[String] = opt(default = Some(default.serialDevice),
        descr = s"Serial device name (default: ${default.serialDevice}).",
      )
      val gui: Opt[Boolean] = toggle(default = Some(default.colorGui),
        descrYes = "Show color GUI.",
      )

      verify()
      val config: Config = ConfigImpl(
        debug         = debug     (),
        verbose       = verbose   (),
        serialDevice  = device    (),
        colorGui      = gui       (),
      )
    }

    implicit val c: Config = p.config
    val port = SerialSupport.openPort(c.serialDevice)
    run(port)
    
  trait Result {
    def setColor(rgb: Int)(implicit tx: RT): Unit
  }

  def dummy(): Result = new Result {
    override def setColor(rgb: Int)(implicit tx: RT): Unit = ()
  }

  final val NTagMemSize = 160

  def memory(sz: Int = NTagMemSize): Memory = new MemoryImpl(sz)

  trait Memory extends Result {
    def capacity: Int
    def used(implicit tx: RT): Int

    /** Returns a current snapshot */
    def getBytes(arr: Array[Byte], off: Int)(implicit tx: RT): Unit // Array[Byte]

    def clear()(implicit tx: RT): Unit
  }

  def switch[A <: Result, B <: Result](a: A, b: B): Switch[A, B] = {
    new SwitchImpl(a, b)
  }

  trait Switch[+A <: Result, +B <: Result] extends Result {
    def branchA: A
    def branchB: B

    def selectA()(implicit tx: RT): Unit
    def selectB()(implicit tx: RT): Unit
  }

  private final class SwitchImpl[A <: Result, B <: Result](val branchA: A, val branchB: B) extends Switch[A, B] {
    private final val branchRef = Ref[Result](branchA)

    override def selectA()(implicit tx: RT): Unit =
      branchRef() = branchA


    override def selectB()(implicit tx: RT): Unit =
      branchRef() = branchB

    override def setColor(rgb: Int)(implicit tx: RT): Unit = {
      val peer = branchRef()
      peer.setColor(rgb)
    }
  }

  private class MemoryImpl(sz: Int) extends Memory {
    private val offRef  = Ref(0)
    private val usedRef = Ref(0)
    private val buf     = TArray.ofDim[Int](sz) // new Array[Int](sz) // circular

    override def capacity: Int = sz

    override def used(implicit tx: RT): Int = usedRef()

    override def clear()(implicit tx: RT): Unit = {
      offRef ()  = 0
      usedRef()  = 0
    }

    override def setColor(rgb: Int)(implicit tx: RT): Unit = {
      val offV = offRef.getAndTransform(i => (i + 1) % sz)
      buf(offV) = rgb
      usedRef.transform(i => if i < sz then i + 1 else sz)
    }

    override def getBytes(arr: Array[Byte], off: Int)(implicit tx: RT): Unit /*Array[Byte]*/ = {
      val used0     = usedRef()
      val used      = math.min(used0, (arr.length - off) / 3)
      val numBytes  = used * 3
      var inOff     = (offRef() - used + sz) % sz
      var i = off
      while (i < numBytes) {
        val rgb   = buf(inOff)
        val r     = (rgb >> 16) & 0xFF
        val g     = (rgb >> 8) & 0xFF
        val b     = rgb & 0xFF
        arr(i)    = r.toByte; i += 1
        arr(i)    = g.toByte; i += 1
        arr(i)    = b.toByte; i += 1
        val off1 = inOff + 1
        inOff = if off1 < sz then off1 else 0
      }
      () // arr
    }
  }

  def run(port: SerialPort)(implicit c: Config): Result = {
    val out = {
      port.getOutputStream
    }

    val bytes = new Array[Byte](11)
    bytes( 0) = 'c' .toByte
    bytes(10) = '\n'.toByte

    def mkSlider(): Slider = {
      val sl = new Slider
      sl.min    = 0x00
      sl.max    = 0xFF
      sl.value  = 0x00
      sl.reactions += {
        case ValueChanged(_) =>
          val r = slRed   .value
          val g = slGreen .value
          val b = slBlue  .value
          sendColor(r, g, b)
      }
      sl
    }

    lazy val slRed    = mkSlider()
    lazy val slGreen  = mkSlider()
    lazy val slBlue   = mkSlider()

    def sendColor(r: Int, g: Int, b: Int): Unit = {
      var i = 1

      def push(v: Int): Unit = {
        bytes(i) = (( v / 100)        + 48).toByte; i += 1
        bytes(i) = (((v /  10) % 10)  + 48).toByte; i += 1
        bytes(i) = (( v        % 10)  + 48).toByte; i += 1
      }
      push(r); push(g); push(b)
      if (c.debug) {
        print(new String(bytes, "UTF-8"))
      }
      out.write(bytes)
      out.flush()
    }

//    lazy val lb: Component = Button("Next") {
//      val bytes = "color\n".getBytes("UTF-8")
//      out.write(bytes)
//      out.flush()
//    }

    lazy val lb: Component = new GridPanel(3, 1) {
      contents ++= Seq(slRed, slGreen, slBlue)
    }

    if c.colorGui then Swing.onEDT {
      val f = new MainFrame {
        override def closeOperation(): Unit = {
          port.closePort()
          super.closeOperation()
        }
      }
      f.contents = lb
      f.pack().centerOnScreen()
      f.open()
    }

    new Result {
      override def setColor(rgb: Int)(implicit tx: RT): Unit = {
        val r = (rgb >> 16) & 0xFF
        val g = (rgb >> 8) & 0xFF
        val b = rgb & 0xFF
        // just send it, no problem if the txn is retried or fails
//        tx.afterCommit { }
        sendColor(r, g, b)
      }
    }
  }
}