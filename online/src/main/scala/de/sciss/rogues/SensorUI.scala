/*
 *  SensorUI.scala
 *  (Rogues)
 *
 *  Copyright (c) 2021-2025 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.rogues

import de.sciss.rogues.SwapRogue as Main
import Main.T
import de.sciss.lucre.Cursor

object SensorUI {
  // test GUI
  def main(args: Array[String]): Unit =
    val c0 = ReceiveSensors.parseArgs(args, "SensorUI")
    implicit val cRcv: ReceiveSensors.Config = c0.copy(sensorGui = true)
    val port = SerialSupport.openPort(SerialSupport.defaultDevice)
    implicit val cTrns: TransmitColor.Config = TransmitColor.ConfigImpl(colorGui = true)
    TransmitColor.run(port)
    implicit val csr: Cursor[T] = Main.mkSystem()
    ReceiveSensors.run(port) // { _ => () }    // N.B.: blocks
    ()
}